import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:platform_device_id/platform_device_id.dart';
import 'package:dgdu_app/bd/UserBD.dart';

// Hex color function
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

// Launch URL
void launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

Future<String> getDeviceId() async {
  // String deviceId;
  // Platform messages may fail, so we use a try/catch PlatformException.
  try {
    return await PlatformDeviceId.getDeviceId;
  } on PlatformException {
    return 'Failed to get deviceId.';
  }
}

Future<bool> conf() async {
  List<UserRegister> list = await DBProvider.db.getAllUserRegisters();
  return list.isNotEmpty;
}
