import 'dart:async';
import 'dart:io';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "pumasAccess.db");
    return await openDatabase(path, version: 2, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE UserRegister ("
          "id INTEGER PRIMARY KEY,"
          "name TEXT,"
          "last_name TEXT,"
          "second_last_name TEXT,"
          "birthday TEXT,"
          "curp TEXT,"
          "password TEXT,"
          "gender TEXT,"
          "email TEXT,"
          "weight REAL,"
          "height REAL,"
          "worker_number TEXT,"
          "master TEXT,"
          "campus TEXT,"
          "career TEXT"
          ")");
    });
  }

  newUserRegister(UserRegister newItem) async {
    final db = await database;
    // Inserting UserRegister in table
    var insertQuery = await db.rawInsert(
        "INSERT INTO UserRegister (id, name, last_name, second_last_name, birthday, curp, password, gender, email, weight, height, worker_number, master, campus, career)"
        " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        [
          newItem.id,
          newItem.name,
          newItem.last_name,
          newItem.second_last_name,
          newItem.birthday,
          newItem.curp,
          newItem.password,
          newItem.gender,
          newItem.email,
          newItem.weight,
          newItem.height,
          newItem.worker_number,
          newItem.master,
          newItem.campus,
          newItem.career
        ]);
    return insertQuery;
  }

  Future<void> updateUserRegister(UserRegister user) async {
    final db = await database;
    await db.update("UserRegister", user.toMap(),
        where: "id = ?", whereArgs: [user.id]);
    // return void;
  }

  getUserRegister(int id) async {
    final db = await database;
    var res = await db.query("UserRegister", where: "id = ?", whereArgs: [id]);
    // print('este es');
    // print(res.isNotEmpty);
    return res.isNotEmpty ? UserRegister.fromMap(res.first) : null;
  }

  getUserRegisterEmail(String email) async {
    final db = await database;
    var res =
        await db.query("UserRegister", where: "email = ?", whereArgs: [email]);
    return res.isNotEmpty ? UserRegister.fromMap(res.first) : null;
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("DELETE FROM UserRegister");
  }

  Future<UserRegister> getUserConfig() async {
    final db = await database;
    var res =
        await db.rawQuery("SELECT * FROM UserRegister ORDER BY id ASC LIMIT 1");
    // print(res);

    return res.isNotEmpty ? UserRegister.fromMap(res.first) : null;
  }

  Future<List<UserRegister>> getAllUserRegisters() async {
    final db = await database;
    var res = await db.query("UserRegister");
    List<UserRegister> list =
        res.isNotEmpty ? res.map((c) => UserRegister.fromMap(c)).toList() : [];
    return list;
  }

  Future<void> updateUserData(int userId, String name, String last_name,
      String second_last_name, String weight, String height) async {
    // Updating db
    final db = await database;

    // Updating
    await db.update(
        "UserRegister",
        {
          'name': name,
          'last_name': last_name,
          'second_last_name': second_last_name,
          'weight': double.parse(weight),
          'height': double.parse(height),
        },
        where: "id = ?",
        whereArgs: [userId]);
  }
}
