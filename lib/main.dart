import 'package:dgdu_app/Views/Effort/EffortSelectType.dart';
import 'package:dgdu_app/Views/Errors/ErrorNotFoundView.error.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:dgdu_app/Views/Dashboard/Dashboard.dart';
import 'package:dgdu_app/Views/News/NewsFetch.dart';
import 'package:dgdu_app/Views/Effort/EffortRegisterScreenFetch.dart';
import 'package:dgdu_app/Views/Effort/EffortMainScreen.dart';
import 'package:dgdu_app/Views/Effort/EffortTrackMainScreen.dart';
import 'package:dgdu_app/Views/Effort/EffortMapFetch.dart';
import 'package:dgdu_app/Views/Effort/EffortPerfilFetch.dart';
import 'package:dgdu_app/Views/Effort/EffortSuccess.dart';
import 'package:dgdu_app/Views/Effort/EffortResumeScreen.dart';
import 'package:dgdu_app/Views/EventsScreens/EventDetailFetch.dart';
import 'package:dgdu_app/Views/EventsScreens/EventsFetch.dart';
import 'package:dgdu_app/Views/EventsScreens/PastEventsFetch.dart';
import 'package:dgdu_app/Views/Login/LoginScreen.dart';
import 'package:dgdu_app/Views/Register/AcademicDataScreen.dart';
import 'package:dgdu_app/Views/Register/PasswordScreen.dart';
import 'package:dgdu_app/Views/Register/RegisterScreen.dart';
import 'package:dgdu_app/Views/User/UserScreen.dart';
import 'package:dgdu_app/Views/Legal/LegalTermsScreen.dart';
import 'package:dgdu_app/Views/Login/PasswordForgetScreen.dart';
import 'package:dgdu_app/Views/Friendship/FriendshipViewFetch.dart';
import 'package:dgdu_app/Views/Friendship/FriendshipsRequestViewFetch.dart';
import 'package:dgdu_app/Views/Programs/ProgramsViewFetch.dart';
import 'package:dgdu_app/Views/Programs/ProgramView.dart';
import 'package:dgdu_app/Views/DownloadDeporteUNAM/DownloadMainViewFetch.dart';
import 'package:dgdu_app/Views/EventsScreens/EventRegister/EventRegisterScreenFetch.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Preloading SVG logos
  Future.wait([
    precachePicture(
      ExactAssetPicture(SvgPicture.svgStringDecoder, 'assets/logo.svg'),
      null,
    ),
    precachePicture(
      ExactAssetPicture(
          SvgPicture.svgStringDecoder, 'assets/logo_unam_svg.svg'),
      null,
    ),
  ]);

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    runApp(MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'), // Inglés
        const Locale('es'), // Español
      ],
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => Dashboard(),
        '/news/list': (context) => NewsFetch(),
        '/login': (context) => LoginScreen(),
        '/register': (context) => RegisterScreen(),
        '/forget/password': (context) => PasswordForgetScreen(),
        '/academicData': (context) => AcademicDataScreen(),
        '/password': (context) => PasswordScreen(),
        '/effort/register': (context) => EffortSelectType(),
        '/effort/select/activity': (context) => EffortMainScreen(),
        '/effort/track/selection': (context) => EffortTrackMainScreen(),
        '/effort/track': (context) => EffortMapFetch(),
        '/effort/register/activity': (context) => EffortRegisterScreenFetch(),
        '/effort/register/success': (context) => EffortSuccess(),
        '/effort/main': (context) => EffortPerfilFetch(),
        '/effor/resume': (context) => EffortResumeScreen(),
        '/events': (context) => EventsFetch(),
        '/event/register': (context) => EventRegisterScreenFetch(),
        '/user/event': (context) => EventDetailFetch(),
        '/event/past': (context) => PastEventsFetch(),
        '/map': (context) => EffortMapFetch(),
        '/user/resume': (context) => UserScreen(),
        '/legal/terms': (context) => LegalTermsScreen(),
        // friendship
        '/friendship': (context) => FriendshipViewFetch(),
        '/friendships/request': (context) => FriendshipsRequestViewFetch(),
        // programs
        '/programs': (context) => ProgramsViewFetch(),
        '/program': (context) => ProgramView(),
        // descarga deporte unam

        '/download': (context) => DownloadMainViewFetch(),

        ///errors views
        '/error': (context) => ErrorNotFoundView(),
      },
      builder: EasyLoading.init(),
    ));
  });
}
