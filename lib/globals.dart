import 'package:flutter/material.dart';
import 'package:dgdu_app/Models/EffortRegister.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/Views/Register/RegisterScreen.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';

// String baseURL = "dgdu.lampserv.com";
String baseURL = "appdeporte.unam.mx";
// String baseURL = "dgdu.sharedwithexpose.com";
// String baseURL = "dgdubeta.lampserv.com";
// String baseURL = "fe2c-189-216-93-67.ngrok.io";

String urlSuffix = "/api";

bool production = true;

Color blue = HexColor("#091234");
Color gold = HexColor("#C4A260");
Color gray = HexColor("#ACB3B6");
Color background = HexColor("#E5E7E3");

// Vars
EffortRegister effort;

// Register
String nombre = '';
String app = '';
String apm = '';
DateTime birthday;
String curp = '';
Genero genere;
String typeUser = '';
String email = '';
double weight;
double height;
String number;
String password;

UserRegister user;

String message = '';
bool errorResponse = false;
Uri uriConstructor(endpoint) {
  // return Uri.https(baseURL, urlSuffix + endpoint);
  if (production) {
    return Uri.https(baseURL, urlSuffix + endpoint);
  } else {
    // print('Fetching from ' + baseURL + urlSuffix + endpoint);
    // return Uri.http(baseURL, urlSuffix + endpoint);
    return Uri.https(baseURL, urlSuffix + endpoint);
  }
}
