import 'dart:convert';

import 'package:dgdu_app/Models/Competitor.dart';

EventPast itemFromJson(String str) {
  final jsonData = json.decode(str);
  return EventPast.fromMap(jsonData);
}

String itemToJson(EventPast data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class EventPast {
  String name;
  String date;
  String hour;
  String distance;
  String route;
  List<List<Competitor>> competitors;

  EventPast(
      {this.name,
      this.date,
      this.hour,
      this.distance,
      this.route,
      this.competitors});

  factory EventPast.fromMap(Map<String, dynamic> json) => new EventPast(
      name: json["name"],
      date: json["date"],
      hour: json["hour"],
      distance: json["distance"],
      route: json["route"],
      competitors: json["competitors"]);

  Map<String, dynamic> toMap() => {
        'name': name,
        'date': date,
        'hour': hour,
        'distance': distance,
        'route': route,
        'competitors': competitors
      };
}
