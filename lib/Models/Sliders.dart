import 'dart:convert';
import 'package:dgdu_app/Models/HomeSlider.dart';

Sliders itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Sliders.fromMap(jsonData);
}

String itemToJson(Sliders data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Sliders {
  List<HomeSlider> sliders;
  List<HomeSlider> news;

  Sliders({
    this.sliders,
    this.news,
  });

  factory Sliders.fromMap(Map<String, dynamic> json) => new Sliders(
        sliders: json["sliders"],
        news: json["news"],
      );

  Map<String, dynamic> toMap() => {
        'sliders': sliders,
        'news': news,
      };
}
