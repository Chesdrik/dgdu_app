import 'dart:convert';

Program itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Program.fromMap(jsonData);
}

String itemToJson(Program data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Program {
  int id;
  String name;
  String content;
  String lat;
  String long;

  Program({
    this.id,
    this.name,
    this.content,
    this.lat,
    this.long,
  });

  factory Program.fromMap(Map<String, dynamic> json) => new Program(
      id: json["id"],
      name: json["name"],
      content: json["content"],
      lat: json["lat"],
      long: json["long"]);

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'content': content,
        'lat': lat,
        'long': long,
      };
}
