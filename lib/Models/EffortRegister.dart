import 'dart:convert';

import 'package:dgdu_app/Models/Stats.dart';

EffortRegister itemFromJson(String str) {
  final jsonData = json.decode(str);
  return EffortRegister.fromMap(jsonData);
}

String itemToJson(EffortRegister data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class EffortRegister {
  int user;
  int idEffort;
  String title;
  String type;
  String beginning;
  String ending;
  String date;
  Stats stats;

  EffortRegister(
      {this.user,
      this.idEffort,
      this.title,
      this.type,
      this.beginning,
      this.ending,
      this.date,
      this.stats});

  factory EffortRegister.fromMap(Map<String, dynamic> json) =>
      new EffortRegister(
          user: json["user"],
          idEffort: json["idEffort"],
          title: json["title"],
          type: json["type"],
          beginning: json["beginning"],
          ending: json["ending"],
          date: json["date"],
          stats: json["stats"]);

  Map<String, dynamic> toMap() => {
        'user': user,
        'idEffort': idEffort,
        'title': title,
        'type': type,
        'beginning': beginning,
        'ending': ending,
        'date': date,
        'stats': stats,
      };

  Map<String, dynamic> toJson() => {
        'user': this.user,
        'idEffort': this.idEffort,
        'title': this.title,
        'type': this.type,
        'beginning': this.beginning,
        'ending': this.ending,
        'date': this.date,
        'stats': this.stats,
      };
}
