import 'dart:convert';

Perfil itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Perfil.fromMap(jsonData);
}

String itemToJson(Perfil data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Perfil {
  int id;
  String name;
  int age;
  double weight;
  double height;

  Perfil({
    this.id,
    this.name,
    this.age,
    this.weight,
    this.height,
  });

  factory Perfil.fromMap(Map<String, dynamic> json) => new Perfil(
      id: json["id"],
      name: json["name"],
      age: json["age"],
      weight: json["weight"],
      height: json["height"]);

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'age': age,
        'weight': weight,
        'height': height,
      };
}
