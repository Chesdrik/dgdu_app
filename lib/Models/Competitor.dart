import 'dart:convert';

Competitor itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Competitor.fromMap(jsonData);
}

String itemToJson(Competitor data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Competitor {
  int position;
  String name;
  String folio;
  String time;
  String speed;

  Competitor({
    this.position,
    this.name,
    this.folio,
    this.time,
    this.speed,
  });

  factory Competitor.fromMap(Map<String, dynamic> json) => new Competitor(
      position: json["position"],
      name: json["name"],
      folio: json["folio"],
      time: json["time"],
      speed: json["speed"]);

  Map<String, dynamic> toMap() => {
        'position': position,
        'name': name,
        'folio': folio,
        'time': time,
        'speed': speed,
      };
}
