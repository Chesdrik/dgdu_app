class HomeSlider {
  String title;
  String date;
  String url;
  String content;
  String image;
  String type;

  HomeSlider(
      this.title, this.date, this.url, this.content, this.image, this.type);

  Map<String, dynamic> toJson() => {
        'title': title,
        'date': date,
        'url': url,
        'content': content,
        'image': image,
        'type': type
      };
}
