import 'dart:convert';

UserRegister itemFromJson(String str) {
  final jsonData = json.decode(str);
  return UserRegister.fromMap(jsonData);
}

String itemToJson(UserRegister data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class UserRegister {
  int id;
  String name;
  String last_name;
  String second_last_name;
  String birthday;
  String curp;
  String password;
  String gender;
  String email;
  double weight;
  double height;
  String worker_number;
  String master;
  String campus;
  String career;

  UserRegister({
    this.id,
    this.name,
    this.last_name,
    this.second_last_name,
    this.birthday,
    this.curp,
    this.password,
    this.gender,
    this.email,
    this.weight,
    this.height,
    this.worker_number,
    this.master,
    this.campus,
    this.career,
  });

  factory UserRegister.fromMap(Map<String, dynamic> json) => new UserRegister(
        id: json["id"],
        name: json["name"],
        last_name: json["last_name"],
        second_last_name: json["second_last_name"],
        birthday: json["birthday"],
        curp: json["curp"],
        password: json["password"],
        gender: json["gender"],
        email: json["email"],
        weight: json["weight"].toDouble(),
        height: json["height"],
        worker_number: json["worker_number"],
        master: json["master"],
        campus: json["campus"],
        career: json["career"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'last_name': last_name,
        'second_last_name': second_last_name,
        'birthday': birthday,
        'curp': curp,
        'password': password,
        'gender': gender,
        'email': email,
        'weight': weight,
        'height': height,
        'worker_number': worker_number,
        'master': master,
        'campus': campus,
        'career': career
      };
  Map<String, dynamic> toJson() => {
        'id': this.id,
        'name': this.name,
        'last_name': this.last_name,
        'second_last_name': this.second_last_name,
        'birthday': this.birthday,
        'curp': this.curp,
        'password': this.password,
        'gender': this.gender,
        'email': this.email,
        'weight': this.weight,
        'height': this.height,
        'worker_number': this.worker_number,
        'master': this.master,
        'campus': this.campus,
        'career': this.career
      };
}
