import 'dart:convert';

Friend itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Friend.fromMap(jsonData);
}

String itemToJson(Friend data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Friend {
  int id;
  String name;
  String email;

  Friend({this.id, this.name, this.email});

  factory Friend.fromMap(Map<String, dynamic> json) => new Friend(
        id: json["id"],
        name: json["name"],
        email: json["email"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'email': email,
      };
}
