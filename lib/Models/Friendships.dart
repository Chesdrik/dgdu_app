import 'dart:convert';

import 'package:dgdu_app/Models/Friend.dart';

Friendships itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Friendships.fromMap(jsonData);
}

String itemToJson(Friendships data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Friendships {
  int pending;
  List<Friend> friends;
  List<int> favor;
  String last;
  String typeLast;

  Friendships(
      {this.pending, this.friends, this.favor, this.last, this.typeLast});

  factory Friendships.fromMap(Map<String, dynamic> json) => new Friendships(
      pending: json["pending"],
      friends: json["friends"],
      favor: json["favor"],
      last: json["last"],
      typeLast: json["typeLast"]);

  Map<String, dynamic> toMap() => {
        'pending': pending,
        'friends': friends,
        'favor': favor,
        'last': last,
        'typeLast': typeLast,
      };
}
