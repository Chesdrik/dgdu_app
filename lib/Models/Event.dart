import 'dart:convert';

Event itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Event.fromMap(jsonData);
}

String itemToJson(Event data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Event {
  int id;
  String nombre;
  String date;
  String end_date;
  bool asociated;

  Event({this.id, this.nombre, this.date, this.end_date, this.asociated});

  factory Event.fromMap(Map<String, dynamic> json) => new Event(
        id: json["id"],
        nombre: json["nombre"],
        date: json["date"],
        end_date: json["end_date"],
        asociated: json["asociated"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'nombre': nombre,
        'date': date,
        'end_date': end_date,
        'asociated': asociated,
      };
}

Category itemCategoryFromJson(String str) {
  final jsonData = json.decode(str);
  return Category.fromMapCategory(jsonData);
}

String itemCategoryToJson(Category data) {
  final dyn = data.toMapCategory();
  return json.encode(dyn);
}

class Category {
  int id;
  String nombre;
  String cod;

  Category({this.id, this.nombre, this.cod});

  factory Category.fromMapCategory(Map<String, dynamic> json) => new Category(
        id: json["id"],
        nombre: json["nombre"],
        cod: json["cod"],
      );

  Map<String, dynamic> toMapCategory() => {
        'id': id,
        'nombre': nombre,
        'cod': cod,
      };
}
