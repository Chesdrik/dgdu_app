import 'dart:convert';

Stats itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Stats.fromMap(jsonData);
}

String itemToJson(Stats data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Stats {
  String step;
  String distance;
  String speed;
  String heart_rate;
  String calories;

  Stats({
    this.step,
    this.distance,
    this.speed,
    this.heart_rate,
    this.calories,
  });

  factory Stats.fromMap(Map<String, dynamic> json) => new Stats(
        step: json["step"],
        distance: json["distance"],
        speed: json["speed"],
        heart_rate: json["heart_rate"],
        calories: json["calories"],
      );

  Map<String, dynamic> toMap() => {
        'step': step,
        'distance': distance,
        'speed': speed,
        'heart_rate': heart_rate,
        'calories': calories,
      };

  Map<String, dynamic> toJson() => {
        'step': this.step,
        'distance': this.distance,
        'speed': this.speed,
        'heart_rate': this.heart_rate,
        'calories': this.calories,
      };
}
