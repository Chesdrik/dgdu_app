class ResponseRequest {
  bool error;
  String message;

  ResponseRequest(this.error, this.message);
}
