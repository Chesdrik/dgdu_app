import 'dart:convert';

import 'package:dgdu_app/Models/Perfil.dart';

Effort itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Effort.fromMap(jsonData);
}

String itemToJson(Effort data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Effort {
  Perfil perfil;
  double min;
  String distance;
  String speed;
  String heartrate;
  String calories;
  String steps;
  String medal;
  List stepsWeek;

  Effort(
      {this.perfil,
      this.min,
      this.distance,
      this.speed,
      this.heartrate,
      this.calories,
      this.steps,
      this.medal,
      this.stepsWeek});

  factory Effort.fromMap(Map<String, dynamic> json) => new Effort(
      perfil: json["perfil"],
      min: json["min"],
      distance: json["distance"],
      speed: json["speed"],
      heartrate: json["heartrate"],
      calories: json["calories"],
      steps: json["steps"],
      medal: json["medal"],
      stepsWeek: json["stepsWeek"]);

  Map<String, dynamic> toMap() => {
        'perfil': perfil,
        'min': min,
        'distance': distance,
        'speed': speed,
        'heartrate': heartrate,
        'calories': calories,
        'steps': steps,
        'medal': medal,
        'stepsWeek': stepsWeek,
      };
}
