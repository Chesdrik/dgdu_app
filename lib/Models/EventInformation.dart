import 'dart:convert';

EventInformation itemFromJson(String str) {
  final jsonData = json.decode(str);
  return EventInformation.fromMap(jsonData);
}

String itemToJson(EventInformation data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class EventInformation {
  int id;
  String nombre;
  String date;
  int maxUser;
  int currentUser;

  EventInformation(
      {this.id, this.nombre, this.date, this.maxUser, this.currentUser});

  factory EventInformation.fromMap(Map<String, dynamic> json) =>
      new EventInformation(
          id: json["id"],
          nombre: json["nombre"],
          date: json["date"],
          maxUser: json["maxUser"],
          currentUser: json["currentUser"]);

  Map<String, dynamic> toMap() => {
        'id': id,
        'nombre': nombre,
        'date': date,
        'maxUser': maxUser,
        'currentUser': currentUser
      };
}
