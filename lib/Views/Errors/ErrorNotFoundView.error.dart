import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ErrorNotFoundView extends StatefulWidget {
  @override
  _ErrorNotFoundViewState createState() => _ErrorNotFoundViewState();
}

class _ErrorNotFoundViewState extends State<ErrorNotFoundView> {
  String error = 'assets/error.svg';
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        endDrawer: AppDrawer(),
        appBar: globalAppBar(context, 'dashboard', 'Error', true),
        body: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Error de conexión',
                  textScaleFactor: 3,
                  style: TextStyle(color: Colors.black),
                ),
                SvgPicture.asset(
                  error,
                  semanticsLabel: 'error',
                  width: 200,
                ),
                Text(
                  'Verifica tu conexión a internet',
                  textScaleFactor: 1.5,
                  style: TextStyle(color: Colors.black),
                ),
              ],
            ),
          ),
        ),
      ),
      onWillPop: () => Navigator.pushNamed(context, '/'),
    );
  }
}
