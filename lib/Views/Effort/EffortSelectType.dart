import 'package:dgdu_app/Models/Arguments.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/globalsRegister.dart' as globalsRegister;
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class EffortSelectType extends StatefulWidget {
  @override
  _EffortSelectTypeState createState() => _EffortSelectTypeState();
}

class _EffortSelectTypeState extends State<EffortSelectType> {
  String corriendo = 'assets/corriendo.svg';
  String cinta = 'assets/cinta.svg';
  String bicicleta = 'assets/bicicleta.svg';
  String pesas = 'assets/pesas.svg';
  String fija = 'assets/fija.svg';
  String remo = 'assets/remo.svg';
  String saltar = 'assets/saltar.svg';

  @override
  Widget build(BuildContext context) {
    final Arguments args = ModalRoute.of(context).settings.arguments;
    globalsRegister.args = args;
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Seleccionar tipo', true),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/fondo_pista.jpg"), fit: BoxFit.cover),
        ),
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 10.0,
                    primary: globals.blue.withOpacity(0.85),
                    minimumSize: Size(double.infinity, 100),
                    textStyle: const TextStyle(fontSize: 20)),
                onPressed: () {
                  Navigator.pushNamed(context, '/effort/select/activity',
                      arguments:
                          Arguments(args.type, 0)); // 0 actividad exterior
                },
                child: ListTile(
                  leading: Icon(
                    Icons.park,
                    color: globals.gold,
                    size: 30.0,
                  ),
                  title: Text('Actividad exterior',
                      style: TextStyle(color: Colors.white)),
                ),
              ),
              Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 10.0,
                    primary: globals.blue.withOpacity(0.85),
                    minimumSize: Size(double.infinity, 100),
                    textStyle: const TextStyle(fontSize: 20)),
                onPressed: () {
                  Navigator.pushNamed(context, '/effort/select/activity',
                      arguments:
                          Arguments(args.type, 1)); //1 actividad interior
                },
                child: ListTile(
                  leading: Icon(
                    Icons.house,
                    color: globals.gold,
                    size: 30.0,
                  ),
                  title: Text('Actividad interior',
                      style: TextStyle(color: Colors.white)),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
