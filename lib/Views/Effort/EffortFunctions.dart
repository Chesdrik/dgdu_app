import 'dart:async';
import 'dart:convert';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import "package:latlong/latlong.dart" as latLng;
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/globalsRegister.dart' as globalsRegister;
import 'package:http/http.dart' as http;
import 'package:dgdu_app/Models/Effort.dart';
import 'package:dgdu_app/Models/EffortRegister.dart';
import 'package:dgdu_app/Models/Perfil.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:dgdu_app/bd/UserBD.dart';

Future<void> updatePerfilData(context, UserRegister user) async {
  try {
    Uri uri = globals.uriConstructor('/update_user_data');

    var data = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': user.email,
        'password': user.password,
      }),
    );

    var jsonData = json.decode(data.body);
    // print(jsonData);

    if (jsonData['error']) {
      // Error in server response
      // print(jsonData('error_message'));
      Navigator.pushNamed(context, '/error');
    } else {
      // No error, update user data
      await DBProvider.db.updateUserData(
        user.id,
        jsonData['user_data']['name'],
        jsonData['user_data']['last_name'],
        jsonData['user_data']['second_last_name'],
        jsonData['user_data']['weight'],
        jsonData['user_data']['height'],
      );
    }
  } catch (e) {
    print(e);
    Navigator.pushNamed(context, '/error');
  }
}

/// Get the current information about user.
///
/// {@category perfilInformation}
/// {@subCategory Information displays}
Future<Effort> getPerfilInfo(context) async {
  print('Fetch!');
  // Checking if user is logged in
  bool login = await conf();

  // Define request body
  var body;

  print('/effort/detail');
  print(login);

  if (!login) {
    String deviceId = await getDeviceId();
    // print('get deviceId');
    print("Fetch using device_id = " + deviceId);

    body = jsonEncode(<String, String>{
      'device_id': deviceId,
    });
  } else {
    print("User id: " + globals.user.id.toString());
    body = jsonEncode(<String, String>{
      'user_id': globals.user.id.toString(),
    });
  }
  // print(body);
  try {
    if (login) {
      // Update user data from server
      // print('Fetching data for user');
      UserRegister _user = await DBProvider.db.getUserConfig();
      await updatePerfilData(context, _user);
      // print('Updating');
    }

    // Fetching data
    Uri uri = globals.uriConstructor('/effort/detail');
    var data = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: body,
    );

    var jsonData = json.decode(data.body);

    print("jsonData: ");
    print(jsonData);

    if (jsonData == null) {
      // print("entre aqui con json data null");
      Perfil perfil = new Perfil(id: 0, name: "", age: 0, weight: 0, height: 0);
      Effort effort = new Effort(
          perfil: perfil,
          min: 0.0,
          distance: "",
          speed: "",
          heartrate: "",
          calories: "",
          steps: "",
          stepsWeek: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);

      // getAllUserRegisters

      globals.errorResponse = true;
      return effort;
    } else {
      if (jsonData["error"]) {
        print("No user found...");

        Perfil perfil =
            new Perfil(id: 0, name: "", age: 0, weight: 0, height: 0);
        Effort effort = new Effort(
            perfil: perfil,
            min: 0.0,
            distance: "",
            speed: "",
            heartrate: "",
            calories: "",
            steps: "",
            stepsWeek: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);
        globals.errorResponse = true;
        return effort;
      } else {
        // Defining details variable with json data on key 'details
        var details = jsonData['details'];

        // Fetch user and update data on local db
        UserRegister aux = await DBProvider.db.getUserConfig();

        // Preparing variables
        List steps = [];
        int edad = details['age'];
        double weight = double.parse(details['weight']);
        double height = double.parse(details['height']);

        if (aux != null) {
          // Updating aux local variable
          aux.name = jsonData['name_detail']['name'];
          aux.last_name = jsonData['name_detail']['last_name'];
          aux.second_last_name = jsonData['name_detail']['second_last_name'];
          aux.weight = weight;
          aux.height = height;

          // Updating DB
          await DBProvider.db.updateUserRegister(aux);
          // await DBProvider.db.getAllUserRegisters();
        }

        // print(details['tiempo']);
        Perfil perfil = new Perfil(
          id: details['id'],
          name: details['name'],
          age: edad,
          weight: weight,
          height: height,
        );

        //change the data to double
        for (var s in details['tiempo']) {
          if (s.runtimeType == int) {
            var aux = s.toDouble();
            steps.add(aux);
          } else if (s.runtimeType == String) {
            var aux = double.parse(s);
            steps.add(aux);
          } else {
            steps.add(s);
          }
        }

        // print(steps);
        // print(jsonData['Tiempo'].runtimeType);
        var time = (jsonData['Tiempo'] != null
            ? double.parse(jsonData['Tiempo'])
            : 0.0);
        Effort effort = new Effort(
            perfil: perfil,
            // min: details['min'],
            min: (jsonData['Tiempo'] != null
                ? double.parse(jsonData['Tiempo'])
                : 0.0),
            // distance: details['distance'].toString(),
            distance: (jsonData['Distancia'] != null
                ? jsonData['Distancia'].toString()
                : ""),
            speed: '0',
            // heartrate: details['heart_rate'].toString(),
            heartrate: '0',
            // calories: details['calories'].toString(),
            calories: (0.0175 * perfil.weight * time).toStringAsFixed(2),
            // steps: details['step'].toString(),
            steps: "",
            medal: details['medal'],
            stepsWeek: steps);

        globals.errorResponse = false;
        return effort;
      }
    }
  } catch (e) {
    print(e);
    // Navigator.pushNamed(context, '/error');
  }
}

/// Send the current information about a effort type user.
/// Receives register effort data to display
///
/// {@category effort}
/// {@subCategory Information effort displays}
Future<bool> sendEffort(EffortRegister effortRegister) async {
  Uri uri = globals.uriConstructor('/effort/register');

  bool login = await conf();

  if (!login) {
    bool response = await sendEffortWithOutLogin(effortRegister);
    return response;
  } else {
    // String event = '';

    var body;
    if (globalsRegister.args.type == 'event') {
      String event = globalsRegister.args.id.toString();
      body = jsonEncode(<String, String>{
        'user_id': globals.user.id.toString(),
        'effort_type_id': effortRegister.idEffort.toString(),
        'effort_start': effortRegister.beginning,
        'effort_end': effortRegister.ending,
        'distance': effortRegister.stats.distance,
        'heart_rate': effortRegister.stats.heart_rate,
        'calories': effortRegister.stats.calories,
        'step': effortRegister.stats.step,
        'speed': effortRegister.stats.speed,
        'event_id': event,
      });
    } else {
      body = jsonEncode(<String, String>{
        'user_id': globals.user.id.toString(),
        'effort_type_id': effortRegister.idEffort.toString(),
        'effort_start': effortRegister.beginning,
        'effort_end': effortRegister.ending,
        'distance': effortRegister.stats.distance,
        'heart_rate': effortRegister.stats.heart_rate,
        'calories': effortRegister.stats.calories,
        'step': effortRegister.stats.step,
        'speed': effortRegister.stats.speed,
      });
    }

    // print(body);

    // print('tipo de registro: ' + globalsRegister.args.type);

    var response = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: body,
    );

    var jsonData = json.decode(response.body);

    if (response.statusCode == 200) {
      // print(jsonData);
      return true;
    } else {
      globals.message = response.statusCode.toString();
      // print('Request failed with status: ${response.statusCode}.');
      return false;
    }
  }
}

/// Send the current information about a effort type user.
/// Receives register effort data to display
///
/// {@category effort}
/// {@subCategory Information effort displays}
Future<Map<String, dynamic>> saveEffort(
    Map<String, latLng.LatLng> routeTracking,
    int effortTypeId,
    String effortStart,
    String effortEnd,
    String totalTime,
    double distance) async {
  Map<String, String> gpsLocations = {};

  bool login = await conf();

  // print("Starting forEach");
  routeTracking.forEach((key, value) {
    gpsLocations[key] =
        value.latitude.toString() + ", " + value.longitude.toString();
  });

  // print(jsonEncode(gpsLocations));

  var gps = jsonEncode(gpsLocations);

  Uri uri = globals.uriConstructor('/effort/register');

  String event = '';
  var body;
  if (!login) {
    var response = await saveEffortWithOutLogin(
        gps, effortTypeId, effortStart, effortEnd, totalTime, distance);
    return response;
  } else {
    if (globalsRegister.args.type == 'event') {
      event = globalsRegister.args.id.toString();
      body = jsonEncode(<String, String>{
        'user_id': globals.user.id.toString(),
        'effort_type_id': effortTypeId.toString(),
        'effort_start': effortStart,
        'effort_end': effortEnd,
        // 'time': totalTime,
        'distance': double.parse(distance.toStringAsExponential(3)).toString(),
        // 'gps_locations': gps,
        'event_id': event,
      });
    } else {
      body = jsonEncode(<String, String>{
        'user_id': globals.user.id.toString(),
        'effort_type_id': effortTypeId.toString(),
        'effort_start': effortStart,
        'effort_end': effortEnd,
        // 'time': totalTime,
        'distance': double.parse(distance.toStringAsExponential(3)).toString(),
        // 'gps_locations': gps,
      });
    }

    var response = await http.post(uri,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    Map<String, dynamic> jsonData = json.decode(response.body);
    // print(jsonData);

    return jsonData;
  }
}

String formatTime(int milliseconds) {
  var secs = milliseconds ~/ 1000;
  var hours = (secs ~/ 3600).toString().padLeft(2, '0');
  var minutes = ((secs % 3600) ~/ 60).toString().padLeft(2, '0');
  var seconds = (secs % 60).toString().padLeft(2, '0');
  return "$hours:$minutes:$seconds";
}

double geolocatorDistance(latLng.LatLng start, latLng.LatLng end) {
  // Using geolocator to calculate distance
  return Geolocator.distanceBetween(
      start.latitude, start.longitude, end.latitude, end.longitude);
}

Widget activeEffort(_effortStartDateTime, _effortStart, _distance,
    _altitudeStart, _altitudeCurrent, _activeTracking) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(
              _activeTracking
                  ? "Registrando tiempo y ubicación de tu esfuerzo"
                  : "Registrando tiempo de tu esfuerzo",
              style: TextStyle(fontSize: 16.0),
              textAlign: TextAlign.center,
            ),
            TimerRunning(70.0),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "Datos registrados:",
          style: TextStyle(fontSize: 16.0, color: globals.blue),
        ),
      ),
      SizedBox(
        height: 20,
      ),
      // Text(_effortStartDateTime.toString()),
      // Text(globalsRegister.args.id.toString()),
      (_effortStartDateTime == null
          ? distanceRow(_distance)
          : Row(
              children: [
                Spacer(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Inicio ",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      _effortStart,
                      style: TextStyle(
                          fontSize: 20, color: globals.blue.withOpacity(0.75)),
                    ),
                  ],
                ),
                Spacer(),
                globalsRegister.args.id == 0
                    ? distanceRow(_distance)
                    : SizedBox(
                        width: 0,
                      ),
                globalsRegister.args.id == 0
                    ? Spacer()
                    : SizedBox(
                        width: 0,
                      ),
              ],
            )),
      SizedBox(
        height: 20,
      ),
      Row(
        children: [
          Spacer(),
          altitudeRow("Altitud inicial", _altitudeStart),
          Spacer(),
          altitudeRow("Altitud actual", _altitudeCurrent),
          Spacer(),
        ],
      ),
      SizedBox(
        height: 30,
      ),
    ],
  );
}

Widget finishedEffort(
    _effortStartDateTime,
    _effortStart,
    _effortEndDateTime,
    _effortEnd,
    _totalTime,
    _speed,
    _distance,
    _altitudeStart,
    _altitudeEnd,
    _activeTracking,
    {bool login,
    double calories}) {
  // print('finishedEffort');

  return Padding(
    padding: const EdgeInsets.all(1.0),
    child: (_effortStart == null
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Da click en el botón para iniciar el registro de tu esfuerzo",
              style: TextStyle(fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Spacer(),
                          Column(
                            children: [
                              Text(
                                "Inicio ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(_effortStart,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: globals.blue.withOpacity(0.75))),
                            ],
                          ),
                          Spacer(),
                          Column(
                            children: [
                              Text(
                                "Fin ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(_effortEnd,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: globals.blue.withOpacity(0.75))),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      (_activeTracking
                          ? Row(
                              children: [
                                Spacer(),
                                Column(
                                  children: [
                                    Text(
                                      "Tiempo total ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(_totalTime,
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: globals.blue
                                                .withOpacity(0.75))),
                                  ],
                                ),
                                Spacer(),
                                altitudeRow("Altitud", _altitudeStart),
                                Spacer(),
                              ],
                            )
                          : altitudeRow("Altitud", _altitudeStart)),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Spacer(),
                          Column(
                            children: [
                              Text(
                                "Tiempo total ",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(_totalTime,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: globals.blue.withOpacity(0.75))),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          if (_activeTracking) distanceRow(_distance),
                          if (_activeTracking) speedRow(_speed),
                          if (login) caloriesRow(calories),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
  );
}

Widget distanceRow(_distance) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        "Distancia",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      Row(
        children: [
          Text(
            double.parse(_distance.toStringAsExponential(2)).toString(),
            style:
                TextStyle(fontSize: 20, color: globals.blue.withOpacity(0.75)),
          ),
          Text(
            ' m',
            style:
                TextStyle(fontSize: 20, color: globals.blue.withOpacity(0.75)),
          ),
        ],
      )
    ],
  );
}

Widget speedRow(_sepeed) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        "Velocidad",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      Row(
        children: [
          Text(
            ms2kmh(_sepeed).toStringAsFixed(2),
            style:
                TextStyle(fontSize: 20, color: globals.blue.withOpacity(0.75)),
          ),
          Text(
            ' km/h',
            style:
                TextStyle(fontSize: 20, color: globals.blue.withOpacity(0.75)),
          ),
        ],
      ),
    ],
  );
}

Widget caloriesRow(_calories) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
        children: [
          Text(
            "Calorías",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Row(
            children: [
              Text(
                _calories.toStringAsFixed(2),
                style: TextStyle(
                    fontSize: 20, color: globals.blue.withOpacity(0.75)),
              ),
              Text(
                ' cal',
                style: TextStyle(
                    fontSize: 20, color: globals.blue.withOpacity(0.75)),
              ),
            ],
          ),
        ],
      )
    ],
  );
}

Future<bool> sendEffortWithOutLogin(EffortRegister effortRegister) async {
  Uri uri = globals.uriConstructor('/effort/register');
  var body;
  body = jsonEncode(<String, String>{
    'device_id': await getDeviceId(),
    'effort_type_id': effortRegister.idEffort.toString(),
    'effort_start': effortRegister.beginning,
    'effort_end': effortRegister.ending,
    'distance': effortRegister.stats.distance,
    'heart_rate': effortRegister.stats.heart_rate,
    'calories': effortRegister.stats.calories,
    'step': effortRegister.stats.step,
    'speed': effortRegister.stats.speed,
  });
  var response = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: body,
  );
  if (response == null) {
    return false;
  } else {
    if (response.statusCode == 200) {
      return true;
    } else {
      globals.message = response.statusCode.toString();
      // print('Request failed with status: ${response.statusCode}.');
      return false;
    }
  }
}

Future<Map<String, dynamic>> saveEffortWithOutLogin(
    var routeTracking,
    int effortTypeId,
    String effortStart,
    String effortEnd,
    String totalTime,
    double distance) async {
  var body;
  Uri uri = globals.uriConstructor('/effort/register');
  body = jsonEncode(<String, String>{
    'device_id': await getDeviceId(),
    'effort_type_id': effortTypeId.toString(),
    'effort_start': effortStart,
    'effort_end': effortEnd,
    // 'time': totalTime,
    'distance': double.parse(distance.toStringAsExponential(3)).toString(),
    // 'gps_locations': routeTracking,
  });
  var response = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: body,
  );
  if (response == null) {
    return null;
  } else {
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonData = json.decode(response.body);
      // print(jsonData);

      return jsonData;
    } else {
      globals.message = response.statusCode.toString();
      // print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }
}

Widget altitudeRow(String _title, double _altitude) {
  var f = NumberFormat("###,###.##", "en_US");

  return (_altitude != null
      ? Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              _title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Row(
              children: [
                Text(
                  f.format(_altitude).toString(),
                  style: TextStyle(
                      fontSize: 20, color: globals.blue.withOpacity(0.75)),
                ),
                Text(
                  ' m',
                  style: TextStyle(
                      fontSize: 20, color: globals.blue.withOpacity(0.75)),
                ),
              ],
            ),
          ],
        )
      : SizedBox(
          width: 0,
        ));
}

double ms2kmh(double ms) {
  return ms * 3.6;
}
