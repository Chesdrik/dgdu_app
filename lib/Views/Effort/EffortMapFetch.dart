import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Views/Effort/EffortTrackScreen.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class EffortMapFetch extends StatefulWidget {
  EffortMapFetch({Key key}) : super(key: key);

  @override
  EffortMapFetchState createState() => new EffortMapFetchState();
}

class EffortMapFetchState extends State<EffortMapFetch> {
  @override
  Widget build(BuildContext context) {
    final Arguments args = ModalRoute.of(context).settings.arguments;
    // print("DEBUG: args: " + args.id.toString());

    // Define activities with GPS tracking
    List<int> gpsTracking = [
      1,
      3,
      7,
      8,
    ]; // Remos y remadora son lo mismo? id: 5,

    // Check permissions before page load
    if (gpsTracking.contains(args.id)) {
      // Check GPS permissions
      return FutureBuilder(
          future: _getCurrentLocation(),
          builder: (BuildContext context, AsyncSnapshot info) {
            return (info.data == null
                ? Loading()
                : EffortTrackScreen(
                    currentPosition: info.data,
                    args: args,
                  ));
          });
    } else {
      // print("DEBUG: No tracking required");
      return EffortTrackScreen(
        currentPosition: null,
        args: args,
      );
    }
  }

  Future<Position> _getCurrentLocation() async {
    // Fetching permissions
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    LocationPermission permission = await Geolocator.checkPermission();

    // Checking if GPS service is enabled
    print("serviceEnabled: " + serviceEnabled.toString());
    if (!serviceEnabled) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    }

    // Redirecting user to home if location permissions is not always enabled
    print("permission: " + permission.toString());
    if (permission != LocationPermission.always) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    }

    // Fetching current position and returning it
    Position pos = await Geolocator.getCurrentPosition();
    return pos;

    // // Return user to home if location is denied forever
    // if (permission == LocationPermission.deniedForever) {
    //   Navigator.of(context)
    //       .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    //   // return Future.error(
    //   //     'Location permissions are permantly denied, we cannot request permissions.');
    // }

    // // If permission is denied, we ask user to enable it
    // if (permission == LocationPermission.denied) {
    //   permission = await Geolocator.requestPermission();
    //   if (permission != LocationPermission.whileInUse) {
    //     // Navigator.popUntil(context, ModalRoute.withName('/'));

    //     // Navigator.pushNamed(context, '/enable_location');
    //     Navigator.of(context)
    //         .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);

    //     return Future.error(
    //         'Location permissions are denied (actual value: $permission).');
    //   }
    // }

    //  permission != LocationPermission.always
    // Position pos = await Geolocator.getCurrentPosition();

    // return pos;
  }
}
