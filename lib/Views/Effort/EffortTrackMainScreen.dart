import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'dart:ui';

import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EffortTrackMainScreen extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(
      builder: (context) => EffortTrackMainScreen(),
    );
  }

  @override
  _EffortTrackMainScreenState createState() => _EffortTrackMainScreenState();
}

class _EffortTrackMainScreenState extends State<EffortTrackMainScreen> {
  String corriendo = 'assets/corriendo.svg';
  String cinta = 'assets/cinta.svg';
  String bicicleta = 'assets/bicicleta.svg';
  String pesas = 'assets/pesas.svg';
  String fija = 'assets/fija.svg';
  String remo = 'assets/remo.svg';
  String saltar = 'assets/saltar.svg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        bottomOpacity: 0,
        elevation: 0,
        title: Text(
          'Registrar entrenamiento',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: globals.blue,
      ),
      backgroundColor: globals.blue,
      body: effort(),
    );
  }

  Widget effort() {
    return Padding(
        padding: EdgeInsets.all(1),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 0,
              ),
              Expanded(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: 1,
                  physics: ScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Card(
                          color: globals.blue,
                          child: ListTile(
                            leading: SvgPicture.asset(corriendo,
                                color: globals.gold, semanticsLabel: 'carrera'),
                            title: Text('Carrera',
                                style: TextStyle(color: Colors.white)),
                            onTap: () {
                              Navigator.pushNamed(context, '/effort/track');
                            },
                          ),
                        ),
                        Card(
                          color: globals.blue,
                          child: ListTile(
                            leading: SvgPicture.asset(cinta,
                                color: globals.gold, semanticsLabel: 'cinta'),
                            title: Text('Banda',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                        // Card(
                        //   color: globals.blue,
                        //   child: ListTile(
                        //     leading: SvgPicture.asset(bicicleta,
                        //         color: globals.gold,
                        //         semanticsLabel: 'bicicleta'),
                        //     title: Text('Bicicleta',
                        //         style: TextStyle(color: Colors.white)),
                        //   ),
                        // ),
                        Card(
                          color: globals.blue,
                          child: ListTile(
                            leading: SvgPicture.asset(bicicleta,
                                color: globals.gold, semanticsLabel: 'fija'),
                            title: Text('Bicicleta fija',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                        // Card(
                        //   color: globals.blue,
                        //   child: ListTile(
                        //     leading: SvgPicture.asset(
                        //       remo,
                        //       color: globals.gold,
                        //       semanticsLabel: 'remo',
                        //       height: 70,
                        //     ),
                        //     title: Text('Remadora',
                        //         style: TextStyle(color: Colors.white)),
                        //   ),
                        // ),
                        Card(
                          color: globals.blue,
                          child: ListTile(
                            leading: SvgPicture.asset(pesas,
                                color: globals.gold, semanticsLabel: 'pesas'),
                            title: Text('Entrenamiento',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                        Card(
                          color: globals.blue,
                          child: ListTile(
                            leading: SvgPicture.asset(saltar,
                                color: globals.gold, semanticsLabel: 'saltar'),
                            title: Text('Acondicionamiento físico',
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ]));
  }
}
