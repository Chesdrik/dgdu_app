import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/globalsRegister.dart' as globalsRegister;
import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class EffortMainScreen extends StatefulWidget {
  @override
  _EffortMainScreenState createState() => _EffortMainScreenState();
}

class _EffortMainScreenState extends State<EffortMainScreen> {
  String corriendo = 'assets/corriendo.svg';
  String cinta = 'assets/cinta.svg';
  String bicicleta = 'assets/bicicleta.svg';
  String pesas = 'assets/pesas.svg';
  String fija = 'assets/fija.svg';
  String remadora = 'assets/remo.svg';
  String saltar = 'assets/saltar.svg';
  String canotaje = 'assets/canotaje.svg';
  String remos = 'assets/remos.svg';

  @override
  Widget build(BuildContext context) {
    final Arguments args = ModalRoute.of(context).settings.arguments;
    globalsRegister.args = args;

    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Seleccionar tipo', true),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                // image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
                image: AssetImage("assets/fondo_pista.jpg"),
                fit: BoxFit.cover)),
        child: effort(args),
      ),
    );
  }

  Widget effort(Arguments args) {
    return Padding(
      padding: EdgeInsets.all(6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                args.id == 0
                    ? activityWidget("carrera", corriendo)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 0
                    ? activityWidget("bicicleta", bicicleta)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 0
                    ? activityWidget("canotaje", canotaje)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 0
                    ? activityWidget("remos", remos)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 1
                    ? activityWidget("banda", cinta)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 1
                    ? activityWidget("banda_fija", bicicleta)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 1
                    ? activityWidget("remadora", remadora)
                    : SizedBox(
                        width: 0,
                      ),
                args.id == 1
                    ? activityWidget("pesas", pesas)
                    : SizedBox(
                        width: 0,
                      ),
                activityWidget("acondicionamiento", saltar)
              ],
            ),
            // },
            // ),
          ),
        ],
      ),
    );
  }

  Widget activityWidget(String _type, String _activityIcon) {
    String _activityTitle = '';
    String _pushNamedRoute = "/effort/register/activity";
    int _argumentId = 0;

    if (_type == 'carrera') {
      _activityTitle = "Carrera";
      _pushNamedRoute = "/effort/track";
      _argumentId = 1;
    } else if (_type == 'banda') {
      _activityTitle = "Banda";
      _pushNamedRoute = "/effort/track";
      _argumentId = 2;
    } else if (_type == 'bicicleta') {
      _activityTitle = "Bicicleta";
      _pushNamedRoute = "/effort/track";
      _argumentId = 3;
    } else if (_type == 'banda_fija') {
      _activityTitle = "Bicicleta fija";
      _pushNamedRoute = "/effort/track";
      _argumentId = 4;
    } else if (_type == 'remadora') {
      _activityTitle = "Remadora";
      _pushNamedRoute = "/effort/track";
      _argumentId = 5;
    } else if (_type == 'pesas') {
      _activityTitle = "Entrenamiento";
      _pushNamedRoute = "/effort/track";
      _argumentId = 6;
    } else if (_type == 'acondicionamiento') {
      _activityTitle = "Acondicionamiento físico";
      _pushNamedRoute = "/effort/track";
      _argumentId = 7;
    } else if (_type == 'canotaje') {
      _activityTitle = "Canotaje";
      _pushNamedRoute = "/effort/track";
      _argumentId = 8;
    } else if (_type == 'remos') {
      _activityTitle = "Remos";
      _pushNamedRoute = "/effort/track";
      _argumentId = 5;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: Card(
        elevation: 10.0,
        color: globals.blue.withOpacity(0.85),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: ListTile(
            leading: SvgPicture.asset(_activityIcon,
                color: globals.gold, semanticsLabel: _activityTitle, width: 70),
            title: Text(_activityTitle, style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.pushNamed(context, _pushNamedRoute,
                  arguments: Arguments(_activityTitle, _argumentId));
            },
          ),
        ),
      ),
    );
  }
}
