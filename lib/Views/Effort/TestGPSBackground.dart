import 'dart:async';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import "package:latlong/latlong.dart" as latLng;
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:dgdu_app/Views/Effort/Location/enable_in_background.dart';
// import 'package:dgdu_app/Views/Effort/Location/listen_location.dart';

// import 'package:url_launcher/url_launcher.dart';
// import 'package:dgdu_app/Views/Effort/enable_in_background.dart';
// import 'package:dgdu_app/Views/Effort/location_plugin/change_notification.dart';
// import 'package:dgdu_app/Views/Effort/location_plugin/get_location.dart';
// import 'package:dgdu_app/Views/Effort/location_plugin/listen_location.dart';
// import 'package:dgdu_app/Views/Effort/location_plugin/permission_status.dart';
// import 'package:dgdu_app/Views/Effort/location_plugin/service_enabled.dart';

class TestGPSBackground extends StatefulWidget {
  @override
  _TestGPSBackgroundState createState() => _TestGPSBackgroundState();
}

class _TestGPSBackgroundState extends State<TestGPSBackground> {
  // View data
  var _currentLocation;

  // Location routeTracking
  Map<String, latLng.LatLng> routeTracking = {};

  // Location services
  Location location = new Location();
  LocationData _location;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  // Location suscription
  StreamSubscription<LocationData> _locationSubscription;
  String _error;

  @override
  void initState() {
    // TODO: implement initState
    // askLocationPermissions();
    super.initState();
  }

  askLocationPermissions() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    location.enableBackgroundMode(enable: true);
    location.onLocationChanged.listen((LocationData currentLocation) {
      // print(currentLocation.toString());
      // Use current location
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: globalAppBar(context, 'register_effort'),
            backgroundColor: globals.blue,
            body: Container(
              // height: MediaQuery.of(context).size.height / 3,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/fondo_gris.jpg"),
                      fit: BoxFit.cover)),
              child: Center(
                child: Column(children: [
                  Container(
                    child: Text('yes'),
                  ),
                  EnableInBackgroundWidget(),
                  // ListenLocationWidget(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Listen location: ' +
                            (_error ?? '${_location ?? "unknown"}'),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 42),
                            child: ElevatedButton(
                              child: const Text('Listen'),
                              onPressed: _listenLocation,
                            ),
                          ),
                          ElevatedButton(
                            child: const Text('Stop'),
                            onPressed: _stopListen,
                          )
                        ],
                      ),
                    ],
                  ),
                  listLatLngElements(),
                ]),
              ),
            )));
  }

  Future<void> _stopListen() async {
    _locationSubscription.cancel();
  }

  Future<void> _listenLocation() async {
    _locationSubscription =
        location.onLocationChanged.handleError((dynamic err) {
      setState(() {
        _error = err.code;
      });
      _locationSubscription.cancel();
    }).listen((LocationData currentLocation) {
      setState(() {
        _error = null;

        var _currentDate =
            DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now());
        this.routeTracking[_currentDate] = this._currentLocation;

        _location = currentLocation;
      });
    });
  }

  Widget listLatLngElements() {
    // print('listing gps positions');
    if (this.routeTracking.length > 0) {
      return ListView.builder(
          itemCount: this.routeTracking.length,
          itemBuilder: (BuildContext context, int index) {
            int rev_index = this.routeTracking.length - index - 1;

            String _date =
                this.routeTracking.keys.elementAt(rev_index).toString();
            String _latlong = this
                    .routeTracking
                    .values
                    .elementAt(rev_index)
                    .latitude
                    .toString() +
                ", " +
                this
                    .routeTracking
                    .values
                    .elementAt(rev_index)
                    .longitude
                    .toString();

            return new Column(
              children: <Widget>[
                new ListTile(
                    title: new Text(_date), subtitle: new Text(_latlong)),
              ],
            );
          });
    } else {
      return Loading();
    }
  }
}
