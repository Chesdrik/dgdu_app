import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'dart:ui';
import 'package:dgdu_app/Views/globalViews.dart';

import 'package:flutter/rendering.dart';

class EffortSuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: globalAppBar(context, 'generic'),
      backgroundColor: globals.blue,
      body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/fondo_gris.jpg"),
                  fit: BoxFit.cover)),
          child: SafeArea(
            child: Column(children: [
              Spacer(),
              Padding(
                  padding: EdgeInsets.all(1),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Tu entrenamiento ha sido guardado",
                          style: TextStyle(fontSize: 25.0, color: globals.blue),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(globals.blue),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                          child: Text('Salir',
                              style:
                                  TextStyle(fontSize: 20, color: globals.gold)),
                          onPressed: () {
                            // Navigator.pushNamed(context, '/');
                            Navigator.popUntil(
                                context, ModalRoute.withName('/'));
                          },
                        ),
                      ])),
              Spacer(flex: 3),
            ]),
          )),
    );
  }
}
