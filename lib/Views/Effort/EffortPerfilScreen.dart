import 'package:dgdu_app/Models/Effort.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Views/Widgets/Graph.dart';
import 'package:flutter/foundation.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

// ignore: must_be_immutable
class EffortPerfilScreen extends StatefulWidget {
  Effort effort;

  EffortPerfilScreen({Key key, this.effort}) : super(key: key);

  @override
  _EffortPerfilScreenState createState() => _EffortPerfilScreenState();
}

class _EffortPerfilScreenState extends State<EffortPerfilScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'dashboard', 'Tu esfuerzo', true),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
        ),
        child: SingleChildScrollView(
          child: efforData(),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButton: startEffortButton(context, 'effort'),
      floatingActionButton: startEffortButton(context, 'dashboard'),
    );
  }

  Widget efforData() {
    return Container(
      child: Padding(
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            globals.errorResponse
                ? Text('DATOS NO ENCONTRADOS',
                    style: TextStyle(color: globals.blue))
                : Text(
                    'DATOS BÁSICOS DE: ' +
                        widget.effort.perfil.name.toUpperCase(),
                    style: TextStyle(color: globals.blue),
                  ),
            Divider(
              color: globals.gold,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'Edad',
                      style: TextStyle(color: globals.blue),
                    ),
                    Text(
                      widget.effort.perfil.age.toString(),
                      style: TextStyle(color: globals.blue.withOpacity(0.80)),
                    )
                  ],
                ),
                VerticalDivider(),
                Column(
                  children: <Widget>[
                    Text(
                      'Peso',
                      style: TextStyle(color: globals.blue),
                    ),
                    Text(
                      widget.effort.perfil.weight.toString() + ' Kg',
                      style: TextStyle(color: globals.blue.withOpacity(0.80)),
                    )
                  ],
                ),
                VerticalDivider(),
                Column(
                  children: <Widget>[
                    Text(
                      'Estatura',
                      style: TextStyle(color: globals.blue),
                    ),
                    Text(
                      widget.effort.perfil.height.toStringAsFixed(2) + ' M',
                      style: TextStyle(color: globals.blue.withOpacity(0.80)),
                    )
                  ],
                ),
              ],
            ),
            Divider(
              color: globals.gold,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'Cal',
                      style: TextStyle(color: globals.blue),
                    ),
                    Text(
                      widget.effort.calories,
                      style: TextStyle(color: globals.blue.withOpacity(0.80)),
                    )
                  ],
                ),
                VerticalDivider(),
                Column(
                  children: <Widget>[
                    Text(
                      'Km',
                      style: TextStyle(color: globals.blue),
                    ),
                    Text(
                      widget.effort.distance + ' Km',
                      style: TextStyle(color: globals.blue.withOpacity(0.80)),
                    )
                  ],
                ),
                VerticalDivider(),
                Column(
                  children: <Widget>[
                    Text(
                      'Min. Actividad',
                      style: TextStyle(color: globals.blue),
                    ),
                    Text(
                      widget.effort.min.toString() + ' min',
                      style: TextStyle(color: globals.blue.withOpacity(0.80)),
                    )
                  ],
                ),
              ],
            ),
            Divider(
              color: globals.gold,
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: <Widget>[
            //     Column(
            //       children: <Widget>[
            //         Text(
            //           'Pasos',
            //           style: TextStyle(color: globals.blue),
            //         ),
            //         Text(
            //           widget.effort.steps.toString(),
            //           style: TextStyle(color: globals.blue.withOpacity(0.80)),
            //         )
            //       ],
            //     ),
            //     VerticalDivider(),
            //     Column(
            //       children: <Widget>[
            //         Text(
            //           'Frecuencia cardiaca',
            //           style: TextStyle(color: globals.blue),
            //         ),
            //         Text(
            //           widget.effort.heartrate,
            //           style: TextStyle(color: globals.blue.withOpacity(0.80)),
            //         )
            //       ],
            //     ),
            //   ],
            // ),
            SizedBox(
              height: 20,
            ),
            widget.effort.medal == null
                ? SizedBox(
                    width: 0,
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(
                            'La medalla que alcanzaste:',
                            style: TextStyle(color: globals.blue),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Column(
                        children: <Widget>[
                          Image.asset(
                            'assets/' + widget.effort.medal + '.png',
                            width: (MediaQuery.of(context).size.width * 0.25),
                          ),
                        ],
                      ),
                    ],
                  ),
            widget.effort.medal == null
                ? SizedBox(
                    width: 0,
                  )
                : Divider(
                    color: globals.gold,
                  ),
            widget.effort.medal == null
                ? SizedBox(
                    width: 0,
                  )
                : SizedBox(
                    height: 20,
                  ),
            Graph(
              data: widget.effort.stepsWeek,
            ),
          ],
        ),
      ),
    );
  }
}
