import 'dart:async';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/bd/UserBD.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
// import 'package:location_permissions/location_permissions.dart'
//     as LocationPermissionsPlugin;
import 'package:location/location.dart';
import 'package:intl/intl.dart';
import "package:latlong/latlong.dart" as latLng;
import 'package:flutter_map/flutter_map.dart';
import 'package:map_controller/map_controller.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Views/Effort/EffortFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:geolocator/geolocator.dart';

// ignore: must_be_immutable
class EffortTrackScreen extends StatefulWidget {
  Position currentPosition;
  Arguments args;

  EffortTrackScreen({Key key, this.currentPosition, this.args})
      : super(key: key);
  @override
  _EffortTrackScreenState createState() => _EffortTrackScreenState();
}

class _EffortTrackScreenState extends State<EffortTrackScreen> {
  // View data
  var _currentLocation;
  var _previousLocation;
  String totalTime = '';
  String effortStart;
  String effortEnd;
  double distance = 0.0;
  double timeTotal = 0.01;
  double speed = 0;
  double calories =
      0; //Gasto energético (kcal/min) = 0,0175 x MET x peso en kilogramos.
  bool _onFocus = true;
  DateTime effortStartDateTime;
  DateTime effortEndDateTime;
  bool activeSession = false;
  //user data
  bool login = false;
  UserRegister user;
  double weight = 0;

  // Mapcontroller
  MapController mapController;
  StatefulMapController statefulMapController;

  // Location routeTracking
  Map<String, latLng.LatLng> routeTracking = {};

  // Location services
  Location location = new Location();
  LocationData _location;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  bool _enabledInBackground;
  String _errorLocationInBackground;
  bool _neverAskLocation = false;
  double _altitudeStart;
  double _altitudeEnd;
  int _locationTotalUpdates = 0;
  bool permission = true;

  // Location suscription
  StreamSubscription<LocationData> _locationSubscription;
  String _error;

  @override
  void initState() {
    if (widget.currentPosition != null) {
      // print('Not null, checking position');
      // Map controller
      mapController = MapController();
      statefulMapController =
          StatefulMapController(mapController: mapController);
      // _checkBackgroundMode();
      // _toggleBackgroundMode();
      _config();
    }

    super.initState();
  }

  _config() async {
    await checkPermissions();
    this.login = await conf();

    // _permissionGranted = await location.hasPermission();
    // print(_permissionGranted);
    // if (_permissionGranted == PermissionStatus.denied) {
    //   setState(() {
    //     this.permission = false;
    //   });
    // } else {
    //   setState(() {
    //     this.permission = true;
    //   });
    // }
    if (login) {
      this.user = await DBProvider.db.getUserConfig();

      setState(() {
        this.weight = user.weight;
      });
    }
  }

  @override
  void dispose() async {
    super.dispose();

    if (widget.currentPosition != null) {
      // Cancelling location suscription
      bool _enabled = await location.isBackgroundModeEnabled();
      if (_enabled) {
        await location.enableBackgroundMode(enable: false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      onForegroundLost: () {
        setState(() {
          _onFocus = false;
        });
      },
      onForegroundGained: () async {
        await recalculateTimer();
      },
      child: Scaffold(
        endDrawer: AppDrawer(),
        appBar: globalAppBar(context, 'generic', ' Registro', true),
        backgroundColor: globals.blue,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
          ),
          child: Center(
            child: Column(
              children: [
                (widget.currentPosition != null
                    ? Container(
                        height: MediaQuery.of(context).size.height / 5,
                        child: FlutterMap(
                          options: MapOptions(
                            center: _currentLocation ??
                                latLng.LatLng(widget.currentPosition.latitude,
                                    widget.currentPosition.longitude),
                            zoom: 16.0,
                            minZoom: 15.5,
                            maxZoom: 17.0,
                            interactiveFlags: InteractiveFlag.none,
                            // InteractiveFlag.pinchZoom | InteractiveFlag.drag,
                          ),
                          mapController: mapController,
                          layers: [
                            TileLayerOptions(
                                urlTemplate:
                                    "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                                subdomains: ['a', 'b', 'c']),
                            MarkerLayerOptions(
                              markers: [
                                Marker(
                                  width: 80.0,
                                  height: 80.0,
                                  point: _currentLocation ??
                                      latLng.LatLng(
                                          widget.currentPosition.latitude,
                                          widget.currentPosition.longitude),
                                  builder: (ctx) => Container(
                                    child: Icon(
                                      Icons.my_location,
                                      color: globals.blue,
                                      size: 35.0,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    : SizedBox(height: 0)),
                SizedBox(height: 10),
                Text(
                  "Tipo de ejercicio: " + widget.args.type,
                  style: TextStyle(fontSize: 16.0),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                (this.activeSession
                    ? activeEffort(
                        this.effortStartDateTime,
                        this.effortStart,
                        this.distance,
                        this._altitudeStart,
                        this._altitudeEnd,
                        widget.currentPosition != null)
                    : finishedEffort(
                        this.effortStartDateTime,
                        this.effortStart,
                        this.effortEndDateTime,
                        this.effortEnd,
                        totalTime,
                        this.speed,
                        this.distance,
                        this._altitudeStart,
                        this._altitudeEnd,
                        widget.currentPosition != null,
                        login: this.login,
                        calories: this.calories)),
                // listLatLngElements()
                Spacer(),
              ],
            ),
          ),
        ),
        floatingActionButton: Container(
          width: double.infinity,
          child: Row(
            children: [
              Spacer(),
              this.permission
                  ? FloatingActionButton(
                      onPressed: handleStartStop,
                      elevation: 2.0,
                      backgroundColor: globals.blue,
                      // fillColor: globals.blue,
                      child: Icon(
                        (this.activeSession ? Icons.pause : Icons.play_arrow),
                        size: 25.0,
                        color: globals.gold,
                      ),
                      shape: CircleBorder(),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              (!this.activeSession && this.effortStartDateTime != null
                  ? RawMaterialButton(
                      onPressed: () async {
                        Map<String, dynamic> returnedData = await saveEffort(
                            this.routeTracking,
                            widget.args.id,
                            DateFormat('yyyy-MM-dd kk:mm:ss')
                                .format(this.effortStartDateTime)
                                .toString(),
                            DateFormat('yyyy-MM-dd kk:mm:ss')
                                .format(this.effortEndDateTime)
                                .toString(),
                            this.totalTime,
                            this.distance);

                        if (returnedData['error']) {
                          // TODO implementar Alert() para avisar al usuario que hubo un error
                        } else {
                          // Redirecting user to success page
                          // print('No error');
                          Navigator.pushNamed(
                              context, '/effort/register/success');
                        }
                      },
                      elevation: 2.0,
                      fillColor: globals.blue,
                      child: Icon(
                        Icons.save_alt_outlined,
                        size: 25.0,
                        color: globals.gold,
                      ),
                      padding: EdgeInsets.all(15.0),
                      shape: CircleBorder(),
                    )
                  : SizedBox(
                      height: 20.0,
                    )),
              Spacer(),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }

  // _enableBakckgroundLocationAlert(context) {
  //   AwesomeDialog(
  //       context: context,
  //       dialogType: DialogType.WARNING,
  //       headerAnimationLoop: false,
  //       animType: AnimType.TOPSLIDE,
  //       showCloseIcon: false,
  //       title: 'Advertencia',
  //       desc:
  //           'Esta aplicación recopila datos de la ubicación del usuario para habilitar la función de seguimiento deportivo, incluso cuando la aplicación está cerrada, en segundo plano o no está en uso. ¿Deseas otorgar permisos de ubicación?',
  //       btnCancelText: 'Cancelar',
  //       btnOkText: 'Otorgar',
  //       btnCancelOnPress: () {
  //         _locationBlocked(context);
  //       },
  //       btnOkOnPress: () {
  //         _toggleBackgroundMode();
  //         // Perm.openAppSettings();
  //         askLocationPermissions();
  //       })
  //     ..show();
  // }

  _locationBlocked(context) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        headerAnimationLoop: false,
        animType: AnimType.TOPSLIDE,
        showCloseIcon: false,
        title: 'Advertencia',
        desc:
            'El permiso de localización ha sido rechazado. Recuerda que para poder registrar tu ruta completa, es necesario habilitarlo aún cuando la aplicación no está en uso. Por favor, ingresa a los permisos de la aplicación desde la configuración de tu dispositivo para habilitarlo.',
        btnCancelText: 'Cancelar',
        btnOkText: 'Otorgar',
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          _toggleBackgroundMode();
          // Perm.openAppSettings();
          askLocationPermissions();
        })
      ..show();
  }

  Widget fadeAlertAnimation(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }

  Future<void> handleStartStop() async {
    // if (this.effortStart != null) {
    //   _checkBackgroundMode();

    //   print('Enabled in background');
    //   print(_enabledInBackground.toString());

    // LocationPermissionsPlugin.PermissionStatus _permission =
    //     await LocationPermissionsPlugin.LocationPermissions()
    //         .requestPermissions();

    // if (!_enabledInBackground && _permission == PermissionStatus.denied) {
    //   print("No enabled in background");
    //   print('Requesting location service');

    // }
    // }

    if (!this.activeSession) {
      setState(() {
        this.activeSession = true;
        // print("Setting date start");

        if (this.effortStartDateTime == null) {
          this.effortStartDateTime = DateTime.now();
          this.effortStart = DateFormat('kk:mm:ss')
              .format(this.effortStartDateTime)
              .toString();
        }

        // Clearing end of session, in case we restart
        this.effortEndDateTime = null;
        this.effortEnd = "";

        // Starting location tracking

        if (widget.currentPosition != null) {
          // print('listen location');
          _listenLocation();
        }
      });
    } else {
      this.activeSession = false;
      // print("stopping");

      // Stoping GPS listening service
      if (widget.currentPosition != null) {
        _stopListen();
      }

      // Updating time
      await updateTimeValue();

      // Calculating speed
      this.speed = this.distance / this.timeTotal;

      // Calculating calories

      double _calories = 0;

      if (widget.currentPosition != null) {
        // print('actividad interior');
        // print(this.timeTotal);
        double _time = this.timeTotal / 60;
        //actividad interior
        _calories = this.calories = 0.0175 * this.weight * _time;
      } else {
        // print('actividad exterior');

        //actividad exterior
        _calories = this.calories = 1.75 * this.weight * this.speed;
      }

      // if (globalsRegister.args.id == 1) {
      // } else {}

      // Updating variables
      setState(() {
        if (login) {
          setState(() {
            this.calories = _calories;
          });
        }

        this.effortEndDateTime = DateTime.now();
        this.effortEnd =
            DateFormat('kk:mm:ss').format(this.effortEndDateTime).toString();
      });
    }
  }

  askLocationPermissions() async {
    _serviceEnabled = await location.serviceEnabled();
    // print(_serviceEnabled);
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    // print(_permissionGranted);
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      // print(_permissionGranted);
      if (_permissionGranted != PermissionStatus.granted) {
        _locationBlocked(context);
        return;
      } else {
        setState(() {
          this.permission = true;
        });
      }
    }
  }

  Future<void> _stopListen() async {
    _locationSubscription.cancel();
  }

  Future<void> _listenLocation() async {
    location.enableBackgroundMode(enable: true);
    await location.changeSettings(interval: 1000, distanceFilter: 5.0);
    _locationSubscription =
        location.onLocationChanged.handleError((dynamic err) {
      setState(() {
        _error = err.code;
      });
      _locationSubscription.cancel();
    }).listen((LocationData currentLocation) {
      updateLocationWithData(currentLocation);
    });
  }

  Future<void> _checkBackgroundMode() async {
    setState(() {
      _errorLocationInBackground = null;
    });
    final bool result = await location.isBackgroundModeEnabled();
    // print('Background mode?');
    // print(result);

    if (!result) {
      // print('Requesting location service');
      // LocationPermissionsPlugin.PermissionStatus permission =
      //     await LocationPermissionsPlugin.LocationPermissions()
      //         .requestPermissions();
      location.enableBackgroundMode(enable: true);
      setState(() {
        _enabledInBackground = true;
      });
    }

    setState(() {
      _enabledInBackground = result;
    });
  }

  Future<void> _toggleBackgroundMode() async {
    // print(_errorLocationInBackground.toString());

    // if(_errorLocationInBackground.toString() == ){
    //   _neverAskLocation
    // }
    // print('_toggleBackgroundMode');
    setState(() {
      _errorLocationInBackground = null;
    });
    try {
      final bool result = await location.enableBackgroundMode(
          enable: !(_enabledInBackground ?? false));
      setState(() {
        // print('Enabling');
        _enabledInBackground = result;
      });
    } on PlatformException catch (err) {
      setState(() {
        _errorLocationInBackground = err.code;
        askLocationPermissions();
      });
    }
  }

  updateLocationWithData(currentLocation) async {
    // print("updateLocationWithData ");
    // Updating total updated data
    _locationTotalUpdates++;
    // print("Altura 1: " + widget.currentPosition.altitude.toString());

    this._altitudeStart = widget.currentPosition.altitude;

    if (_locationTotalUpdates >= 2) {
      // print('Ya hay dos');
      _error = null;

      var _currentDate =
          DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now());
      // print('currentlocation');
      // print(await currentLocation);
      // print(currentLocation.latitude.toString());
      // print(currentLocation.longitude.toString());

      // print("Altura: " + widget.currentPosition.altitude.toString());
      //  final double altitude; // In meters above the WGS 84 reference ellipsoid
      this._altitudeEnd = widget.currentPosition.altitude;

      this._currentLocation =
          latLng.LatLng(currentLocation.latitude, currentLocation.longitude);
      this.routeTracking[_currentDate] = this._currentLocation;

      this.statefulMapController.centerOnPoint(this._currentLocation);

      this._location = currentLocation;

      setState(() {
        if (this._previousLocation != null) {
          // Calculating distance and updating previous location
          this.distance +=
              geolocatorDistance(this._currentLocation, this._previousLocation);
          this._previousLocation = this._currentLocation;
        } else {
          this.distance = 0.0;
          this._previousLocation = this._currentLocation;
        }
      });
    }
  }

  recalculateTimer() async {
    if (effortStartDateTime != null) {
      // print('gained focus, recalculating timer');
      // print(this.effortStart.toString());
      var diff = DateTime.now().difference(effortStartDateTime);
      // print("Diff in milisconds: " + diff.inSeconds.toString());
    }

    setState(() {
      _onFocus = true;
    });
  }

  Widget listLatLngElements() {
    return Container(
      height: 240.0,
      decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: (this.routeTracking != null
            ? ListView.builder(
                itemCount: this.routeTracking.length,
                itemBuilder: (BuildContext context, int index) {
                  int rev_index = this.routeTracking.length - index - 1;

                  String _date =
                      this.routeTracking.keys.elementAt(rev_index).toString();
                  String _latlong = this
                          .routeTracking
                          .values
                          .elementAt(rev_index)
                          .latitude
                          .toString() +
                      ", " +
                      this
                          .routeTracking
                          .values
                          .elementAt(rev_index)
                          .longitude
                          .toString();

                  return new Column(
                    children: <Widget>[
                      new ListTile(
                          title: new Text(_date), subtitle: new Text(_latlong)),
                    ],
                  );
                })
            : Loading()),
      ),
    );
  }

  Future<void> updateTimeValue() async {
    this.totalTime = formatTime(
        DateTime.now().difference(effortStartDateTime).inMilliseconds);

    var diff = DateTime.now().difference(effortStartDateTime);
    // print("Diff in seconds: " + timeTotal.toString());

    // Updating timeTotal
    setState(() {
      this.timeTotal = diff.inSeconds.toDouble();
    });
  }

  checkPermissions() async {
    _permissionGranted = await location.hasPermission();
    // print(_permissionGranted);
    if (_permissionGranted == PermissionStatus.denied) {
      AwesomeDialog(
          context: context,
          dialogType: DialogType.WARNING,
          headerAnimationLoop: false,
          animType: AnimType.TOPSLIDE,
          showCloseIcon: false,
          title: 'Advertencia',
          desc:
              'Esta aplicación recopila datos de la ubicación del usuario para habilitar la función de seguimiento deportivo, incluso cuando la aplicación está cerrada, en segundo plano o no está en uso. ¿Deseas otorgar permisos de ubicación?',
          btnCancelText: 'Cancelar',
          btnOkText: 'Otorgar',
          btnCancelOnPress: () {
            setState(() {
              this.permission = false;
            });
            _locationBlocked(context);
          },
          btnOkOnPress: () {
            // Perm.openAppSettings();
            askLocationPermissions();
          })
        ..show();
    }
  }

  // Future<void> checkPermissions() async {
  //   var status = await Permission.locationAlways.status;
  //   print("Permissions = " + status.toString());

  //   // We don't have permissions, so we open prompt
  //   if (!status.isGranted) {
  //     await Permission.locationAlways.request();
  //   }

  //   // _permissionGranted = await location.hasPermission();
  //   // print(_permissionGranted);

  //   // Fetching permission
  //   // bool serviceEnabled;
  //   // LocationPermission permission;
  //   // permission = await Geolocator.checkPermission();
  //   // print(permission.toString());

  //   if (!status.isGranted) {
  //     AwesomeDialog(
  //         context: context,
  //         dialogType: DialogType.WARNING,
  //         headerAnimationLoop: false,
  //         animType: AnimType.TOPSLIDE,
  //         showCloseIcon: false,
  //         closeIcon: Icon(Icons.close_fullscreen_outlined),
  //         title: 'Advertencia',
  //         desc:
  //             'Esta aplicación recopila datos de la ubicación del usuario para habilitar la función de seguimiento deportivo, incluso cuando la aplicación está cerrada, en segundo plano o no está en uso. ¿Deseas otorgar permisos de ubicación?',
  //         btnCancelText: 'Cancelar',
  //         btnOkText: 'Otorgar',
  //         btnCancelOnPress: () {},
  //         btnOkOnPress: () async {
  //           print('DEBUG: OpenAppSettings');
  //           await Permission.locationAlways.request();
  //           // await openAppSettings();
  //         })
  //       ..show();
  //   }
  // }
}
