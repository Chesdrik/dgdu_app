import 'package:date_format/date_format.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Models/EffortRegister.dart';
import 'package:dgdu_app/Models/Stats.dart';
import 'package:dgdu_app/Views/Effort/EffortFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Forms/DropdownDate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/globalsRegister.dart' as globalsRegister;

class EffortRegisterScreen extends StatefulWidget {
  Arguments arguments;

  EffortRegisterScreen({Key key, this.arguments}) : super(key: key);

  @override
  EffortRegisterScreenState createState() => EffortRegisterScreenState();
}

class EffortRegisterScreenState extends State<EffortRegisterScreen> {
  bool login = false;
  int idUser = 0;

  double _intensity = 20;

  GlobalKey<FormState> _key = GlobalKey();

  String _nombre;
  String _steps;
  String _distance;
  String _calorias;
  String _speed;
  String _heart_rate;

  String _setTime, _setDate;

  String _hour, _minute, _time;

  String dateTime;

  DateTime selectedDate = DateTime.now();

  TimeOfDay selectedTimeInicio = TimeOfDay(hour: 07, minute: 00);
  TimeOfDay selectedTimeFin = TimeOfDay(hour: 07, minute: 00);

  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();

  RegExp numbersOnly = new RegExp(r'^[0-9]*$');

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        selectedDate = picked;
        _dateController.text = DateFormat.yMd().format(selectedDate);
      });
  }

  Future<Null> _selectTimeInicio(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTimeInicio,
    );
    if (picked != null)
      setState(() {
        selectedTimeInicio = picked;
        _hour = selectedTimeInicio.hour.toString();
        _minute = selectedTimeInicio.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTimeInicio.hour,
                selectedTimeInicio.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  Future<Null> _selectTimeFin(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTimeFin,
    );
    if (picked != null)
      setState(() {
        selectedTimeFin = picked;
        _hour = selectedTimeFin.hour.toString();
        _minute = selectedTimeFin.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTimeFin.hour, selectedTimeFin.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  @override
  void initState() {
    _config();

    _dateController.text = DateFormat.yMd().format(DateTime.now());
    _timeController.text = formatDate(
        DateTime(2019, 08, 1, DateTime.now().hour, DateTime.now().minute),
        [hh, ':', nn, " ", am]).toString();
    super.initState();
  }

  _config() async {
    this.login = await conf();
    if (!login) {
      setState(() {
        this.idUser = 0;
      });
    } else {
      setState(() {
        this.idUser = globals.user.id;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    dateTime = DateFormat.yMd().format(DateTime.now());
    return Scaffold(
      backgroundColor: globals.blue,
      appBar: AppBar(
        brightness: Brightness.dark,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text('Agregar actividad: ' + widget.arguments.type),
      ),
      body: SingleChildScrollView(
        child: formEffort(),
      ),
    );
  }

  Widget formEffort() {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Container(
            // width: 300.0, //size.width * .6,
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  // TextFormField(
                  //   style: TextStyle(color: Colors.white),
                  //   validator: (text) {
                  //     if (text.length == 0) {
                  //       return "Este campo es requerido";
                  //     }
                  //     return null;
                  //   },
                  //   keyboardType: TextInputType.emailAddress,
                  //   maxLength: 30,
                  //   textAlign: TextAlign.center,
                  //   decoration: InputDecoration(
                  //       enabledBorder: new OutlineInputBorder(
                  //         borderRadius: new BorderRadius.circular(10.0),
                  //         borderSide: BorderSide(color: globals.gold),
                  //         gapPadding: 5,
                  //       ),
                  //       focusedBorder: new OutlineInputBorder(
                  //         borderRadius: new BorderRadius.circular(10.0),
                  //         borderSide: BorderSide(color: globals.gold),
                  //         gapPadding: 5,
                  //       ),
                  //       labelText: 'TÍTULO DE LA ACTIVIDAD:',
                  //       labelStyle: TextStyle(color: globals.gold),
                  //       counterText: '',
                  //       contentPadding:
                  //           new EdgeInsets.symmetric(horizontal: 8.0),
                  //       focusColor: Colors.white),
                  //   onSaved: (text) => _nombre = text,
                  // ),
                  // SizedBox(
                  //   height: 30,
                  // ),
                  DropdownDate(
                    labelText: 'FECHA:',
                    valueText: "${selectedDate.toLocal()}".split(' ')[0],
                    onPressed: () {
                      _selectDate(context);
                    },
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  DropdownDate(
                    labelText: 'INICIO:',
                    valueText: selectedTimeInicio.format(context),
                    onPressed: () {
                      _selectTimeInicio(context);
                    },
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  DropdownDate(
                    labelText: 'FIN:',
                    valueText: selectedTimeFin.format(context),
                    onPressed: () {
                      _selectTimeFin(context);
                    },
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: Colors.white),
                    validator: (text) {
                      if (!numbersOnly.hasMatch(text)) {
                        return "Sólo se admiten números enteros";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        labelText: 'PASOS:',
                        labelStyle: TextStyle(color: globals.gold),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0)),
                    onSaved: (text) => _steps = text,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: Colors.white),
                    validator: (text) {
                      if (text.length == 0) {
                        return null;
                      } else if (!numbersOnly.hasMatch(text)) {
                        return "Sólo se admiten números enteros";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        labelText: 'DISTANCIA:',
                        labelStyle: TextStyle(color: globals.gold),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0)),
                    onSaved: (text) => _distance = text,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: Colors.white),
                    validator: (text) {
                      if (text.length == 0) {
                        return null;
                      } else if (!numbersOnly.hasMatch(text)) {
                        return "Sólo se admiten números enteros";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        labelText: 'VELOCIDAD:',
                        labelStyle: TextStyle(color: globals.gold),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0)),
                    onSaved: (text) => _speed = text,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: Colors.white),
                    validator: (text) {
                      if (text.length == 0) {
                        return null;
                      } else if (!numbersOnly.hasMatch(text)) {
                        return "Sólo se admiten números enteros";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        labelText: 'FRECUENCIA CARDIACA:',
                        labelStyle: TextStyle(color: globals.gold),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0)),
                    onSaved: (text) => _heart_rate = text,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: Colors.white),
                    validator: (text) {
                      if (text.length == 0) {
                        return null;
                      } else if (!numbersOnly.hasMatch(text)) {
                        return "Sólo se admiten números enteros";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        labelText: 'CALORIAS:',
                        labelStyle: TextStyle(color: globals.gold),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0)),
                    onSaved: (text) => _calorias = text,
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  // Text(
                  //   'INTENSIDAD: $_intensity',
                  //   style: TextStyle(color: Colors.white),
                  // ),
                  // Slider(
                  //   activeColor: globals.gold,
                  //   value: _intensity,
                  //   min: 0,
                  //   max: 100,
                  //   divisions: 100,
                  //   label: _intensity.round().toString(),
                  //   onChanged: (double value) {
                  //     setState(() {
                  //       _intensity = value;
                  //     });
                  //   },
                  // ),
                  ButtonTheme(
                    minWidth: 200.0,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(globals.blue),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      child: Text('GUARDAR',
                          style: TextStyle(fontSize: 20, color: globals.gold)),
                      onPressed: () async {
                        if (_key.currentState.validate()) {
                          _key.currentState.save();
                          EffortRegister effort = await _processInfo(
                              _nombre,
                              selectedDate,
                              selectedTimeInicio,
                              selectedTimeFin,
                              widget.arguments.type,
                              _steps,
                              _distance,
                              _speed,
                              _heart_rate,
                              _calorias);
                          EasyLoading.show(status: 'Enviando esfuerzo...');
                          bool response = await sendEffort(effort);
                          EasyLoading.dismiss();
                          if (response) {
                            //si no esta logueado
                            if (!this.login) {
                              Navigator.pushNamed(
                                  context, '/effort/register/success');
                            } else {
                              //checar si viene del esfuerzo, del evento o del dashboard
                              //si viene del dash bashboard o del esfuerzo mandar al del perfil
                              // si viene del evento mostrar un resumen

                              if (globalsRegister.args.type == 'event') {
                                Navigator.pushNamed(
                                    context, '/effort/register/success');
                              } else {
                                Navigator.pushNamed(context, '/effort/main');
                              }
                            }
                          } else {
                            Fluttertoast.showToast(
                                msg: "Hubo un error al iniciar sesión, " +
                                    globals.message,
                                backgroundColor: Colors.grey,
                                timeInSecForIosWeb: 5);
                          }

                          globals.effort = effort;
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<EffortRegister> _processInfo(
      String _nombre,
      date,
      inicio,
      fin,
      String type,
      String steps,
      String distance,
      String speed,
      String heart_rate,
      String calorias) async {
    DateTime inicioHour = new DateTime(
        date.year, date.month, date.day, inicio.hour, inicio.minute);
    DateTime finHour =
        new DateTime(date.year, date.month, date.day, fin.hour, fin.minute);
    String beginning = inicioHour.toString();
    String ending = finHour.toString();
    String dateEffort = date.toString();

    // String dateEffort = DateFormat('yyyy-MM-dd').format(date).toString();

    Stats stat = new Stats(
        step: steps,
        distance: distance,
        speed: speed,
        heart_rate: heart_rate,
        calories: calorias);

    EffortRegister effort = new EffortRegister(
        user: this.idUser,
        idEffort: widget.arguments.id,
        title: _nombre,
        type: type,
        beginning: beginning,
        ending: ending,
        date: dateEffort,
        stats: stat);

    return effort;
  }
}
