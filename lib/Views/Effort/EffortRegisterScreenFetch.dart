import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Views/Effort/EffortRegisterScreen.dart';
import 'package:flutter/material.dart';

class EffortRegisterScreenFetch extends StatefulWidget {
  EffortRegisterScreenFetch({Key key}) : super(key: key);

  @override
  EffortRegisterScreenFetchState createState() =>
      new EffortRegisterScreenFetchState();
}

class EffortRegisterScreenFetchState extends State<EffortRegisterScreenFetch> {
  @override
  Widget build(BuildContext context) {
    Arguments args = ModalRoute.of(context).settings.arguments;
    return FutureBuilder(builder: (BuildContext context, AsyncSnapshot info) {
      return (EffortRegisterScreen(arguments: args));
    });
  }
}
