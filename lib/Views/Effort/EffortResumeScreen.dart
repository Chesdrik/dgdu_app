import 'package:dgdu_app/Models/EffortRegister.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/material.dart';

class EffortResumeScreen extends StatelessWidget {
  EffortRegister effort = globals.effort;

  EffortResumeScreen({Key key, this.effort}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var levelIndicator = Container(
      child: Container(
        child: LinearProgressIndicator(
            backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
            value: 1,
            valueColor: AlwaysStoppedAnimation(Colors.green)),
      ),
    );

    var eventDate = Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        'effort.date,',
        style: TextStyle(color: Colors.white),
      ),
    );

    var topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 120.0),
        Text(
          effort.title,
          style: TextStyle(color: Colors.white, fontSize: 45.0),
        ),
        SizedBox(height: 20.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(flex: 1, child: levelIndicator),
            Expanded(
                flex: 6,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      effort.type,
                      style: TextStyle(color: Colors.white),
                    ))),
            Expanded(flex: 6, child: eventDate)
          ],
        ),
      ],
    );

    var topContent = Stack(
      children: <Widget>[
        // Container(
        //     padding: EdgeInsets.only(left: 10.0),
        //     height: MediaQuery.of(context).size.height * 0.5,
        //     decoration: new BoxDecoration(
        //       image: new DecorationImage(
        //         image: new AssetImage("drive-steering-wheel.jpg"),
        //         fit: BoxFit.cover,
        //       ),
        //     )),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: globals.blue),
          child: Center(
            child: topContentText,
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/effort/main');
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );

    var bottomContentText = Text(
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      style: TextStyle(fontSize: 18.0),
    );
    var readButton = Container(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      width: MediaQuery.of(context).size.width,
      child: ButtonTheme(
        minWidth: 200.0,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(globals.blue),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          child: Text('REGRESAR',
              style: TextStyle(fontSize: 20, color: Colors.white)),
          onPressed: () {
            Navigator.pushNamed(context, '/effort/main');
          },
        ),
      ),
    );
    var bottomContent = Container(
      // height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // color: Theme.of(context).primaryColor,
      padding: EdgeInsets.all(40.0),
      child: Center(
        child: Column(
          children: <Widget>[bottomContentText, readButton],
        ),
      ),
    );

    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[topContent, bottomContent],
      ),
    ));
  }
}
