import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';

class EnableInBackgroundWidget extends StatefulWidget {
  const EnableInBackgroundWidget({Key key}) : super(key: key);

  @override
  _EnableInBackgroundState createState() => _EnableInBackgroundState();
}

class _EnableInBackgroundState extends State<EnableInBackgroundWidget> {
  final Location location = Location();

  bool _enabledInBackground;
  String _errorLocationInBackground;

  @override
  void initState() {
    _checkBackgroundMode();
    super.initState();
  }

  Future<void> _checkBackgroundMode() async {
    setState(() {
      _errorLocationInBackground = null;
    });
    final bool result = await location.isBackgroundModeEnabled();
    setState(() {
      _enabledInBackground = result;
    });
  }

  Future<void> _toggleBackgroundMode() async {
    setState(() {
      _errorLocationInBackground = null;
    });
    try {
      final bool result = await location.enableBackgroundMode(
          enable: !(_enabledInBackground ?? false));
      setState(() {
        _enabledInBackground = result;
      });
    } on PlatformException catch (err) {
      setState(() {
        _errorLocationInBackground = err.code;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
            'Enabled in background: ${_errorLocationInBackground ?? '${_enabledInBackground ?? false}'}',
            style: Theme.of(context).textTheme.bodyText1),
        Row(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(right: 42),
              child: ElevatedButton(
                child: const Text('Check'),
                onPressed: _checkBackgroundMode,
              ),
            ),
            ElevatedButton(
              child: Text(_enabledInBackground ?? false ? 'Disable' : 'Enable'),
              onPressed:
                  _enabledInBackground == null ? null : _toggleBackgroundMode,
            )
          ],
        )
      ],
    );
  }
}
