import 'package:dgdu_app/Views/Effort/EffortFunctions.dart';
import 'package:dgdu_app/Views/Effort/EffortPerfilScreen.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class EffortPerfilFetch extends StatefulWidget {
  EffortPerfilFetch({Key key}) : super(key: key);

  @override
  EffortPerfilFetchState createState() => new EffortPerfilFetchState();
}

class EffortPerfilFetchState extends State<EffortPerfilFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getPerfilInfo(context),
        builder: (BuildContext context, AsyncSnapshot info) {
          return (info.data == null
              ? LoadingScreen()
              : EffortPerfilScreen(effort: info.data));
        });
  }
}
