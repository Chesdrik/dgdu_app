import 'dart:convert';

import 'package:dgdu_app/Views/Login/LoginFunctions.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(
      builder: (context) => LoginScreen(),
    );
  }

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  GlobalKey<FormState> _key = GlobalKey();

  RegExp emailRegExp = new RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");
  // new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');
  String _correo;
  String _contrasena;
  String mensaje = '';

  bool login = false;
  bool _obscureText = true;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  int _counter = 0;

  @override
  initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);

    controller.forward();
    _checkCounter();
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  _checkCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int counter = (prefs.getInt('loginFails') ?? 0);

    String date = (prefs.getString('failsDate') ?? null);
    if (date != null) {
      DateTime dt = DateTime.parse(date);
      var now = DateTime.now();
      //we check if the time has passed
      //Returns true if dt occurs before now.
      var result = dt.isBefore(now);
      if (result) {
        setState(() {
          this._counter = 0;
        });
        //reset the shared preferences
        await prefs.setString('failsDate', null);
        await prefs.setInt('loginFails', 0);
      }
    } else {
      setState(() {
        this._counter = counter;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'dashboard', 'Ingresar', true),
      backgroundColor: globals.blue,
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
        child: loginForm(),
      ),
    );
  }

  Widget loginForm() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AnimatedLogo(animation: animation),
            ],
          ),
          SizedBox(
            height: 50,
          ),
          Container(
            width: 300.0, //size.width * .6,
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      if (text.length == 0) {
                        return "Este campo es requerido";
                      } else if (!emailRegExp.hasMatch(text)) {
                        return "El formato para correo no es correcto";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'Correo',
                        labelStyle:
                            TextStyle(color: globals.blue.withOpacity(0.5)),
                        counterText: '',
                        focusColor: globals.gold),
                    onSaved: (text) => _correo = text,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      if (text.length == 0) {
                        return "Este campo es requerido";
                      } else if (text.length <= 5) {
                        return "Su contraseña debe ser al menos de 5 caracteres";
                      } else if (!contRegExp.hasMatch(text)) {
                        return "El formato para contraseña no es correcto";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                        borderSide: BorderSide(color: globals.blue),
                        gapPadding: 5,
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                        borderSide: BorderSide(color: globals.blue),
                        gapPadding: 5,
                      ),
                      labelText: 'Contraseña',
                      labelStyle:
                          TextStyle(color: globals.blue.withOpacity(0.5)),
                      counterText: '',
                    ),
                    onSaved: (text) => _contrasena = text,
                    obscureText: _obscureText,
                  ),
                  Row(
                    children: [
                      Checkbox(
                        value: !_obscureText,
                        activeColor: globals.blue,
                        onChanged: (bool value) {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                      ),
                      Text('Mostrar contraseña'),
                    ],
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/forget/password');
                    },
                    child: Text("¿OLVIDASTE TU CONTRASEÑA?",
                        style: TextStyle(
                            fontSize: 11,
                            color: globals.blue.withOpacity(0.75))),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/register');
                    },
                    child: Text("REGISTRARSE",
                        style: TextStyle(
                            fontSize: 12,
                            color: globals.blue.withOpacity(0.75))),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  ButtonTheme(
                      // minWidth: 200.0,
                      minWidth: double.infinity,
                      child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: this._counter <= 2
                                ? MaterialStateProperty.all<Color>(globals.blue)
                                : MaterialStateProperty.all<Color>(Colors.grey),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('INGRESAR',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: this._counter <= 2
                                        ? globals.gold
                                        : Colors.black)),
                          ),
                          onPressed: () async {
                            if (this._counter <= 2) {
                              if (_key.currentState.validate()) {
                                _key.currentState.save();
                                //Aqui se llamaria al API para hacer el login
                                setState(() {
                                  login = true;
                                });
                                EasyLoading.show(
                                    status: 'Espere un momento...');
                                bool response =
                                    await userLogin(_correo, _contrasena);
                                EasyLoading.dismiss();
                                if (response) {
                                  Navigator.pushNamed(context, '/');
                                } else {
                                  _checkCounter();

                                  Fluttertoast.showToast(
                                      msg: "Error: " + globals.message,
                                      backgroundColor: Colors.grey,
                                      timeInSecForIosWeb: 5);
                                }
                              }
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Error: demasiados intentos, espere tres horas por favor",
                                  backgroundColor: Colors.grey,
                                  timeInSecForIosWeb: 5);
                            }
                          })),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AnimatedLogo extends AnimatedWidget {
  final String login = 'assets/logo.svg';

  static final _opacityTween = Tween<double>(begin: 0.1, end: 1.0);
  static final _sizeTween = Tween<double>(begin: 0.0, end: 150.0);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Opacity(
      opacity: _opacityTween.evaluate(animation),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        height: _sizeTween.evaluate(animation), // Aumenta la altura
        width: _sizeTween.evaluate(animation), // Aumenta el ancho
        child: SvgPicture.asset(login,
            color: globals.blue, semanticsLabel: 'login'),
      ),
    );
  }
}
