import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Views/Login/LoginFunctions.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class PasswordForgetScreen extends StatefulWidget {
  @override
  _PasswordForgetScreenState createState() => _PasswordForgetScreenState();
}

class _PasswordForgetScreenState extends State<PasswordForgetScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  GlobalKey<FormState> _key = GlobalKey();

  RegExp emailRegExp = new RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');
  String _correo;
  String mensaje = '';

  @override
  initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);

    controller.forward();
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Recuperar contraseña', true),
      backgroundColor: globals.blue,
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
        child: loginForm(),
      ),
    );
  }

  Widget loginForm() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AnimatedLogo(animation: animation),
            ],
          ),
          SizedBox(
            height: 50,
          ),
          Container(
            width: 300.0, //size.width * .6,
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  Text('Ingrese el correo con el cual se registro'),
                  SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      if (text.length == 0) {
                        return "Este campo correo es requerido";
                      } else if (!emailRegExp.hasMatch(text)) {
                        return "El formato para correo no es correcto";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'Correo',
                        labelStyle:
                            TextStyle(color: globals.blue.withOpacity(0.5)),
                        counterText: '',
                        focusColor: globals.gold),
                    onSaved: (text) => _correo = text,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/login');
                    },
                    child: Text("VOLVER AL LOGIN",
                        style: TextStyle(
                            fontSize: 11,
                            color: globals.blue.withOpacity(0.75))),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  ButtonTheme(
                    // minWidth: 200.0,
                    minWidth: double.infinity,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(globals.blue),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('ENVIAR',
                            style:
                                TextStyle(fontSize: 20, color: globals.gold)),
                      ),
                      onPressed: () async {
                        if (_key.currentState.validate()) {
                          _key.currentState.save();
                          EasyLoading.show(status: 'Espere un momento...');
                          ResponseRequest response =
                              await forgetPassword(_correo);
                          EasyLoading.dismiss();
                          Fluttertoast.showToast(
                              msg: response.message,
                              backgroundColor:
                                  response.error ? Colors.red : Colors.green,
                              timeInSecForIosWeb: 5);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AnimatedLogo extends AnimatedWidget {
  String login = 'assets/logo.svg';

  static final _opacityTween = Tween<double>(begin: 0.1, end: 1.0);
  static final _sizeTween = Tween<double>(begin: 0.0, end: 150.0);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Opacity(
      opacity: _opacityTween.evaluate(animation),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        height: _sizeTween.evaluate(animation), // Aumenta la altura
        width: _sizeTween.evaluate(animation), // Aumenta el ancho
        child: SvgPicture.asset(login,
            color: globals.blue, semanticsLabel: 'login'),
      ),
    );
  }
}
