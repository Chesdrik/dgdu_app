import 'dart:convert';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/bd/UserBD.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

/// Send the current information about a user to login.
/// Receives register user data to display
///
/// {@category login}
/// {@subCategory Information user displays}
Future<bool> userLogin(String email, String password) async {
  String deviceId = await getDeviceId();

  Uri uri = globals.uriConstructor('/login');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'email': email,
      'password': password,
      'deviceId': deviceId
    }),
  );

  var jsonData = json.decode(data.body);

  if (jsonData == null) {
    globals.message = 'No hay conexión a internet';
    return false;
  } else {
    // print(jsonData);

    var user = jsonData['user'];

    if (!jsonData['error']) {
      var weight;
      var height;
      if (user['weight'].runtimeType == String) {
        weight = double.parse(user['weight']);
      } else {
        weight = user['weight'].toDouble();
      }

      if (user['height'].runtimeType == String) {
        height = double.parse(user['height']);
      } else {
        height = user['height'].toDouble();
      }
      UserRegister userregister = new UserRegister(
          id: user['id'],
          name: user['name'],
          last_name: user['last_name'],
          second_last_name: user['second_last_name'],
          birthday: user['birthdate'],
          curp: user['CURP'],
          gender: user['gender'],
          email: user['email'],
          password: password,
          weight: weight,
          height: height,
          worker_number: user['worker_number'],
          master: user['master'],
          campus: user['campus'],
          career: user['career']);

      UserRegister exist = await DBProvider.db.getUserRegisterEmail(email);
      if (exist != null) {
        await DBProvider.db.updateUserRegister(userregister);
      } else {
        await DBProvider.db.newUserRegister(userregister);
      }
      globals.user = userregister;
      return true;
    } else {
      _incrementCounter();
      globals.message = jsonData['message'];
      return false;
    }
  }
}

Future<bool> logoutApp() async {
  await DBProvider.db.deleteAll();
  return true;
}

Future<ResponseRequest> forgetPassword(String email) async {
  Uri uri = globals.uriConstructor('/reset_password');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'email': email,
    }),
  );

  var jsonData = json.decode(data.body);

  if (jsonData == null) {
    ResponseRequest response =
        new ResponseRequest(true, "No hay conexión a internet");
    return response;
  } else {
    // print(jsonData);
    ResponseRequest response =
        new ResponseRequest(jsonData['error'], jsonData['message']);
    return response;
  }
}

_incrementCounter() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int counter = (prefs.getInt('loginFails') ?? 0) + 1;
  await prefs.setInt('loginFails', counter);
  if (counter >= 2) {
    //we saved the data
    var now = DateTime.now();
    var expiredData = now.add(Duration(hours: 3));
    String dateFails = expiredData.toString();

    await prefs.setString('failsDate', dateFails);
  }
}
