import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/Views/Effort/EffortFunctions.dart';
import 'package:dgdu_app/bd/UserBD.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class UserScreen extends StatefulWidget {
  UserScreen({Key key}) : super(key: key);

  @override
  _EventsListScreenState createState() => _EventsListScreenState();
}

class _EventsListScreenState extends State<UserScreen> {
  static TextStyle general = TextStyle(color: Colors.white);

  UserRegister user;
  bool existConf = false;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    _conf();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'dashboard', 'Perfil', true),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
        child: (loading
            ? Loading()
            : Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10.0),
                    // width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(color: globals.blue),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 12.0),
                            child: Text(
                              user.name,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 45.0),
                            ),
                          ),
                          Text(
                            user.last_name,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          Text(
                            user.second_last_name,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 30.0, horizontal: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                _displayContent(
                                    'Peso', user.weight.toString() + " Kg"),
                                _displayContent(
                                    'Altura', user.height.toString() + " M"),
                                _displayContent(
                                    'IMC',
                                    (user.weight / (user.height * user.height))
                                        .toStringAsFixed(2)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 40.0),
                          ButtonTheme(
                            minWidth: 200.0,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        globals.blue),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                ),
                              ),
                              child: Text('Modificar datos',
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white)),
                              onPressed: () {
                                launchURL(
                                    'https://redpuma.unam.mx/registro/login.aspx');
                              },
                            ),
                          ),
                          // if (existConf)
                          //   ButtonTheme(
                          //     minWidth: 200.0,
                          //     child: ElevatedButton(
                          //       style: ButtonStyle(
                          //         backgroundColor:
                          //             MaterialStateProperty.all<Color>(
                          //                 globals.blue),
                          //         shape: MaterialStateProperty.all<
                          //             RoundedRectangleBorder>(
                          //           RoundedRectangleBorder(
                          //             borderRadius: BorderRadius.circular(18.0),
                          //           ),
                          //         ),
                          //       ),
                          //       child: Text('Actualizar datos del servidor',
                          //           style: TextStyle(
                          //               fontSize: 20, color: Colors.white)),
                          //       onPressed: () async {
                          //         print('Update data from server');

                          //         // Set loading screen
                          //         setState(() {
                          //           this.loading = true;
                          //         });

                          //         // Update user data from server
                          //         await updatePerfilData(context, this.user);

                          //         // Reload conf
                          //         await _conf();

                          //         setState(() {
                          //           this.loading = false;
                          //         });
                          //       },
                          //     ),
                          //   ),
                          SizedBox(height: 20.0),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
      ),
    );
  }

  _displayContent(String title, String value) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        Text(
          value,
          style: TextStyle(color: Colors.white70, fontSize: 16),
        )
      ],
    );
  }

  _conf() async {
    List<UserRegister> list = await DBProvider.db.getAllUserRegisters();
    if (list.isNotEmpty) {
      UserRegister aux = await DBProvider.db.getUserConfig();
      setState(() {
        this.existConf = true;
        this.user = aux;
        this.loading = false;
      });
    } else {
      setState(() {
        this.existConf = false;
        this.loading = false;
      });
    }
  }
}


                          
// Column(
//   children: <Widget>[
//     Text(
//       'Correo:',
//       style: TextStyle(fontSize: 20.0),
//     )
//   ],
// ),
// Column(
//   children: <Widget>[
//     Text(
//       user.email,
//       style: TextStyle(fontSize: 20.0),
//     )
//   ],
// ),
// SizedBox(height: 20.0),
// Column(
//   children: <Widget>[
//     Text(
//       'Fecha de nacimiento:',
//       style: TextStyle(fontSize: 20.0),
//     )
//   ],
// ),
// Column(
//   children: <Widget>[
//     Text(
//       user.birthday,
//       style: TextStyle(fontSize: 20.0),
//     )
//   ],
// ),