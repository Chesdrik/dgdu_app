import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter_svg/svg.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class LegalTermsScreen extends StatefulWidget {
  LegalTermsScreen({Key key}) : super(key: key);

  @override
  _EventsListScreenState createState() => _EventsListScreenState();
}

class _EventsListScreenState extends State<LegalTermsScreen> {
  String login = 'assets/logo.svg';
  var now = DateTime.now();
  void initState() {
    super.initState();
  }

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'dashboard', 'Acerca de', true),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        launchURL("https://unam.mx");
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: SvgPicture.asset(
                          'assets/logo_unam_svg.svg',
                          color: globals.blue,
                          semanticsLabel: 'UNAM',
                          fit: BoxFit.contain,
                          width: (MediaQuery.of(context).size.width * 0.28),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        launchURL("https://deporte.unam.mx");
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(right: 15.0),
                        child: SvgPicture.asset(
                          'assets/logo.svg',
                          color: globals.blue,
                          semanticsLabel: 'DGDU',
                          fit: BoxFit.contain,
                          width: (MediaQuery.of(context).size.width * 0.35),
                        ),
                      ),
                    ),
                  ],
                ),
                Text('DEPORTE UNAM', style: TextStyle(fontSize: 16)),
                Text('CONOCE EL VALOR DE TU ESFUERZO.',
                    style: TextStyle(fontSize: 16)),
                Text('IDIOMA: Español/México.', style: TextStyle(fontSize: 16)),
                Text('VERSIÓN 1.0.11', style: TextStyle(fontSize: 16)),
                Text('CATEGORÍA Y SUBCATEGORÍA:',
                    style: TextStyle(fontSize: 16)),
                Text('Deportes / Salud y forma física',
                    style: TextStyle(fontSize: 16)),
                ButtonTheme(
                  minWidth: 200.0,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(globals.blue),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text('POLÍTICA DE PRIVACIDAD',
                          style: TextStyle(fontSize: 16, color: Colors.white)),
                    ),
                    onPressed: () {
                      // Navigator.pushNamed(context, '/register');
                      launchURL(
                          'https://deporte.unam.mx/quienes_somos/aviso.php');
                    },
                  ),
                ),
                Text('Copyright @ ' + now.year.toString()),
                Text('UNAM | Dirección General del Deporte Universitario'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
