import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Models/HomeSlider.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';

// class News exteds
class News extends StatelessWidget {
  // const News({Key key}) : super(key: key);
  News(this._homeSlider);

  // HomeSlider object
  HomeSlider _homeSlider;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Noticias', true),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/fondo_gris.jpg"),
                  fit: BoxFit.cover)),
          child: Column(
            children: [
              CachedNetworkImage(
                imageUrl: this._homeSlider.image,
              ),
              SizedBox(height: 20),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 14.0, horizontal: 8.0),
                child: Column(
                  children: [
                    Text(
                      this._homeSlider.title,
                      style: TextStyle(fontSize: 25.0),
                    ),
                    SizedBox(height: 10),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        this._homeSlider.date,
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    SizedBox(height: 20),
                    Html(
                        data: this._homeSlider.content,
                        onLinkTap: (url) {
                          launchURL(url);
                        }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
