import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/bd/UserBD.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dgdu_app/Models/Sliders.dart';
import 'package:dgdu_app/Views/News/NewsFunctions.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/globals.dart' as globals;

class NewsList extends StatefulWidget {
  Sliders slider;

  NewsList({Key key, this.slider}) : super(key: key);

  @override
  _NewsListState createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  UserRegister user;
  bool existConf = false;
  _conf() async {
    List<UserRegister> list = await DBProvider.db.getAllUserRegisters();
    if (list.isNotEmpty) {
      UserRegister aux = await DBProvider.db.getUserConfig();
      setState(() {
        this.existConf = true;
        this.user = aux;
        globals.user = aux;
      });
    } else {
      setState(() {
        this.existConf = false;
      });
    }
  }

  int _currentSlider = 0;
  int _currentNews = 0;
  List<String> sliderList = [];
  List<String> newsList = [];
  List<Widget> imageSliders = [];
  List<Widget> newsSliders = [];

  @override
  void initState() {
    super.initState();

    _conf();
    this.imageSliders = this
        .widget
        .slider
        .sliders
        .map((item) => newsSliderToWidget(context, item))
        .toList();

    this.newsSliders = this
        .widget
        .slider
        .news
        .map((item) => newsSliderToWidget(context, item))
        .toList();
  }

  dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'dashboard', 'Noticias', true, true),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
        ),
        child: Column(children: [
          Spacer(flex: 2),
          Text(
            'Noticias',
            style: TextStyle(
                fontSize: 18.0,
                color: globals.blue,
                fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: CarouselSlider(
              items: newsSliders,
              options: CarouselOptions(
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 4),
                  enlargeCenterPage: true,
                  aspectRatio: 2.0,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _currentNews = index;
                    });
                  }),
            ),
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: sliderList.map((url) {
          //     int index = sliderList.indexOf(url);
          //     print(index);
          //     return Container(
          //       width: 8.0,
          //       height: 8.0,
          //       margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          //       decoration: BoxDecoration(
          //         shape: BoxShape.circle,
          //         color: _currentNews == index
          //             ? Color.fromRGBO(0, 0, 0, 0.9)
          //             : Color.fromRGBO(0, 0, 0, 0.4),
          //       ),
          //     );
          //   }).toList(),
          // ),
          Spacer(),
          Text(
            'Otro contenido',
            style: TextStyle(
                fontSize: 18.0,
                color: globals.blue,
                fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: CarouselSlider(
              items: imageSliders,
              options: CarouselOptions(
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 3),
                  enlargeCenterPage: true,
                  aspectRatio: 2.0,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _currentSlider = index;
                    });
                  }),
            ),
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: sliderList.map((url) {
          //     int index = sliderList.indexOf(url);
          //     return Container(
          //       width: 8.0,
          //       height: 8.0,
          //       margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
          //       decoration: BoxDecoration(
          //         shape: BoxShape.circle,
          //         color: _currentSlider == index
          //             ? Color.fromRGBO(0, 0, 0, 0.9)
          //             : Color.fromRGBO(0, 0, 0, 0.4),
          //       ),
          //     );
          //   }).toList(),
          // ),
          Spacer(flex: 2),
        ]),
      ),
    );
  }
}
