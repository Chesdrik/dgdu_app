import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/HomeSlider.dart';
import 'package:dgdu_app/Models/Sliders.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:dgdu_app/Views/News/News.dart';
import 'package:cached_network_image/cached_network_image.dart';

/// Get the current information about sliders.
///
/// {@category slidersInformation}
/// {@subCategory Information displays}
// ignore: missing_return
Future<Sliders> getSliders(BuildContext context) async {
  try {
    Uri uri = globals.uriConstructor('/dashboard');
    var response = await http.post(uri);

    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);

      // Parsing sliders
      List<HomeSlider> slidersList = [];
      for (var s in jsonData['sliders']) {
        HomeSlider img = new HomeSlider(
            s['title'], s['date'], s['url'], '', s['image'], 'slider');
        slidersList.add(img);
      }

      // Parsing news
      List<HomeSlider> newsList = [];
      for (var n in jsonData['news']) {
        HomeSlider img = new HomeSlider(
            n['title'], n['date'], n['url'], n['content'], n['image'], 'news');
        newsList.add(img);
      }

      // Returning list with sliders and news
      return new Sliders(sliders: slidersList, news: newsList);
    } else {
      // print('Request failed with status: ${response.statusCode}.');
    }
  } catch (e) {
    // print(e);
    Navigator.pushNamed(context, '/error');
  }
}

Widget newsSliderToWidget(BuildContext context, HomeSlider item) {
  return Container(
    child: Container(
      margin: EdgeInsets.all(5.0),
      child: GestureDetector(
        onTap: () {
          // print("Tapped a Container");
          if (item.type == 'news') {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => News(item)));
          } else if (item.type == 'slider') {
            // print('slider');
            // Checking if url is not empty (null)
            if (item.url != null) {
              launchURL(item.url);
            }
          }
        },
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            child: Stack(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: item.image,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      CircularProgressIndicator(
                          value: downloadProgress.progress),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),

                // Image.memory(
                //   base64Decode(item.image),
                //   fit: BoxFit.cover,
                //   width: double.infinity,
                // ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  right: 0.0,
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color.fromARGB(200, 0, 0, 0),
                          Color.fromARGB(150, 0, 0, 0)
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      ),
                    ),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    child: Text(
                      item.title,
                      // overflow: TextOverflow.fade,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      softWrap: false,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    ),
  );
}
