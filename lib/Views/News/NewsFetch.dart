import 'package:dgdu_app/Views/News/NewsList.dart';
import 'package:dgdu_app/Views/News/NewsFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class NewsFetch extends StatefulWidget {
  NewsFetch({Key key}) : super(key: key);

  @override
  NewsFetchState createState() => new NewsFetchState();
}

class NewsFetchState extends State<NewsFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getSliders(context),
        builder: (BuildContext context, AsyncSnapshot slider) {
          return (slider.data == null
              ? LoadingScreen()
              : NewsList(slider: slider.data));
        });
  }
}
