import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:dgdu_app/Views/Login/LoginFunctions.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  String descarga = 'assets/descarga.svg';
  var now = DateTime.now();

  bool existConf = false;

  @override
  void initState() {
    super.initState();
    _config();
  }

  _config() async {
    bool aux = await conf();
    setState(() {
      this.existConf = aux;
      // print(this.existConf);
    });
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context)
          .copyWith(canvasColor: globals.blue, brightness: Brightness.light),
      child: SizedBox(
        width: 160.0,
        child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              SizedBox(
                height: 40,
              ),
              buildMenuItem(Icons.sports_handball, "Tu esfuerzo",
                  opacity: 1.0,
                  color: globals.gold,
                  ruta: 'effort/main',
                  logout: false),
              buildMenuItem(Icons.event, "Eventos",
                  color: globals.gold,
                  opacity: 1.0,
                  ruta: 'events',
                  logout: false),
              buildMenuItem(Icons.chat_outlined, "Noticias",
                  color: globals.gold,
                  opacity: 1.0,
                  ruta: 'news/list',
                  logout: false),
              this.existConf
                  ? buildMenuItem(Icons.group, "Tus amigos",
                      color: globals.gold,
                      opacity: 1.0,
                      ruta: 'friendship',
                      logout: false)
                  : SizedBox(height: 0.0),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, '/download');
                },
                child: Opacity(
                  opacity: 1.0,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SvgPicture.asset(
                          descarga,
                          width: 80,
                          height: 80,
                          color: globals.gold,
                          semanticsLabel: 'descarga',
                        ),
                        Text(
                          'Descarga deporte',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14.0,
                              color: globals.gold),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              buildMenuItem(Icons.topic, "Programas",
                  color: globals.gold,
                  opacity: 1.0,
                  ruta: 'programs',
                  logout: false),
              this.existConf
                  ? buildMenuItem(Icons.person, "Perfil",
                      color: globals.gold,
                      opacity: 1.0,
                      ruta: 'user/resume',
                      logout: false)
                  : SizedBox(height: 0.0),
              this.existConf
                  ? buildMenuItem(Icons.logout, "Salir",
                      color: globals.gold, opacity: 1.0, logout: true)
                  : buildMenuItem(Icons.login, "Login",
                      color: globals.gold,
                      opacity: 1.0,
                      ruta: 'login',
                      logout: false),
              Divider(),
              // Spacer(),
              buildMenuItem(Icons.description, "Acerca de",
                  color: globals.gold,
                  opacity: 1.0,
                  logout: false,
                  ruta: 'legal/terms'),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'Copyright. ' + now.year.toString(),
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 10.0,
                      color: globals.gold),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  'UNAM | DGDU',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 10.0,
                      color: globals.gold),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem(IconData icon, String title,
      {double opacity = 0.3,
      Color color = Colors.black,
      String ruta,
      bool logout}) {
    return InkWell(
      onTap: () async {
        if (logout) {
          EasyLoading.show(status: 'Espere un momento...');
          bool response = await logoutApp();
          EasyLoading.dismiss();
          if (response) {
            Navigator.pushNamed(context, '/');
          } else {
            Fluttertoast.showToast(
              msg: "Hubo un error al cerrar sesión, intente en otro momento",
              backgroundColor: Colors.grey,
            );
          }
        } else {
          Navigator.pushNamed(context, '/$ruta');
        }
      },
      child: Opacity(
        opacity: opacity,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 10.0,
              ),
              Icon(
                icon,
                size: MediaQuery.of(context).size.width / 12,
                color: color,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 14.0, color: color),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
