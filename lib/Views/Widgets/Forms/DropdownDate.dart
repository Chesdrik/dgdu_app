import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;

class DropdownDate extends StatefulWidget {
  DropdownDate(
      {Key key, this.child, this.labelText, this.valueText, this.onPressed})
      : super(key: key);

  String labelText;
  String valueText;
  VoidCallback onPressed;
  Widget child;

  @override
  _DropdownDateState createState() => _DropdownDateState();
}

class _DropdownDateState extends State<DropdownDate> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onPressed,
      child: new InputDecorator(
        decoration: InputDecoration(
            enabledBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(10.0),
              borderSide: BorderSide(color: globals.blue),
              gapPadding: 5,
            ),
            focusedBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(10.0),
              borderSide: BorderSide(color: globals.blue),
              gapPadding: 5,
            ),
            labelText: widget.labelText,
            labelStyle: TextStyle(color: globals.blue),
            counterText: '',
            contentPadding: new EdgeInsets.symmetric(horizontal: 8.0),
            focusColor: Colors.white),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text(widget.valueText, style: TextStyle(color: globals.blue)),
            new Icon(Icons.arrow_drop_down,
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.grey.shade700
                    : Colors.white70),
          ],
        ),
      ),
    );
  }
}
