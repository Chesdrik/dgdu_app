import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;

class Dropdown extends StatefulWidget {
  String label;
  String value;
  List<DDEntry> values;
  Function(String) callback;

  Dropdown(this.label, this.value, this.values, this.callback);

  @override
  _DropdownState createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          DropdownButtonFormField<String>(
            isExpanded: true,
            value: widget.value.toString(),
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            dropdownColor: Colors.white,
            style: TextStyle(color: globals.blue),
            decoration: InputDecoration(
                enabledBorder: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                  borderSide: BorderSide(color: globals.blue),
                  gapPadding: 5,
                ),
                focusedBorder: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                  borderSide: BorderSide(color: globals.blue),
                  gapPadding: 5,
                ),
                labelText: widget.label,
                labelStyle: TextStyle(color: globals.blue),
                counterText: '',
                contentPadding: new EdgeInsets.symmetric(horizontal: 8.0),
                focusColor: globals.blue),
            onChanged: (String newValue) {
              setState(() {
                widget.callback(newValue);
                widget.value = newValue;
              });
            },
            hint: Text('Selecciona una opción'),
            items: widget.values
                .map<DropdownMenuItem<String>>(
                    (value) => new DropdownMenuItem<String>(
                          value: value.key,
                          child: new Text(value.value),
                        ))
                .toList(),
          ),
        ],
      ),
    );
  }
}
