import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;

class Button extends StatelessWidget {
  final String buttonLabel;
  final String route;

  const Button({Key key, this.buttonLabel, this.route}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          width: double.infinity,
          height: 150.0,
          child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(globals.gray),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                ),
              ),
              onPressed: () {
                // print("Pushing named route " + this.route);
                Navigator.pushNamed(context, this.route);
              },
              child: Container(
                margin: new EdgeInsets.symmetric(vertical: 35.0),
                child: Center(
                  child: Text(
                    this.buttonLabel,
                    style: new TextStyle(
                      color: Colors.white,
                      letterSpacing: 1.5,
                      fontFamily: "Poppins-Medium",
                      fontSize: 25,
                    ),
                  ),
                ),
              )),
        ),
      ),
    );
  }
}
