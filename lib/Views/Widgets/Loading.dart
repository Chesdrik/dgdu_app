import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: globals.blue,
      child: SpinKitThreeBounce(
        color: globals.gold,
        size: 30.0,
      ),
    );
  }
}

class ContaineddLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SpinKitThreeBounce(
        color: globals.gold,
        size: 30.0,
      ),
    );
  }
}

class TimerRunning extends StatelessWidget {
  final double size;

  TimerRunning(this.size);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SpinKitRipple(
        color: globals.gold,
        size: this.size,
      ),
    );
  }
}

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
        child: Center(
          child: Column(
            children: [
              Spacer(),
              TimerRunning(75.0),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
