import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/bd/UserBD.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);
  // const Dashboard(this._permission);
  // final Permission _permission;

  @override
  _DashboardState createState() => _DashboardState();
  // _DashboardState createState() => _DashboardState(_permission);
}

class _DashboardState extends State<Dashboard> {
  // _DashboardState(this._permission);

  bool _loadingUNAM = true;
  UserRegister user;
  bool existConf = false;
  // final Permission _permission;

  @override
  void initState() {
    _conf();
    super.initState();
  }

  Future<void> _conf() async {
    List<UserRegister> list = await DBProvider.db.getAllUserRegisters();
    if (list.isNotEmpty) {
      UserRegister aux = await DBProvider.db.getUserConfig();
      setState(() {
        this.existConf = true;
        this.user = aux;
        globals.user = this.user;
      });
    } else {
      setState(() {
        this.existConf = false;
      });
    }

    Future.delayed(const Duration(seconds: 3), () {
      setState(() {
        _loadingUNAM = false;
      });
      Future.delayed(const Duration(milliseconds: 200), () {
        checkPermissions();
      });
    });
  }

  Widget build(BuildContext context) {
    return (_loadingUNAM
        ? loadingScreen()
        : Scaffold(
            endDrawer: AppDrawer(),
            appBar: globalAppBar(context, 'dashboard', null, false, false),
            body: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/fondo_gris.jpg"),
                    fit: BoxFit.cover),
              ),
              child: Container(
                width: double.infinity,
                child: Column(children: [
                  Spacer(flex: 2),
                  GestureDetector(
                    onTap: () {
                      launchURL("https://deporte.unam.mx");
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 6.0),
                      child: SvgPicture.asset(
                        'assets/logo.svg',
                        color: globals.blue,
                        semanticsLabel: 'DGDU',
                        fit: BoxFit.contain,
                        width: MediaQuery.of(context).size.width / 3,
                      ),
                    ),
                  ),
                  Spacer(),
                  startEffortButton(context, 'dashboard'),
                  Spacer(flex: 2),
                ]),
              ),
            ),
          ));
  }

  Widget loadingScreen() {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/fondo_gris.jpg"),
                  fit: BoxFit.cover)),
          child: Center(
            child: Column(
              children: [
                Spacer(flex: 5),
                SvgPicture.asset(
                  'assets/logo_unam_svg.svg',
                  color: globals.blue,
                  semanticsLabel: 'DGDU',
                  fit: BoxFit.contain,
                  width: MediaQuery.of(context).size.width / 3,
                ),
                SizedBox(
                  height: 30,
                ),
                Text('Cargando...', style: TextStyle(fontSize: 16.0)),
                Spacer(),
                TimerRunning(75.0),
                Spacer(flex: 5),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Future<void> requestPermission(Permission permission) async {
  //   final status = await permission.request();

  //   setState(() {
  //     print(status);
  //     _permissionStatus = status;
  //     print(_permissionStatus);
  //   });
  // }

  Future<void> checkPermissions() async {
    // Fetching permissions
    final locationServiceStatus = Permission.locationAlways;
    bool locationGranted = await locationServiceStatus.isGranted;

    bool locationisPermanentlyDenied =
        await locationServiceStatus.isPermanentlyDenied;

    print("locationServiceStatus = " + locationServiceStatus.toString());
    print("locationGranted = " + locationGranted.toString());

    if (locationisPermanentlyDenied) {
      AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          headerAnimationLoop: false,
          animType: AnimType.TOPSLIDE,
          showCloseIcon: false,
          closeIcon: Icon(Icons.close_fullscreen_outlined),
          title: 'Advertencia',
          desc:
              'Esta aplicación recopila datos de la ubicación del usuario para habilitar la función de seguimiento deportivo, incluso cuando la aplicación está cerrada, en segundo plano o no está en uso. Los permisos fueron negados',
          btnCancelText: 'Cancelar',
          btnOkText: 'Otorgar',
          btnCancelOnPress: () {},
          btnOkOnPress: () async {
            try {
              await openAppSettings();
            } catch (e) {
              print('Error ------ $e');
            }
          })
        ..show();
    } else if (!locationGranted) {
      AwesomeDialog(
          context: context,
          dialogType: DialogType.WARNING,
          headerAnimationLoop: false,
          animType: AnimType.TOPSLIDE,
          showCloseIcon: false,
          closeIcon: Icon(Icons.close_fullscreen_outlined),
          title: 'Advertencia',
          desc:
              'Esta aplicación recopila datos de la ubicación del usuario para habilitar la función de seguimiento deportivo, incluso cuando la aplicación está cerrada, en segundo plano o no está en uso. ¿Deseas otorgar permisos de ubicación?',
          btnCancelText: 'Cancelar',
          btnOkText: 'Otorgar',
          btnCancelOnPress: () {},
          btnOkOnPress: () async {
            try {
              await locationServiceStatus.request();
            } catch (e) {
              print('Error ------ $e');
            }
          })
        ..show();
    }
  }
}
