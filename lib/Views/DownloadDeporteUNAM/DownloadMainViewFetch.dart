import 'package:dgdu_app/Views/DownloadDeporteUNAM/DownloadMainView.dart';
import 'package:dgdu_app/Views/DownloadDeporteUNAM/DownloadFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class DownloadMainViewFetch extends StatefulWidget {
  DownloadMainViewFetch({Key key}) : super(key: key);

  @override
  DownloadMainViewFetchState createState() => new DownloadMainViewFetchState();
}

class DownloadMainViewFetchState extends State<DownloadMainViewFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Services.information(context),
        builder: (BuildContext context, AsyncSnapshot videos) {
          return (videos.data == null
              ? LoadingScreen()
              : DownloadMainView(
                  videosList: videos.data,
                ));
        });
  }
}
