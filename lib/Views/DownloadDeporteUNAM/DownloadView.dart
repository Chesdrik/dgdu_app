import 'package:cached_network_image/cached_network_image.dart';
import 'package:dgdu_app/Models/ChannelInfo.dart';
import 'package:dgdu_app/Models/VideosList.dart';
import 'package:dgdu_app/Views/DownloadDeporteUNAM/DownloadFunctions.dart';
import 'package:dgdu_app/Views/DownloadDeporteUNAM/VideoView.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/material.dart';

class DownloadView extends StatefulWidget {
  String playListId;
  DownloadView({Key key, this.playListId}) : super(key: key);
  @override
  _DownloadViewState createState() => _DownloadViewState();
}

class _DownloadViewState extends State<DownloadView> {
  ChannelInfo _channelInfo;
  VideosList _videosList;
  Item _item;
  bool _loading;
  bool load;
  String _playListId;
  String _nextPageToken;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _loading = true;
    load = true;
    _nextPageToken = '';
    _scrollController = ScrollController();
    _videosList = VideosList();
    _videosList.videos = List();
    _getChannelInfo();
  }

  _getChannelInfo() async {
    _channelInfo = await Services.getChannelInfo();
    _item = _channelInfo.items[0];
    _playListId = widget.playListId;
    // print('_playListId $_playListId');
    await _loadVideos();
    setState(() {
      _loading = false;
    });
  }

  _loadVideos() async {
    VideosList tempVideosList = await Services.getVideosList(
      playListId: _playListId,
      pageToken: _nextPageToken,
    );
    _nextPageToken = tempVideosList.nextPageToken;
    _videosList.videos.addAll(tempVideosList.videos);
    // print('videos: ${_videosList.videos.length}');
    // print('_nextPageToken $_nextPageToken');
    setState(() {
      load = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _loading
        ? LoadingScreen()
        : Scaffold(
            endDrawer: AppDrawer(),
            appBar: globalAppBar(
                context, 'generic', 'Descarga UNAM', true, false, 'download'),
            body: Container(
              color: Colors.white,
              child: Column(
                children: [
                  _buildInfoView(),
                  Expanded(
                    child: NotificationListener<ScrollEndNotification>(
                      onNotification: (ScrollNotification notification) {
                        if (_videosList.videos.length >=
                            int.parse(_item.statistics.videoCount)) {
                          setState(() {
                            load = false;
                          });
                          return true;
                        }
                        if (notification.metrics.pixels ==
                            notification.metrics.maxScrollExtent) {
                          // print('entre');
                          _loadVideos();
                        }
                        setState(() {
                          load = true;
                        });
                        return false;
                      },
                      child: ListView.builder(
                        controller: _scrollController,
                        itemCount: _videosList.videos.length,
                        itemBuilder: (context, index) {
                          VideoItem videoItem = _videosList.videos[index];
                          return InkWell(
                            onTap: () async {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return VideoView(
                                  idVideo: widget.playListId,
                                  videoItem: videoItem,
                                );
                              }));
                            },
                            child: Container(
                              padding: EdgeInsets.all(20.0),
                              child: Row(
                                children: [
                                  CachedNetworkImage(
                                    imageUrl: videoItem
                                        .video.thumbnails.thumbnailsDefault.url,
                                  ),
                                  SizedBox(width: 20),
                                  Flexible(child: Text(videoItem.video.title)),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  load
                      ? Text('Para cargar más videos hacer scroll hacia abajo')
                      : SizedBox(
                          width: 0,
                        ),
                  load
                      ? TimerRunning(50.0)
                      : SizedBox(
                          width: 0,
                        ),
                ],
              ),
            ),
          );
  }

  _buildInfoView() {
    return _loading
        ? CircularProgressIndicator()
        : Container(
            padding: EdgeInsets.all(20.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    CircleAvatar(
                      backgroundImage: CachedNetworkImageProvider(
                        _item.snippet.thumbnails.medium.url,
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Text(
                        _item.snippet.title,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Text(_item.statistics.videoCount),
                    SizedBox(width: 20),
                  ],
                ),
              ),
            ),
          );
  }
}
