import 'package:cached_network_image/cached_network_image.dart';
import 'package:dgdu_app/Models/VideosList.dart';
import 'package:dgdu_app/Views/DownloadDeporteUNAM/DownloadView.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DownloadMainView extends StatefulWidget {
  List<InfoVideoDGDU> videosList;
  // VideosList videosList;

  DownloadMainView({Key key, this.videosList}) : super(key: key);

  @override
  _DownloadMainViewState createState() => _DownloadMainViewState();
}

class _DownloadMainViewState extends State<DownloadMainView> {
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'dashboard', 'Descarga UNAM', true),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                controller: _scrollController,
                itemCount: widget.videosList.length,
                itemBuilder: (context, index) {
                  VideoItem videoItem = widget.videosList[index].list.videos[0];
                  return InkWell(
                    onTap: () async {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return DownloadView(
                              playListId: widget
                                  .videosList[index].key, //aqui va el item id
                            );
                          },
                        ),
                      );
                    },
                    child: Container(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              child: Text(
                                widget.videosList[index].name,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                CachedNetworkImage(
                                  imageUrl: videoItem
                                      .video.thumbnails.thumbnailsDefault.url,
                                ),
                                SizedBox(width: 20),
                                Flexible(child: Text(videoItem.video.title)),
                              ],
                            ),
                          ],
                        )),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
