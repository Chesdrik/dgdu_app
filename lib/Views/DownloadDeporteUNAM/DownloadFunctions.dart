import 'dart:convert';
import 'dart:io';

import 'package:dgdu_app/Constants/Constants.dart';
import 'package:dgdu_app/Models/ChannelInfo.dart';
import 'package:dgdu_app/Models/VideosList.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:dgdu_app/globals.dart' as globals;

import 'dart:async';

class Services {
  static const CHANNEL_ID = 'UCEULH_CS7ew4E4AjU5EXUGA';
  static const PLAYLIST_ID = 'PLq-Y1HEHqDqnwHCzyXyfFcouPCLYCUw8S';
  static const _baseUrl = 'www.googleapis.com';

  /// Get information about a youtube's channel.
  ///
  /// {@category DownloadDeporteUNAM}
  /// {@subCategory Information displays}
  static Future<List<InfoVideoDGDU>> information(context) async {
    // print('videos request');
    try {
      Uri uri = globals.uriConstructor('/videos');
      var data = await http.post(
        uri,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      ).timeout(const Duration(seconds: 10));

      var jsonData = json.decode(data.body);
      List<InfoVideoDGDU> info = [];

      for (var v in jsonData['videos']) {
        VideosList list = await getVideosList(playListId: v['playlist_id']);
        InfoVideoDGDU infoVideo = new InfoVideoDGDU(
            name: v['title'], list: list, key: v['playlist_id']);
        info.add(infoVideo);
      }
      return info;
    } catch (e) {
      // print(e);
      Navigator.pushNamed(context, '/error');
    }
  }

  /// Get information about a youtube's channel.
  ///
  /// {@category DownloadDeporteUNAM}
  /// {@subCategory Information displays}
  static Future<ChannelInfo> getChannelInfo() async {
    Map<String, String> parameters = {
      'part': 'snippet,contentDetails,statistics',
      'id': CHANNEL_ID,
      'key': Constants.API_KEY,
    };
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/channels',
      parameters,
    );
    Response response = await http.get(uri, headers: headers);
    // print(response.body);
    ChannelInfo channelInfo = channelInfoFromJson(response.body);
    return channelInfo;
  }

  /// Get all videos of a youtube playlist.
  ///
  /// {@category DownloadDeporteUNAM}
  /// {@subCategory Information displays}
  static Future<VideosList> getVideosList(
      {String playListId, String pageToken}) async {
    Map<String, String> parameters = {
      'part': 'snippet',
      'playlistId': playListId,
      'maxResults': '8',
      'pageToken': pageToken,
      'key': Constants.API_KEY,
    };
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/playlistItems',
      parameters,
    );
    Response response = await http
        .get(uri, headers: headers)
        .timeout(const Duration(seconds: 10));
    ;
    // print(response.body);
    VideosList videosList = videosListFromJson(response.body);
    return videosList;
  }

  /// Get the current information about a youtube playlist.
  ///
  /// {@category DownloadDeporteUNAM}
  /// {@subCategory Information displays}
  static Future<VideosList> getPlayListInfo(
      {String playListId, String pageToken}) async {
    Map<String, String> parameters = {
      'part': 'snippet',
      'id': playListId,
      'maxResults': '8',
      'pageToken': pageToken,
      'key': Constants.API_KEY,
    };
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/playlists',
      parameters,
    );
    Response response = await http.get(uri, headers: headers);
    // print(response.body);
    VideosList videosList = videosListFromJson(response.body);
    return videosList;
  }
}
