import 'package:dgdu_app/Models/VideosList.dart';
import 'package:dgdu_app/Views/DownloadDeporteUNAM/DownloadView.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/services.dart';

import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoView extends StatefulWidget {
  final VideoItem videoItem;
  String idVideo;
  VideoView({this.videoItem, this.idVideo});

  @override
  _VideoViewState createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  //
  YoutubePlayerController _controller;
  bool _isPlayerReady;

  @override
  void initState() {
    super.initState();
    _isPlayerReady = false;
    _controller = YoutubePlayerController(
      initialVideoId: widget.videoItem.video.resourceId.videoId,
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
      ),
    )..addListener(_listener);
  }

  void _listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      //
    }
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    if (_controller.value.isPlaying) _controller.pause();
    _controller.removeListener(_listener);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    _controller.dispose();
    _controller = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: globals.blue,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (_controller.value.isPlaying) {
              _controller.pause();
              _controller.removeListener(_listener);
            }
            SystemChrome.setPreferredOrientations([
              DeviceOrientation.portraitUp,
            ]);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return DownloadView(
                    playListId: widget.idVideo,
                  );
                },
              ),
            );
            return Future.value(false);
          },
        ),
        title: titleUNAM('Descarga deporte'),
      ),
      body: WillPopScope(
        onWillPop: () async {
          if (_controller.value.isPlaying) _controller.pause();
          SystemChrome.setPreferredOrientations([
            DeviceOrientation.portraitUp,
          ]);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return DownloadView(
                  playListId: widget.idVideo,
                );
              },
            ),
          );
          return Future.value(false);
        },
        child: Container(
          child: YoutubePlayer(
            controller: _controller,
            showVideoProgressIndicator: true,
            onReady: () {
              // print('Player is ready.');
              _isPlayerReady = true;
            },
          ),
        ),
      ),
    );
  }
}
