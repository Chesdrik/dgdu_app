import 'package:dgdu_app/Models/Competitor.dart';
import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:dgdu_app/Models/Event.dart';
import 'package:dgdu_app/Models/EventInformation.dart';
import 'package:dgdu_app/Models/EventPast.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

/// Get the current information about events.
///
/// {@category EventsInformation}
/// {@subCategory Information displays}
// ignore: missing_return
Future<List<Event>> getEventsInfo(context) async {
  print("fetching events");
  print('/events/' + globals.user.id.toString());

  try {
    Uri uri = globals.uriConstructor('/events/' + globals.user.id.toString());
    var data = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'password': globals.user.password,
      }),
    );

    var jsonData = json.decode(data.body);
    print(jsonData);

    List<Event> events = [];
    for (var e in jsonData['events']) {
      Event event = new Event(
          id: e['id'],
          nombre: e['name'],
          date: e['date'].toString(),
          end_date: e['end_date'].toString(),
          asociated: e['asociado']);
      events.add(event);
    }
    return events;
  } catch (e) {
    // print(e);
    Navigator.pushNamed(context, '/error');
  }
}

/// Get the current information about events without user.
///
/// {@category EventsInformation}
/// {@subCategory Information displays}
// ignore: missing_return
Future<List<Event>> getEvents(context) async {
  // print('Sin info del usuario');
  try {
    Uri uri = globals.uriConstructor('/event');
    var data = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    var jsonData = json.decode(data.body);

    List<Event> events = [];
    for (var e in jsonData['events']) {
      if (e != null) {
        Event event = new Event(
            id: e['id'],
            nombre: e['name'],
            date: e['date'].toString(),
            end_date: e['end_date'].toString(),
            asociated: e['active'] == 1 ? true : false);
        events.add(event);
      }
    }
    return events;
  } catch (e) {
    // print(e);
    Navigator.pushNamed(context, '/error');
  }
}

/// Get the current information about event.
///
/// {@category EventInformation}
/// {@subCategory Information displays}
Future<EventInformation> getEventInfo(int idEvent) async {
  Uri uri = globals.uriConstructor(
      '/event/' + globals.user.id.toString() + '/' + idEvent.toString());
  // print('-------getEventInfo--------');
  // print(uri);
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );

  var jsonData = json.decode(data.body);

  // print(jsonData);

  EventInformation event = new EventInformation(
      id: jsonData['id'],
      nombre: jsonData['name'],
      date: jsonData['start'],
      maxUser: jsonData['max_users'],
      currentUser: jsonData['current_users']);

  return event;
}

/// Get the current information about event.
///
/// {@category EventInformation}
/// {@subCategory Information displays}
Future<EventInformation> getEventInfoWithOutUser(int idEvent) async {
  Uri uri = globals.uriConstructor('/event/' + idEvent.toString());
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );

  var jsonData = json.decode(data.body);

  // print('------------response---------');
  // print(uri);

  // print(jsonData);

  EventInformation event = new EventInformation(
      id: jsonData['event']['id'],
      nombre: jsonData['event']['name'],
      date: jsonData['event']['start'],
      maxUser: jsonData['event']['max_users'],
      currentUser: jsonData['event']['current_users']);

  return event;
}

/// Get the current information about past event.
///
/// {@category EventInformation}
/// {@subCategory Information displays}
Future<EventPast> getEventPastInfo(int idEvent) async {
  // /result/{event}/{user}/
  Uri uri = globals.uriConstructor(
      '/result/' + idEvent.toString() + '/' + globals.user.id.toString());
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );

  var jsonData = json.decode(data.body);
  // print(jsonData['results']);

  List<List<Competitor>> complete = [];

  globals.errorResponse = jsonData['error'];

  if (jsonData['error']) {
    globals.message = jsonData['error_message'];
  } else {
    // Iterating through results
    jsonData['results'].forEach((final String key, final value) {
      // print('super key');

      // print(key);
      // Creating categoryCompetitors List
      List<Competitor> categoryCompetitors = [];

      Competitor com = new Competitor(
        position: 0,
        name: key,
        folio: "",
        time: "",
        speed: "",
      );

      categoryCompetitors.add(com);

      // Iterating through results for that category
      for (var competitor in value['results']) {
        // print(competitor.toString());

        // Appending competitor to list
        categoryCompetitors.add(Competitor(
          position: competitor['position'],
          name: competitor['name'],
          folio: competitor['folio'],
          time: competitor['time'],
          speed: competitor['speed'],
        ));
      }

      // Appending List of competitors for that category in complete List
      complete.add(categoryCompetitors);
    });
  }

  // Creating EventPast object
  EventPast event = new EventPast(
      name: jsonData['event'],
      date: jsonData['date'],
      hour: jsonData['time'],
      distance: jsonData['distance'],
      route: 'ruta',
      competitors: complete);

  // Returning event
  return event;
}

/// Get the current information about categories.
///
/// {@category CategoryInformation}
/// {@subCategory Information displays}
Future<List<DDEntry>> getCategories(int eventId) async {
  // /result/{event}/{user}/
  Uri uri = globals.uriConstructor('/categories');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'event_id': eventId.toString(),
    }),
  );
  print('id event----------');
  print(eventId);
  print(uri);

  var jsonData = json.decode(data.body);
  print(jsonData['categories']);

  List<DDEntry> entryCategories = [];
  if (jsonData['error']) {
    globals.message = jsonData['message'];
  } else {
    // Iterating through results
    for (var e in jsonData['categories']) {
      DDEntry entry = DDEntry(key: e['id'].toString(), value: e['name']);

      entryCategories.add(entry);
    }
  }

  // Returning categories
  return entryCategories;
}

/// Inscription user to event.
///
/// {@category EventInformation}
/// {@subCategory Information displays}
Future<ResponseRequest> eventInscription(
    String idEvent, String idCategory) async {
  Uri uri = globals.uriConstructor('/user/event/register');
  // print(uri);

  print('/user/event/register');
  print("Inscribiendo al usuario al evento " +
      idEvent +
      " a la categoría " +
      idCategory);

  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'user_id': globals.user.id.toString(),
      'category_id': idCategory,
      'event_id': idEvent,
      'team_name': '',
      'team_password': ''
    }),
  );

  var jsonData = json.decode(data.body);

  // print(jsonData);

  ResponseRequest response =
      new ResponseRequest(jsonData['error'], jsonData['message']);

  return response;
}
