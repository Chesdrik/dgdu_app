import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Models/Event.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class EventsListScreen extends StatefulWidget {
  bool login;
  List<Event> events;

  EventsListScreen({Key key, this.events, this.login}) : super(key: key);

  @override
  _EventsListScreenState createState() => _EventsListScreenState();
}

class _EventsListScreenState extends State<EventsListScreen> {
  static TextStyle general = TextStyle(color: Colors.white);
  List<Event> past = [];
  List<Event> future = [];
  void initState() {
    super.initState();
    for (var e in widget.events) {
      // print("End date: " + e.end_date);
      if (checkDate(e.end_date)) {
        past.add(e);
      } else {
        future.add(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        endDrawer: AppDrawer(),
        appBar: AppBar(
          brightness: Brightness.dark,
          backgroundColor: globals.blue,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushNamed(context, '/');
            },
          ),
          title: titleUNAM('Eventos'),
          bottom: TabBar(
            tabs: [
              Tab(
                text: 'Eventos próximos',
              ),
              Tab(
                text: 'Eventos pasados',
              ),
            ],
          ),
        ),
        backgroundColor: globals.blue,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
          ),
          child: TabBarView(
            children: [
              _listEvent(this.future),
              _listEvent(this.past),
            ],
          ),
        ),
      ),
    );
  }

  Widget _listEvent(List<Event> event) {
    return Padding(
      padding: EdgeInsets.all(1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 0,
          ),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: event.length,
              physics: ScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  elevation: 3,
                  shadowColor: globals.gold,
                  color: globals.blue,
                  child: ExpansionTile(
                    title: Text(event[index].nombre, style: general),
                    subtitle: checkDate(event[index].date)
                        ? Text(
                            'Inactivo',
                            style: general,
                          )
                        : Text(
                            'Activo',
                            style: general,
                          ),
                    children: <Widget>[
                      Text(
                        // 'Fecha: ' + event[index].date,
                        'Fecha: ' +
                            event[index]
                                .date
                                .substring(0, event[index].date.length - 3),
                        style: general,
                        textAlign: TextAlign.left,
                      ),
                      widget.login
                          ? Text(
                              'Inscrito: ' +
                                  (event[index].asociated ? 'SI' : 'No'),
                              style: general)
                          : SizedBox(
                              width: 0,
                              height: 0,
                            ),
                      TextButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(globals.blue),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(1.0),
                                side: BorderSide(color: Colors.white)),
                          ),
                        ),
                        onPressed: () {
                          //check if the user has login
                          if (widget.login) {
                            if (checkDate(event[index].date)) {
                              //the event has already passed and show the results
                              // print('evento pasado');
                              Navigator.pushNamed(context, '/event/past',
                                  arguments:
                                      Arguments('past', event[index].id));
                            } else {
                              //the event has not happened yet, check if is associated
                              if (event[index].asociated) {
                                //if the user is asociated, then add effort, show button to add effor
                                Navigator.pushNamed(context, '/user/event',
                                    arguments:
                                        Arguments('event', event[index].id));
                              } else {
                                //else show info about event, only show event information
                                Navigator.pushNamed(context, '/user/event',
                                    arguments:
                                        Arguments('register', event[index].id));
                              }
                            }
                          }
                          //  TODO: se necesita api para ver un evento pasado sin usuario registrado
                          // else if (checkDate(event[index].date)) {
                          //   //the event has already passed and show the results
                          //   print('evento pasado');
                          //   Navigator.pushNamed(context, '/event/past',
                          //       arguments: Arguments('past', event[index].id));
                          // }
                          else {
                            //without login
                            Navigator.pushNamed(context, '/user/event',
                                arguments: Arguments('info', event[index].id));
                          }
                        },
                        child: Text(
                          "Ver Evento",
                          style: general,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  /// Get the boolean about the event date is after of the current date.
  ///
  /// {@category EventInformation}
  /// {@subCategory Information displays}
  bool checkDate(String date) {
    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
    var now = DateTime.now();
    return now.isAfter(tempDate);
  }
}
