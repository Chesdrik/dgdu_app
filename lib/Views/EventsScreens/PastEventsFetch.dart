import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Views/EventsScreens/EventFunctions.dart';
import 'package:dgdu_app/Views/EventsScreens/PastEventsScreen.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class PastEventsFetch extends StatefulWidget {
  PastEventsFetch({Key key}) : super(key: key);

  @override
  PastEventsFetchState createState() => new PastEventsFetchState();
}

class PastEventsFetchState extends State<PastEventsFetch> {
  @override
  Widget build(BuildContext context) {
    final Arguments args = ModalRoute.of(context).settings.arguments;
    return FutureBuilder(
        future: getEventPastInfo(args.id),
        builder: (BuildContext context, AsyncSnapshot event) {
          return (event.data == null
              ? Loading()
              : PastEventsScreen(event: event.data));
        });
  }
}
