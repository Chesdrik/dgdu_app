import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Views/EventsScreens/EventFunctions.dart';
import 'package:dgdu_app/Views/EventsScreens/EventsListScreen.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class EventsFetch extends StatefulWidget {
  EventsFetch({Key key}) : super(key: key);

  @override
  EventsFetchState createState() => new EventsFetchState();
}

class EventsFetchState extends State<EventsFetch> {
  bool login = false;
  void initState() {
    super.initState();
    _config();
  }

  _config() async {
    bool aux = await conf();

    setState(() {
      this.login = aux;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: login ? getEventsInfo(context) : getEvents(context),
        builder: (BuildContext context, AsyncSnapshot events) {
          return (events.data == null
              ? LoadingScreen()
              : EventsListScreen(
                  events: events.data,
                  login: login,
                ));
        });
  }
}
