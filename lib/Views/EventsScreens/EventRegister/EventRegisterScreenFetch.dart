import 'package:dgdu_app/Views/EventsScreens/EventFunctions.dart';
import 'package:dgdu_app/Views/EventsScreens/EventRegister/EventRegisterScreen.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class EventRegisterScreenFetch extends StatefulWidget {
  EventRegisterScreenFetch({Key key}) : super(key: key);

  @override
  EventRegisterScreenFetchState createState() =>
      new EventRegisterScreenFetchState();
}

class EventRegisterScreenFetchState extends State<EventRegisterScreenFetch> {
  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args = ModalRoute.of(context).settings.arguments;
    return FutureBuilder(
        future: getCategories(1),
        builder: (BuildContext context, AsyncSnapshot categories) {
          return (categories.data == null
              ? LoadingScreen()
              : EventRegisterScreen(
                  categories: categories.data, arguments: args));
        });
  }
}
