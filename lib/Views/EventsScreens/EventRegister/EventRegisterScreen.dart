import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:dgdu_app/Views/EventsScreens/EventFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Forms/Dropdown.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;

// ignore: must_be_immutable
class EventRegisterScreen extends StatefulWidget {
  List<DDEntry> categories;
  final Map<String, dynamic> arguments;

  EventRegisterScreen({
    Key key,
    this.categories,
    this.arguments,
  }) : super(key: key);

  @override
  _EventRegisterScreenState createState() => _EventRegisterScreenState();
}

/// This is the private State class that goes with EventRegisterScreen.
class _EventRegisterScreenState extends State<EventRegisterScreen> {
  Gender gender = Gender.hombre;
  Type type = Type.interior;
  List<DDEntry> categories = [];
  String teamName = '';
  String teamPassword = '';

  int _index = 0;

  var idCategory = '0';

  initState() {
    super.initState();
    getCategoriesAPI(widget.arguments['event'].id);
    this.idCategory = widget.categories.first.key;
  }

  dispose() {
    super.dispose();
  }

  _updateCategory(_newValue) {
    setState(() {
      this.idCategory = _newValue;
      // print(idCategory);
    });
  }

  Future<List<DDEntry>> getCategoriesAPI(int eventId) async {
    List<DDEntry> aux = await getCategories(eventId);
    setState(() {
      this.categories = aux;
      this.idCategory = this.categories.first.key;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: globalAppBar(context, 'generic', 'Inscripción', true),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Stepper(
                  controlsBuilder: (BuildContext context,
                      {VoidCallback onStepContinue,
                      VoidCallback onStepCancel}) {
                    return Row(
                      children: <Widget>[
                        TextButton(
                          onPressed: onStepContinue,
                          child: const Text('Siguiente'),
                        ),
                        TextButton(
                          onPressed: onStepCancel,
                          child: const Text('Cancelar'),
                        ),
                      ],
                    );
                  },
                  currentStep: _index,
                  onStepCancel: () {
                    if (_index <= 0) {
                      return;
                    }
                    setState(() {
                      _index--;
                    });
                  },
                  onStepContinue: () {
                    if (_index > 1) {
                      return;
                    }
                    setState(() {
                      _index++;
                    });
                  },
                  onStepTapped: (index) {
                    setState(() {
                      _index = index;
                    });
                  },
                  steps: [
                    Step(
                      title: Text("Categoría"),
                      content: Column(
                        children: <Widget>[
                          Dropdown('Categorias:', this.idCategory,
                              widget.categories, _updateCategory),
                        ],
                      ),
                    ),
                    Step(
                      title: Text("Nombre del equipo"),
                      content: Column(
                        children: <Widget>[
                          Text(
                              "En caso de no conocer el nombre del equipo se creará uno nuevo, si no es el caso, introducir el nombre y contraseña del equipo."),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            style: TextStyle(color: globals.blue),
                            keyboardType: TextInputType.text,
                            maxLength: 100,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: BorderSide(color: globals.blue),
                                  gapPadding: 5,
                                ),
                                focusedBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: BorderSide(color: globals.blue),
                                  gapPadding: 5,
                                ),
                                labelText: 'Nombre del equipo:',
                                labelStyle: TextStyle(color: globals.blue),
                                counterText: '',
                                contentPadding:
                                    new EdgeInsets.symmetric(horizontal: 8.0)),
                            onSaved: (text) => this.teamName = text.trim(),
                          ),
                        ],
                      ),
                    ),
                    Step(
                      title: Text("Contraseña del equipo"),
                      content: Column(
                        children: <Widget>[
                          TextFormField(
                            style: TextStyle(color: globals.blue),
                            keyboardType: TextInputType.text,
                            maxLength: 100,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: BorderSide(color: globals.blue),
                                  gapPadding: 5,
                                ),
                                focusedBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: BorderSide(color: globals.blue),
                                  gapPadding: 5,
                                ),
                                labelText: 'Contraseña del equipo:',
                                labelStyle: TextStyle(color: globals.blue),
                                counterText: '',
                                contentPadding:
                                    new EdgeInsets.symmetric(horizontal: 8.0)),
                            onSaved: (text) => this.teamPassword = text.trim(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                ButtonTheme(
                  minWidth: 200.0,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(globals.blue),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                      ),
                    ),
                    child: Text('CONTINUAR',
                        style: TextStyle(fontSize: 20, color: Colors.white)),
                    onPressed: () {
                      if (type == Type.exterior) {
                        //views to register exterior effort
                      } else {
                        //views to register inside effort
                      }
                    },
                  ),
                ),
              ],
            ),
          )),
    );
  }
}

enum Gender { hombre, mujer }
enum Category { Cch, Alumno, Licenciatura, Posgrado }
enum Type { interior, exterior }
