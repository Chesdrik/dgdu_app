import 'package:dgdu_app/Models/EventPast.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PastEventsScreen extends StatelessWidget {
  EventPast event;
  PastEventsScreen({Key key, this.event}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Resultado de eventos', true),
      body: Column(
        children: [
          Expanded(
            flex: null,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(color: globals.blue),
                  child: Center(
                    child: Column(
                      children: [
                        Column(
                          children: <Widget>[
                            Text(
                              event.name,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 30.0),
                            )
                          ],
                        ),
                        SizedBox(height: 20.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            VerticalDivider(),
                            Column(
                              children: <Widget>[
                                Text(
                                  'Fecha',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Text(
                                  event.date,
                                  // event.date.substring(0, event.date.length - 3),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                )
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 10.0),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text('Resultados',
              style: TextStyle(color: globals.blue, fontSize: 20)),
          globals.errorResponse
              ? Padding(
                  padding: const EdgeInsets.only(top: 11.0),
                  child: Text(
                    globals.message,
                    style: TextStyle(color: globals.blue, fontSize: 25),
                    textAlign: TextAlign.center,
                  ),
                )
              : SizedBox(
                  height: 10,
                ),
          Expanded(
            flex: 1,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.50,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: event.competitors.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: double.infinity,
                    // width: 400,
                    width: MediaQuery.of(context).size.width,
                    // width: double.infinity,
                    child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: event.competitors[index].length,
                      itemBuilder: (BuildContext context, int index1) {
                        return ListTile(
                          dense: true,
                          leading: index1 == 0
                              ? SizedBox(
                                  height: 0,
                                )
                              : CircleAvatar(
                                  child: Text(
                                      event.competitors[index][index1].position
                                          .toString(),
                                      style: TextStyle(
                                          color: globals.gold, fontSize: 13.0)),
                                  backgroundColor: globals.blue,
                                ),
                          title: index1 == 0
                              ? Text(
                                  event.competitors[index][index1].name,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      color: globals.blue, fontSize: 20),
                                )
                              : Row(
                                  // color: Colors.green,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  // crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 3,
                                      child: Text(
                                        event.competitors[index][index1].folio +
                                            " - " +
                                            event.competitors[index][index1]
                                                .name,
                                        style: TextStyle(
                                            color: globals.blue, fontSize: 14),
                                      ),
                                    ),
                                    Spacer(),
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        event.competitors[index][index1].time,
                                        style: TextStyle(
                                            color: globals.blue, fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                        );
                      },
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
