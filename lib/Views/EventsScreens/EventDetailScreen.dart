import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:dgdu_app/Models/EventInformation.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Views/EventsScreens/EventFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Forms/Dropdown.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class EventDetailScreen extends StatefulWidget {
  EventInformation event;
  Arguments arguments;

  EventDetailScreen({Key key, this.event, this.arguments}) : super(key: key);

  @override
  EventDetailScreenState createState() => EventDetailScreenState();
}

/// This is the private State class that goes with EventRegisterScreen.
class EventDetailScreenState extends State<EventDetailScreen> {
  List<DDEntry> categories = [];

  var idCategory = '0';

  initState() {
    super.initState();
    getCategoriesAPI(widget.arguments.id);
  }

  dispose() {
    super.dispose();
  }

  _updateCategory(_newValue) {
    setState(() {
      this.idCategory = _newValue;
      // print(idCategory);
    });
  }

  Future<List<DDEntry>> getCategoriesAPI(int eventId) async {
    List<DDEntry> aux = await getCategories(eventId);
    setState(() {
      this.categories = aux;
      this.idCategory = this.categories.first.key;
    });
    print(this.categories);
  }

  @override
  Widget build(BuildContext context) {
    var levelIndicator = Container(
      child: Container(
        child: LinearProgressIndicator(
            backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
            value: 1,
            valueColor: AlwaysStoppedAnimation(Colors.green)),
      ),
    );

    var eventDate = Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        // event.date,
        widget.event.date.substring(0, widget.event.date.length - 3),
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );

    var topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.event.nombre,
          style: TextStyle(color: Colors.white, fontSize: 35.0),
        ),
        SizedBox(height: 20.0),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.start,
        //   children: <Widget>[
        //     Expanded(flex: 1, child: levelIndicator),
        //     Expanded(
        //         flex: 6,
        //         child: Padding(
        //             padding: EdgeInsets.only(left: 10.0),
        //             child: Text(
        //               'widget.event.range',
        //               style: TextStyle(color: Colors.white),
        //             ))),
        //     Expanded(flex: 6, child: eventDate)
        //   ],
        // ),
      ],
    );

    var topContent = Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: globals.blue),
          child: Center(
            child: topContentText,
          ),
        ),
      ],
    );

    var bottomContentText = Text(
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
      style: TextStyle(fontSize: 18.0),
    );
    var readButton = Container(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      width: MediaQuery.of(context).size.width,
      child: widget.arguments.type == 'info'
          ? Text(
              'Necesitas registrarte en la aplicación para ingresar al evento',
              style: TextStyle(fontSize: 20, color: globals.blue))
          : ButtonTheme(
              minWidth: 200.0,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(globals.blue),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                child: typeText(widget.arguments.type),
                onPressed: () {
                  if (widget.arguments.type == 'event') {
                    Navigator.pushNamed(context, '/effort/register',
                        arguments: widget.arguments); //registro de esfuerzo
                  } else if (widget.arguments.type == 'register') {
                    // Navigator.pushNamed(context,
                    //     '/event/register'); //registro ya en la plataforma
                    AwesomeDialog(
                        context: context,
                        dialogType: DialogType.NO_HEADER,
                        headerAnimationLoop: true,
                        animType: AnimType.BOTTOMSLIDE,
                        title: 'Escoger categoria',
                        body: Column(
                          children: <Widget>[
                            Text('Escoge una categoría'),
                            SizedBox(
                              height: 30,
                            ),
                            Dropdown('Categorias:', this.idCategory,
                                this.categories, _updateCategory),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                        btnCancel: TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("CERRAR",
                              style: TextStyle(
                                  fontSize: 11,
                                  color: globals.blue.withOpacity(0.75))),
                        ),
                        btnOk: TextButton(
                          onPressed: () async {
                            print('Clicked');
                            print(widget.arguments.id.toString());
                            print(this.idCategory.toString());

                            EasyLoading.show(status: 'Espere un momento...');
                            ResponseRequest response = await eventInscription(
                                widget.arguments.id.toString(),
                                this.idCategory);
                            EasyLoading.dismiss();

                            if (response.error) {
                              // Show error notification
                              Fluttertoast.showToast(
                                  msg: response.message,
                                  backgroundColor: Colors.red,
                                  timeInSecForIosWeb: 5);
                            } else {
                              // Show notification
                              Fluttertoast.showToast(
                                  msg: response.message,
                                  backgroundColor: Colors.green,
                                  timeInSecForIosWeb: 5);

                              // Return to view
                              await Future.delayed(
                                  const Duration(milliseconds: 1000), () {
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    '/events', (Route<dynamic> route) => false);
                              });
                            }
                          },
                          child: Text("INSCRIBIRSE",
                              style: TextStyle(
                                  fontSize: 11,
                                  color: globals.blue.withOpacity(0.75))),
                        ),
                        btnOkOnPress: () async {})
                      ..show();
                  }
                  //
                },
              ),
            ),
    );

    var bottomContent = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(40.0),
      child: Center(
        child: Column(
          children: <Widget>[readButton],
        ),
      ),
    );

    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Evento', true),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[topContent, bottomContent],
        ),
      ),
    );
  }

  /// Get Text widget based it the type.
  ///
  /// {@category Information}
  /// {@subCategory Information displays}
  Widget typeText(String type) {
    if (type == 'event') {
      return Text('AGREGAR ESFUERZO',
          style: TextStyle(fontSize: 20, color: Colors.white));
    } else if (type == 'register') {
      return Text('INSCRIBIRSE',
          style: TextStyle(fontSize: 20, color: Colors.white));
    } else {
      return Text('Necesitas registrarte en DGDU para ingresar al evento',
          style: TextStyle(fontSize: 20, color: Colors.white));
    }
  }
}
