import 'package:dgdu_app/Models/Arguments.dart';
import 'package:dgdu_app/Views/EventsScreens/EventDetailScreen.dart';
import 'package:dgdu_app/Views/EventsScreens/EventFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';

class EventDetailFetch extends StatefulWidget {
  EventDetailFetch({Key key}) : super(key: key);

  @override
  EventDetailFetchState createState() => new EventDetailFetchState();
}

class EventDetailFetchState extends State<EventDetailFetch> {
  bool login = false;
  void initState() {
    super.initState();
    _config();
  }

  _config() async {
    this.login = await conf();
  }

  @override
  Widget build(BuildContext context) {
    final Arguments args = ModalRoute.of(context).settings.arguments;
    return FutureBuilder(
        future:
            login ? getEventInfo(args.id) : getEventInfoWithOutUser(args.id),
        builder: (BuildContext context, AsyncSnapshot event) {
          return (event.data == null
              ? Loading()
              : EventDetailScreen(
                  event: event.data,
                  arguments: args,
                ));
        });
  }
}
