import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:dgdu_app/Models/Arguments.dart';

Widget globalAppBar(BuildContext context, String _type,
    [String _sectionTitle, bool _backButton, bool _popToHome, String route]) {
  if (_type == 'dashboard') {
    return AppBar(
        backgroundColor: globals.blue,
        brightness: Brightness.dark,
        centerTitle: true,
        // leading: (_backButton != null ? ),
        leading: (_backButton != null && _backButton
            ? IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  if (_popToHome != null && _popToHome != false) {
                    Navigator.of(context).pop();
                  } else {
                    Navigator.popUntil(context, ModalRoute.withName('/'));
                  }
                },
              )
            : SizedBox(width: 0.0)),
        title: titleUNAM(_sectionTitle));
  } else if (_type == 'register_effort') {
    return AppBar(
      brightness: Brightness.dark,
      backgroundColor: globals.blue,
      title: titleUNAM(_sectionTitle),
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        },
        // tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    );
  } else {
    return AppBar(
      brightness: Brightness.dark,
      backgroundColor: globals.blue,
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () {
          route == null
              ? Navigator.pop(context)
              : Navigator.pushNamed(context, '/$route');
        },
      ),
      title: titleUNAM(_sectionTitle),
    );
  }
}

Widget startEffortButton(BuildContext context, String arguments) {
  return TextButton.icon(
    onPressed: () {
      Navigator.pushNamed(context, '/effort/register',
          arguments: Arguments(arguments, 0));
    },
    icon: Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Icon(
        Icons.directions_run_outlined,
        color: globals.gold,
      ),
    ),
    label: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        'Inicia tu ejercicio',
        style: TextStyle(color: Colors.white, fontSize: 16.0),
      ),
    ),
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.all<Color>(globals.blue),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      elevation: MaterialStateProperty.all<double>(5.0),
    ),
  );
}

Widget titleUNAM(_sectionTitle) {
  return Row(
    children: [
      (_sectionTitle == null
          ? SizedBox(
              width: 0.0,
            )
          : Text(
              _sectionTitle,
              style: TextStyle(fontSize: 14),
            )),
      Spacer(),
      GestureDetector(
        onTap: () {
          launchURL("https://deporte.unam.mx");
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: SvgPicture.asset(
            'assets/logo.svg',
            color: Colors.white,
            semanticsLabel: 'DGDU',
            fit: BoxFit.contain,
            height: 35.0,
          ),
        ),
      ),
      GestureDetector(
        onTap: () {
          launchURL("https://unam.mx");
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: SvgPicture.asset(
            'assets/logo_unam_svg.svg',
            color: Colors.white,
            semanticsLabel: 'UNAM',
            fit: BoxFit.contain,
            height: 35.0,
          ),
        ),
      ),
      (_sectionTitle == null ? Spacer() : SizedBox(width: 0.0)),
      // Spacer(),
    ],
  );
}
