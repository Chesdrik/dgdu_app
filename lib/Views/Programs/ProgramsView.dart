import 'package:dgdu_app/Models/Programs.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';

class ProgramsView extends StatefulWidget {
  List<Program> program;

  ProgramsView({Key key, this.program}) : super(key: key);

  @override
  _ProgramsViewState createState() => _ProgramsViewState();
}

class _ProgramsViewState extends State<ProgramsView> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Programas', true),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
          ),
          child: Container(
            width: double.infinity,
            child: _listFriedship(),
          ),
        ),
      ),
    );
  }

  Widget _listFriedship() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.program.length,
      physics: ScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: Text(widget.program[index].name),
          onTap: () {
            Navigator.pushNamed(context, '/program',
                arguments: widget.program[index]);
          },
        );
      },
    );
  }
}
