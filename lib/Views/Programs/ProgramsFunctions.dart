import 'package:dgdu_app/Models/Programs.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

/// Get the current information about programs's DGDU.
///
/// {@category ProgramsInformation}
/// {@subCategory Information displays}
Future<List<Program>> getProgramsInfo(context) async {
  // print('info del usuario');
  try {
    Uri uri = globals.uriConstructor('/content');
    var data = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    var jsonData = json.decode(data.body);

    // print(jsonData);

    List<Program> programs = [];

    if (jsonData['programs'].isNotEmpty) {
      for (var e in jsonData['programs']) {
        var content = e['content'] != null ? e['content'] : '';
        Program program = new Program(
            id: e['id'],
            name: e['key'],
            content: content,
            lat: e['lat'],
            long: e['long']);
        programs.add(program);
      }
    }

    return programs;
  } catch (e) {
    // print(e);
    Navigator.pushNamed(context, '/error');
  }
}
