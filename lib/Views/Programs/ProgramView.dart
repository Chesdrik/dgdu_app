import 'package:dgdu_app/Models/Programs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';

class ProgramView extends StatelessWidget {
  ProgramView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final program = ModalRoute.of(context).settings.arguments as Program;

    return Scaffold(
      backgroundColor: Colors.grey[300],
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', '', true),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 14.0, horizontal: 8.0),
                child: Column(
                  children: [
                    Text(
                      program.name,
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Html(
                      data: program.content,
                      onLinkTap: (url) {
                        // print(url);
                        launchURL(url);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
