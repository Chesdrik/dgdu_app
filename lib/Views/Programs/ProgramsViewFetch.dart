import 'package:dgdu_app/Views/Programs/ProgramsFunctions.dart';
import 'package:dgdu_app/Views/Programs/ProgramsView.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class ProgramsViewFetch extends StatefulWidget {
  ProgramsViewFetch({Key key}) : super(key: key);

  @override
  ProgramsViewFetchState createState() => new ProgramsViewFetchState();
}

class ProgramsViewFetchState extends State<ProgramsViewFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getProgramsInfo(context),
        builder: (BuildContext context, AsyncSnapshot programs) {
          return (programs.data == null
              ? LoadingScreen()
              : ProgramsView(
                  program: programs.data,
                ));
        });
  }
}
