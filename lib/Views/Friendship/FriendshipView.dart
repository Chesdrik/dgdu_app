import 'package:dgdu_app/Models/Friendships.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/Views/Friendship/FriendshipFunctions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Functions/Helpers/HelperFunctions.dart';
import 'package:badges/badges.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:like_button/like_button.dart';
import 'package:email_validator/email_validator.dart';

class FriendshipView extends StatefulWidget {
  Friendships friends;

  FriendshipView({Key key, this.friends}) : super(key: key);

  @override
  _FriendshipViewState createState() => _FriendshipViewState();
}

class _FriendshipViewState extends State<FriendshipView>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  UserRegister user;
  bool liked = false;
  bool existConf = false;
  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  String _correo;
  GlobalKey<FormState> _key = GlobalKey();

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
      reverseDuration: Duration(milliseconds: 400),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: AppBar(
        backgroundColor: globals.blue,
        brightness: Brightness.dark,
        // centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("Tus Amigos"),
            // Spacer(),
            GestureDetector(
              onTap: () {
                launchURL("https://deporte.unam.mx");
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 2.0),
                child: SvgPicture.asset(
                  'assets/logo.svg',
                  color: Colors.white,
                  semanticsLabel: 'DGDU',
                  fit: BoxFit.contain,
                  height: 32.0,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                launchURL("https://unam.mx");
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 2.0),
                child: SvgPicture.asset(
                  'assets/logo_unam_svg.svg',
                  color: Colors.white,
                  semanticsLabel: 'UNAM',
                  fit: BoxFit.contain,
                  height: 32.0,
                ),
              ),
            ),
            // Spacer(),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.person_add),
            onPressed: () {
              AwesomeDialog(
                  context: context,
                  dialogType: DialogType.NO_HEADER,
                  headerAnimationLoop: true,
                  animType: AnimType.BOTTOMSLIDE,
                  title: 'Agregar Usuario',
                  body: Form(
                    key: _key,
                    child: Column(
                      children: <Widget>[
                        Text('Ingrese el correo del usuario'),
                        SizedBox(
                          height: 30,
                        ),
                        TextFormField(
                          style: TextStyle(color: globals.blue),
                          validator: (text) {
                            String sanitizedVal = text.trim();
                            if (sanitizedVal.length == 0) {
                              return "El correo no puede ir vacio";
                            } else if (!EmailValidator.validate(sanitizedVal)) {
                              return "El formato para correo no es correcto";
                            }
                            return null;
                          },
                          keyboardType: TextInputType.emailAddress,
                          maxLength: 30,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                              enabledBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: globals.blue),
                                gapPadding: 5,
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: globals.blue),
                                gapPadding: 5,
                              ),
                              labelText: 'Correo',
                              labelStyle: TextStyle(
                                  color: globals.blue.withOpacity(0.5)),
                              counterText: '',
                              focusColor: globals.gold),
                          onSaved: (text) => this._correo = text.trim(),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                  btnCancel: TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Cerrar",
                        style: TextStyle(
                            fontSize: 11,
                            color: globals.blue.withOpacity(0.75))),
                  ),
                  btnOk: TextButton(
                    onPressed: () async {
                      if (_key.currentState.validate()) {
                        _key.currentState.save();
                        EasyLoading.show(status: 'Espere un momento...');
                        ResponseRequest response =
                            await newFriendshipRequest(_correo);
                        EasyLoading.dismiss();
                        Fluttertoast.showToast(
                            msg: response.message,
                            backgroundColor:
                                response.error ? Colors.red : Colors.green,
                            timeInSecForIosWeb: 5);
                      }
                    },
                    child: Text("Buscar",
                        style: TextStyle(
                            fontSize: 11,
                            color: globals.blue.withOpacity(0.75))),
                  ),
                  btnOkOnPress: () async {})
                ..show();
            },
          ),
          Badge(
            position: BadgePosition.topStart(top: 1, start: 1),
            badgeContent: Text(widget.friends.pending.toString()),
            child: IconButton(
              icon: Icon(Icons.notifications),
              onPressed: () {
                Navigator.pushNamed(context, '/friendships/request');
              },
            ),
          ),
          // SizedBox(
          //   width: 10,
          // )
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
        ),
        child: Container(
          width: double.infinity,
          child: Column(children: [
            widget.friends.last == null
                ? SizedBox(
                    width: 0,
                  )
                : _buildLastReactiosView(),
            _buildReactiosView(),
            _listFriedship(),
          ]),
        ),
      ),
    );
  }

  Widget _listFriedship() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.friends.friends.length,
      physics: ScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: Text(widget.friends.friends[index].name),
          subtitle: Text(widget.friends.friends[index].email),
          trailing: Wrap(
            children: <Widget>[
              Container(
                width: 45,
                height: 45,
                child: LikeButton(
                  size: 25,
                  circleColor: CircleColor(
                      start: Color(0xff00ddff), end: Color(0xff0099cc)),
                  bubblesColor: BubblesColor(
                    dotPrimaryColor: Color(0xff33b5e5),
                    dotSecondaryColor: Color(0xff0099cc),
                  ),
                  likeBuilder: (liked) {
                    return Icon(
                      Icons.thumb_up_outlined,
                      color: this.liked ? globals.blue : Colors.grey,
                      size: 25,
                    );
                  },
                  animationDuration: const Duration(seconds: 2),
                  onTap: (isLiked) {
                    return onLikeButtonTapped(
                        isLiked, widget.friends.friends[index].id, 'like');
                  },
                ),
              ),
              Container(
                width: 45,
                height: 45,
                child: LikeButton(
                  size: 25,
                  circleColor: CircleColor(
                      start: Color(0xff00ddff), end: Color(0xff0099cc)),
                  bubblesColor: BubblesColor(
                    dotPrimaryColor: Color(0xff33b5e5),
                    dotSecondaryColor: Color(0xff0099cc),
                  ),
                  likeBuilder: (bool isLiked) {
                    return Icon(
                      Icons.sentiment_very_satisfied_outlined,
                      color: isLiked ? globals.blue : Colors.grey,
                      size: 25,
                    );
                  },
                  animationDuration: const Duration(seconds: 2),
                  onTap: (bool isLiked) {
                    return onLikeButtonTapped(isLiked,
                        widget.friends.friends[index].id, 'admiration');
                  },
                ),
              ),
              Container(
                width: 45,
                height: 45,
                child: LikeButton(
                  size: 25,
                  circleColor: CircleColor(
                      start: Color(0xff00ddff), end: Color(0xff0099cc)),
                  bubblesColor: BubblesColor(
                    dotPrimaryColor: Color(0xff33b5e5),
                    dotSecondaryColor: Color(0xff0099cc),
                  ),
                  likeBuilder: (bool isLiked) {
                    return Icon(
                      Icons.favorite_border,
                      color: isLiked ? globals.blue : Colors.grey,
                      size: 25,
                    );
                  },
                  animationDuration: const Duration(seconds: 2),
                  onTap: (bool isLiked) {
                    return onLikeButtonTapped(
                        isLiked, widget.friends.friends[index].id, 'pride');
                  },
                ),
              ),
              Container(
                width: 45,
                height: 45,
                child: LikeButton(
                  size: 25,
                  circleColor: CircleColor(
                      start: Color(0xff00ddff), end: Color(0xff0099cc)),
                  bubblesColor: BubblesColor(
                    dotPrimaryColor: Color(0xff33b5e5),
                    dotSecondaryColor: Color(0xff0099cc),
                  ),
                  likeBuilder: (bool isLiked) {
                    return Icon(
                      Icons.military_tech_outlined,
                      color: isLiked ? globals.blue : Colors.grey,
                      size: 25,
                    );
                  },
                  animationDuration: const Duration(seconds: 2),
                  onTap: (bool isLiked) {
                    return onLikeButtonTapped(
                        isLiked, widget.friends.friends[index].id, 'improve');
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _buildReactiosView() {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Card(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _reactionElement(widget.friends.favor[0].toString(), "Me gusta",
                  Icon(Icons.thumb_up_outlined)),
              _reactionElement(widget.friends.favor[1].toString(), "Admiración",
                  Icon(Icons.sentiment_very_satisfied_outlined)),
              _reactionElement(widget.friends.favor[2].toString(), "Orgullo",
                  Icon(Icons.favorite_border)),
              _reactionElement(widget.friends.favor[3].toString(),
                  "Sigue mejorando", Icon(Icons.military_tech_outlined)),
            ],
          ),
        ),
      ),
    );
  }

  _reactionElement(String badgeContent, String label, Icon icon) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Column(
          children: <Widget>[
            Badge(
                badgeColor: Colors.green,
                position: BadgePosition.topStart(),
                badgeContent: Text(badgeContent),
                child: icon),
            Text(
              label,
              style: TextStyle(fontSize: 12),
              textAlign: TextAlign.center,
              maxLines: 2,
            )
          ],
        ),
      ),
    );
  }

  _buildLastReactiosView() {
    return Container(
      padding: EdgeInsets.all(0),
      child: Card(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text('Última reacción que recibiste:'),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(widget.friends.last),
                  _buildReaction(widget.friends.typeLast),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildReaction(String type) {
    switch (type) {
      case 'like':
        return Column(
          children: <Widget>[Icon(Icons.thumb_up_outlined), Text('Me gusta')],
        );
      case 'admiration':
        return Column(
          children: <Widget>[
            Icon(Icons.sentiment_very_satisfied_outlined),
            Text('Admiración')
          ],
        );
      case 'pride':
        return Column(
          children: <Widget>[Icon(Icons.favorite_border), Text('Orgullo')],
        );
      case 'improve':
        return Column(
          children: <Widget>[
            Icon(Icons.military_tech_outlined),
            Text('Sigue Mejorando')
          ],
        );
      default:
        return Column(
          children: <Widget>[Icon(Icons.thumb_up_outlined), Text('Me gusta')],
        );
    }
  }

  Future<bool> onLikeButtonTapped(bool isLiked, id, String type) async {
    ResponseRequest response = await sendFavor(type, id);
    Fluttertoast.showToast(
        msg: response.message,
        backgroundColor: response.error ? Colors.red : Colors.green,
        timeInSecForIosWeb: 5);

    setState(() {
      Future.delayed(const Duration(seconds: 3), () {
        this.liked = false;
      });
    });
    return !isLiked;
  }
}
