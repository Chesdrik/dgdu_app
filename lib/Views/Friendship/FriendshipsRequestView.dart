import 'package:dgdu_app/Models/Friend.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Views/Friendship/FriendshipFunctions.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FriendshipsRequestView extends StatefulWidget {
  List<Friend> friends;

  FriendshipsRequestView({Key key, this.friends}) : super(key: key);

  @override
  _FriendshipsRequestViewState createState() => _FriendshipsRequestViewState();
}

class _FriendshipsRequestViewState extends State<FriendshipsRequestView> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(
          context, 'generic', 'Solicitudes', true, false, 'friendship'),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover),
        ),
        child: Container(
          width: double.infinity,
          child: Column(children: [_listFriedshipRequest()]),
        ),
      ),
    );
  }

  Widget _listFriedshipRequest() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.friends.length,
      physics: ScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: Text(widget.friends[index].name),
          subtitle: Text(widget.friends[index].email),
          trailing: Wrap(
            spacing: 12, // space between two icons
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () async {
                  EasyLoading.show(status: 'Espere un momento...');
                  ResponseRequest response = await changeStatusFriendship(
                      'denied', widget.friends[index].id);
                  EasyLoading.dismiss();
                  Fluttertoast.showToast(
                      msg: response.message,
                      backgroundColor:
                          response.error ? Colors.red : Colors.green,
                      timeInSecForIosWeb: 5);

                  setState(() {
                    if (!response.error) {
                      widget.friends.removeWhere(
                          (item) => item.id == widget.friends[index].id);
                    }
                  });
                },
              ),
              IconButton(
                icon: Icon(Icons.done),
                onPressed: () async {
                  EasyLoading.show(status: 'Espere un momento...');
                  ResponseRequest response = await changeStatusFriendship(
                      'approved', widget.friends[index].id);
                  EasyLoading.dismiss();
                  Fluttertoast.showToast(
                      msg: response.message,
                      backgroundColor:
                          response.error ? Colors.red : Colors.green,
                      timeInSecForIosWeb: 5);
                  setState(() {
                    if (!response.error) {
                      widget.friends.removeWhere(
                          (item) => item.id == widget.friends[index].id);
                    }
                  });
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
