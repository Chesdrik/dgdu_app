import 'package:dgdu_app/Views/Friendship/FriendshipFunctions.dart';
import 'package:dgdu_app/Views/Friendship/FriendshipView.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class FriendshipViewFetch extends StatefulWidget {
  FriendshipViewFetch({Key key}) : super(key: key);

  @override
  FriendshipViewFetchState createState() => new FriendshipViewFetchState();
}

class FriendshipViewFetchState extends State<FriendshipViewFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getFriendshipInfo(context),
        builder: (BuildContext context, AsyncSnapshot friends) {
          return (friends.data == null
              ? LoadingScreen()
              : FriendshipView(
                  friends: friends.data,
                ));
        });
  }
}
