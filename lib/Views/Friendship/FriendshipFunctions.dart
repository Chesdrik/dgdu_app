import 'package:dgdu_app/Models/Friend.dart';
import 'package:dgdu_app/Models/Friendships.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

/// Get the current information about user's friends.
///
/// {@category FriendshipInformation}
/// {@subCategory Information displays}
// ignore: missing_return
Future<Friendships> getFriendshipInfo(context) async {
  // print('info del usuario');
  try {
    Uri uri = globals.uriConstructor('/friendship');
    var data = await http.post(
      uri,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'id_user': globals.user.id.toString(),
      }),
    );

    var jsonData = json.decode(data.body);

    List<Friend> friendlist = [];
    // print(jsonData);
    // print(jsonData['friends'].isNotEmpty);

    if (jsonData['friends'].isNotEmpty) {
      for (var e in jsonData['friends']) {
        Friend friend =
            new Friend(id: e['id'], name: e['name'], email: e['email']);
        friendlist.add(friend);
      }
    }
    List<int> favors = [];

    if (jsonData.containsKey("details")) {
      // print(jsonData['details'][0]['Me gusta']);
      favors.add(jsonData['details']['Me gusta']);
      favors.add(jsonData['details']['Orgullo']);
      favors.add(jsonData['details']['Admiracion']);
      favors.add(jsonData['details']['Sigue mejorando']);
    } else {
      favors = [0, 0, 0, 0];
    }

    // print(favors);

    String last = jsonData['last'];
    String type = jsonData['type'];

    Friendships friends = new Friendships(
        pending: jsonData['pendigs'],
        friends: friendlist,
        favor: favors,
        last: last,
        typeLast: type);

    return friends;
  } catch (e) {
    // print(e);
    Navigator.pushNamed(context, '/error');
  }
}

/// Get the current information about user's friendship request.
///
/// {@category FriendshipInformation}
/// {@subCategory Information displays}
Future<List<Friend>> getFriendshipRequest() async {
  // print('friendship request');
  Uri uri = globals.uriConstructor('/pending/friendship');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'id_user': globals.user.id.toString(),
    }),
  );

  var jsonData = json.decode(data.body);

  List<Friend> friendlist = [];
  // print(jsonData);
  // print(jsonData['friends'].isNotEmpty);

  if (jsonData['friends'].isNotEmpty) {
    for (var e in jsonData['friends']) {
      Friend friend =
          new Friend(id: e['id'], name: e['name'], email: e['email']);
      friendlist.add(friend);
    }
  }

  return friendlist;
}

/// Create reqwuest for a new request friendship.
///
/// {@category FriendshipInformation}
/// {@subCategory Information displays}
Future<ResponseRequest> newFriendshipRequest(String email) async {
  // print('friendship request');
  Uri uri = globals.uriConstructor('/new/friendship');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'id_user': globals.user.id.toString(),
      'email': email
    }),
  );

  var jsonData = json.decode(data.body);

  if (jsonData == null) {
    ResponseRequest response =
        new ResponseRequest(true, "No hay conexión a internet");
    return response;
  } else {
    // print(jsonData);
    ResponseRequest response =
        new ResponseRequest(jsonData['error'], jsonData['message']);
    return response;
  }
}

/// Change status friendships request.
///
/// {@category FriendshipInformation}
/// {@subCategory Information displays}
Future<ResponseRequest> changeStatusFriendship(
    String type, int idfriend) async {
  // print('friendship request');
  Uri uri = globals.uriConstructor('/change_ststus/friendship');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'id_user': globals.user.id.toString(),
      'id_friend': idfriend.toString(),
      'type': type
    }),
  );

  var jsonData = json.decode(data.body);

  if (jsonData == null) {
    ResponseRequest response =
        new ResponseRequest(true, "No hay conexión a internet");
    return response;
  } else {
    // print(jsonData);
    ResponseRequest response =
        new ResponseRequest(jsonData['error'], jsonData['message']);
    return response;
  }
}

/// Send favor to friend.
///
/// {@category FriendshipInformation}
/// {@subCategory Information displays}
Future<ResponseRequest> sendFavor(String type, int idfriend) async {
  // print('friendship request');
  Uri uri = globals.uriConstructor('/reactions/friendship');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'id_user': globals.user.id.toString(),
      'id_friend': idfriend.toString(),
      'type': type
    }),
  );

  var jsonData = json.decode(data.body);

  if (jsonData == null) {
    ResponseRequest response =
        new ResponseRequest(true, "No hay conexión a internet");
    return response;
  } else {
    // print(jsonData);
    ResponseRequest response =
        new ResponseRequest(jsonData['error'], jsonData['message']);
    return response;
  }
}
