import 'package:dgdu_app/Views/Friendship/FriendshipFunctions.dart';
import 'package:dgdu_app/Views/Friendship/FriendshipsRequestView.dart';
import 'package:dgdu_app/Views/Widgets/Loading.dart';
import 'package:flutter/material.dart';

class FriendshipsRequestViewFetch extends StatefulWidget {
  FriendshipsRequestViewFetch({Key key}) : super(key: key);

  @override
  FriendshipsRequestViewFetchState createState() =>
      new FriendshipsRequestViewFetchState();
}

class FriendshipsRequestViewFetchState
    extends State<FriendshipsRequestViewFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getFriendshipRequest(),
        builder: (BuildContext context, AsyncSnapshot friends) {
          return (friends.data == null
              ? LoadingScreen()
              : FriendshipsRequestView(
                  friends: friends.data,
                ));
        });
  }
}
