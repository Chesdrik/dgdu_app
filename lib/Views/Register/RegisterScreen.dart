import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Views/Register/RegisterFunctions.dart';
import 'package:dgdu_app/Views/Widgets/AppDrawer.dart';
import 'package:dgdu_app/Views/Widgets/Forms/Dropdown.dart';
import 'package:dgdu_app/Views/Widgets/Forms/DropdownDate.dart';
import 'package:dgdu_app/Views/globalViews.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:email_validator/email_validator.dart';
import 'package:intl/intl.dart';

enum Genero { hombre, mujer }

class RegisterScreen extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(
      builder: (context) => RegisterScreen(),
    );
  }

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  GlobalKey<FormState> _key = GlobalKey();

  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');

  // CURP modificado (sin verificación de homoclave)
  RegExp curpRegExp = new RegExp(
      r'^([A-Z&]|[a-z&]{1})([AEIOU]|[aeiou]{1})([A-Z&]|[a-z&]{1})([A-Z&]|[a-z&]{1})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]|[hm]{1})([AS|as|BC|bc|BS|bs|CC|cc|CS|cs|CH|ch|CL|cl|CM|cm|DF|df|DG|dg|GT|gt|GR|gr|HG|hg|JC|jc|MC|mc|MN|mn|MS|ms|NT|nt|NL|nl|OC|oc|PL|pl|QT|qt|QR|qr|SP|sp|SL|sl|SR|sr|TC|tc|TS|ts|TL|tl|VZ|vz|YN|yn|ZS|zs|NE|ne]{2})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})(([A-Z]|[a-z]|[0-9]){2})$');

  // CURP original
  // RegExp curpRegExp = new RegExp(
  //     r'^([A-Z&]|[a-z&]{1})([AEIOU]|[aeiou]{1})([A-Z&]|[a-z&]{1})([A-Z&]|[a-z&]{1})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]|[hm]{1})([AS|as|BC|bc|BS|bs|CC|cc|CS|cs|CH|ch|CL|cl|CM|cm|DF|df|DG|dg|GT|gt|GR|gr|HG|hg|JC|jc|MC|mc|MN|mn|MS|ms|NT|nt|NL|nl|OC|oc|PL|pl|QT|qt|QR|qr|SP|sp|SL|sl|SR|sr|TC|tc|TS|ts|TL|tl|VZ|vz|YN|yn|ZS|zs|NE|ne]{2})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([0-9]{2})$');

  // RegExp curpRegExp = new RegExp(
  //     r'^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$');

  RegExp namesRegExp =
      new RegExp(r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
  RegExp numbersRegExp = new RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]');
  String _nombre;
  String _app;
  String _apm;
  String _curp;
  String _correo;
  String mensaje = '';
  var idUserType = '0';
  List<DDEntry> userType = [];

  Genero _genero = Genero.hombre;

  bool _logueado = false;

  initState() {
    super.initState();
    this.userType = getUserType();
    this.idUserType = this.userType.first.key;
  }

  dispose() {
    super.dispose();
  }

  DateTime selectedDate = DateTime.now();

  _selectDate(BuildContext context) async {
    var thlastDate = DateTime.now();
    final DateTime picked = await showDatePicker(
      locale: const Locale("es"),
      context: context,
      initialDate:
          DateTime(thlastDate.year - 5, thlastDate.month, thlastDate.day),
      firstDate:
          DateTime(thlastDate.year - 90, thlastDate.month, thlastDate.day),
      lastDate: DateTime(thlastDate.year - 5, thlastDate.month, thlastDate.day),
      builder: (context, child) {
        return Theme(
          data: ThemeData.dark(), // This will change to light theme.
          child: child,
        );
      },
    );
    if (picked != null &&
        picked !=
            DateTime(thlastDate.year - 5, thlastDate.month, thlastDate.day))
      setState(() {
        selectedDate = picked;
      });
  }

  _updateUserType(_newValue) {
    setState(() {
      this.idUserType = _newValue;
      // print(idUserType);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: AppDrawer(),
      appBar: globalAppBar(context, 'generic', 'Registro', true),
      backgroundColor: globals.blue,
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/fondo_gris.jpg"), fit: BoxFit.cover)),
        child: SingleChildScrollView(
          child: registerForm(),
        ),
      ),
    );
  }

  Widget registerForm() {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Container(
            // width: 300.0, //size.width * .6,
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      String sanitizedVal = text.trim();
                      if (sanitizedVal.length == 0) {
                        return "Este campo es requerido";
                      } else if (!namesRegExp.hasMatch(sanitizedVal)) {
                        return "Solo se aceptan letras A-Z";
                      }
                      return null;
                    },
                    textCapitalization: TextCapitalization.characters,
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'NOMBRE(S):',
                        labelStyle: TextStyle(color: globals.blue),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0),
                        focusColor: Colors.white),
                    onSaved: (text) => _nombre = text.trim(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      String sanitizedVal = text.trim();
                      if (sanitizedVal.length == 0) {
                        return "Este campo es requerido";
                      } else if (!namesRegExp.hasMatch(sanitizedVal)) {
                        return "Solo se aceptan letras A-Z";
                      }
                      return null;
                    },
                    textCapitalization: TextCapitalization.characters,
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'PRIMER APELLIDO:',
                        labelStyle: TextStyle(color: globals.blue),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0),
                        focusColor: Colors.white),
                    onSaved: (text) => _app = text.trim(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      String sanitizedVal = text.trim();
                      if (sanitizedVal.length == 0) {
                        return "Este campo es requerido";
                      } else if (!namesRegExp.hasMatch(sanitizedVal)) {
                        return "Solo se aceptan letras A-Z";
                      }
                      return null;
                    },
                    textCapitalization: TextCapitalization.characters,
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'SEGUNDO APELLIDO:',
                        labelStyle: TextStyle(color: globals.blue),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0),
                        focusColor: Colors.white),
                    onSaved: (text) => _apm = text.trim(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  DropdownDate(
                    labelText: 'FECHA DE NACIMIENTO:',
                    valueText: "${selectedDate.toLocal()}".split(' ')[0],
                    onPressed: () {
                      _selectDate(context);
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      String sanitizedVal = text.trim();
                      if (sanitizedVal.length == 0) {
                        return "Este campo es requerido";
                      } else if (!curpRegExp.hasMatch(sanitizedVal)) {
                        return "CURP incorrecto";
                      }
                      return null;
                    },
                    textCapitalization: TextCapitalization.characters,
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'CURP:',
                        labelStyle: TextStyle(color: globals.blue),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0),
                        focusColor: Colors.white),
                    onSaved: (text) => _curp = text.trim(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Hombre',
                        style: new TextStyle(color: globals.blue),
                      ),
                      Radio(
                        activeColor: globals.blue,
                        value: Genero.hombre,
                        groupValue: _genero,
                        onChanged: (Genero value) {
                          setState(() {
                            _genero = value;
                          });
                        },
                      ),
                      Text(
                        'Mujer',
                        style: new TextStyle(color: globals.blue),
                      ),
                      Radio(
                        activeColor: globals.blue,
                        value: Genero.mujer,
                        groupValue: _genero,
                        onChanged: (Genero value) {
                          setState(() {
                            _genero = value;
                          });
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Dropdown('TIPO DE USUARIO', this.idUserType, this.userType,
                      _updateUserType),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    style: TextStyle(color: globals.blue),
                    validator: (text) {
                      String sanitizedVal = text.trim();
                      if (sanitizedVal.length == 0) {
                        return "Este campo es requerido";
                      } else if (!EmailValidator.validate(sanitizedVal)) {
                        return "El formato para el correo no es correcto";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    maxLength: 100,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.blue),
                          gapPadding: 5,
                        ),
                        labelText: 'CORREO:',
                        labelStyle: TextStyle(color: globals.blue),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0)),
                    onSaved: (text) => _correo = text.trim(),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  ButtonTheme(
                    minWidth: 200.0,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(globals.blue),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      child: Text('ENVIAR',
                          style: TextStyle(fontSize: 20, color: globals.gold)),
                      onPressed: () async {
                        _key.currentState.save();
                        if (_key.currentState.validate()) {
                          _key.currentState.save();

                          if (!validateCURP(_nombre, _app, _apm, selectedDate,
                              _curp, _genero)) {
                            Fluttertoast.showToast(
                                msg:
                                    'El CURP no coincide con los datos proporcionados',
                                backgroundColor: Colors.red,
                                timeInSecForIosWeb: 5);
                          } else {
                            // print('Todo chido con el curp');
                            EasyLoading.show(status: 'Espere un momento...');
                            ResponseRequest response = await userRegister(
                                _nombre,
                                _app,
                                _apm,
                                selectedDate,
                                _curp,
                                _genero,
                                idUserType,
                                _correo);
                            EasyLoading.dismiss();
                            showMessage(response);
                          }
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  showMessage(ResponseRequest response) {
    AwesomeDialog(
        context: context,
        dialogType: response.error ? DialogType.ERROR : DialogType.SUCCES,
        headerAnimationLoop: false,
        animType: AnimType.TOPSLIDE,
        showCloseIcon: false,
        title: ' ',
        desc: response.message,
        btnOkText: 'Cerrar',
        onDissmissCallback: (type) {
          if (!response.error) {
            _key.currentState.reset();
          }
        },
        btnOkOnPress: () {
          if (!response.error) {
            _key.currentState.reset();
          }
        })
      ..show();
  }

  bool validateCURP(_nombre, _app, _apm, selectedDate, _curp, _genero) {
    String appInputSubstring = _app.substring(0, 1) +
        getVocal(_app.substring(1,
            _app.length)); // Primera letra y primera vocal (sin ser la primera letra) del apellido paterno
    String apmInputSubstring =
        _apm.substring(0, 1); // Primera letra del apellido materno
    String nameInputSubstring =
        _nombre.substring(0, 1); // Primera letra del nombre
    String nameInput =
        (appInputSubstring + apmInputSubstring + nameInputSubstring)
            .toUpperCase(); // String conformado por las variables anteriores

    // Complete name validation
    String nameCURP = _curp.substring(0, 4);

    bool resultName = nameCURP == nameInput;
    // print("nameCURP: " + nameCURP.toString());
    // print("nameInput: " + nameInput.toString());

    // Birthdate validation
    DateFormat formatter = DateFormat('yyMMdd');
    String dateInput = formatter.format(selectedDate);
    String dateCurp = _curp.substring(4, 10);

    bool resultDate = dateCurp == dateInput;

    // Genero validation
    String generoInput = _genero == Genero.hombre ? 'H' : 'M';
    String generoCURP = _curp.substring(10, 11);

    bool resultGenero = generoInput == generoCURP;

    // Consonantes validation
    String appConsonant = _app.substring(1);
    String apmConsonant = _apm.substring(1);
    String nameConsonant = _nombre.substring(1);
    String consonanteAppImput = getConsonant(appConsonant);
    String consonanteApmImput = getConsonant(apmConsonant);
    String consonanteNameImput = getConsonant(nameConsonant);

    String consonantFinalImput =
        (consonanteAppImput + consonanteApmImput + consonanteNameImput)
            .toUpperCase();

    String consonantCURP = _curp.substring(13, 16);
    // print('Consonants');
    // print(consonantFinalImput);
    // print(consonantCURP);

    bool resultConsonants = consonantFinalImput == consonantCURP;

    // print('Results');
    // print("resultName: " + resultName.toString());
    // print("resultDate: " + resultDate.toString());
    // print("resultGenero: " + resultGenero.toString());
    // print("resultConsonants: " + resultConsonants.toString());

    return resultName && resultDate && resultGenero && resultConsonants;
  }

  getConsonant(str) {
    String result = '';
    for (int i = 0; i <= str.length; i++) {
      if (isConsonant(str[i])) {
        result = str[i];
        break;
      }
    }
    return result;
  }

  isConsonant(s) {
    return !(s == 'a' ||
        s == 'e' ||
        s == 'i' ||
        s == 'o' ||
        s == 'u' ||
        s == 'A' ||
        s == 'E' ||
        s == 'I' ||
        s == 'O' ||
        s == 'U' ||
        s == '1' ||
        s == '2' ||
        s == '3' ||
        s == '4' ||
        s == '5' ||
        s == '6' ||
        s == '7' ||
        s == '8' ||
        s == '9' ||
        s == 'O');
  }

  // Returns first vocal or empty string if none found
  String getVocal(str) {
    for (int i = 0; i <= str.length; i++) {
      if (isVocal(str[i])) {
        return str[i];
      }
    }

    return '';
  }

  // Check if a string is a vocal
  bool isVocal(String vocal) {
    // Defining vocals
    List vocals = ["a", "e", "i", "o", "u"];

    // Checking if variable exists in list
    return vocals.contains(vocal.toLowerCase());
  }
}
