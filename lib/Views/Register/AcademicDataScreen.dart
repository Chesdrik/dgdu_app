import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:dgdu_app/Views/Register/RegisterFunctions.dart';
import 'package:dgdu_app/Views/Widgets/Forms/Dropdown.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;

class AcademicDataScreen extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(
      builder: (context) => AcademicDataScreen(),
    );
  }

  @override
  _AcademicDataScreenState createState() => _AcademicDataScreenState();
}

class _AcademicDataScreenState extends State<AcademicDataScreen> {
  GlobalKey<FormState> _key = GlobalKey();

  String _num;
  var idUserType = '0';
  List<DDEntry> userType = [];

  initState() {
    super.initState();
    this.userType = getUserType();
    this.idUserType = this.userType.first.key;
  }

  dispose() {
    super.dispose();
  }

  DateTime selectedDate = DateTime.now();

  _updateUserType(_newValue) {
    setState(() {
      this.idUserType = _newValue;
      // print(idUserType);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globals.blue,
      body: SingleChildScrollView(
        child: registerForm(),
      ),
    );
  }

  Widget registerForm() {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 80,
          ),
          Container(
            // width: 300.0, //size.width * .6,
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  Text(
                    'DATOS ACADÉMICOS',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  Divider(
                    color: Colors.white,
                    height: 30,
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  TextFormField(
                    style: TextStyle(color: Colors.white),
                    validator: (text) {
                      if (text.length == 0) {
                        return "Este campo es requerido";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 30,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: BorderSide(color: globals.gold),
                          gapPadding: 5,
                        ),
                        labelText: 'NÚMERO DE TRABAJADOR:',
                        labelStyle: TextStyle(color: globals.gold),
                        counterText: '',
                        contentPadding:
                            new EdgeInsets.symmetric(horizontal: 8.0),
                        focusColor: Colors.white),
                    onSaved: (text) => _num = text,
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Dropdown('POSGRADO:', this.idUserType, this.userType,
                      _updateUserType),
                  SizedBox(
                    height: 35,
                  ),
                  Dropdown('PLANTER:', this.idUserType, this.userType,
                      _updateUserType),
                  SizedBox(
                    height: 35,
                  ),
                  Dropdown('CARRERA/PROGRAMA:', this.idUserType, this.userType,
                      _updateUserType),
                  SizedBox(
                    height: 35,
                  ),
                  ButtonTheme(
                    minWidth: 200.0,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(globals.blue),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      child: Text('CONTINUAR',
                          style: TextStyle(fontSize: 20, color: globals.gold)),
                      onPressed: () {
                        if (_key.currentState.validate()) {
                          _key.currentState.save();
                          setState(() {
                            globals.number = _num;
                          });
                          Navigator.pushNamed(context, '/password');
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
