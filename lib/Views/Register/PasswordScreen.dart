import 'package:dgdu_app/Views/Register/RegisterFunctions.dart';
import 'package:flutter/material.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:fluttertoast/fluttertoast.dart';

class PasswordScreen extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(
      builder: (context) => PasswordScreen(),
    );
  }

  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  GlobalKey<FormState> _key = GlobalKey();
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');

  // Initially password is obscure
  bool _obscureText = true;
  bool _obscureTextConf = true;

  String _contrasena;
  String _confContrasena;

  initState() {
    super.initState();
  }

  dispose() {
    super.dispose();
  }

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  // Toggles the password confirm show status
  void _toggleConf() {
    setState(() {
      _obscureTextConf = !_obscureTextConf;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globals.blue,
      body: SingleChildScrollView(
        child: loginForm(),
      ),
    );
  }

  Widget loginForm() {
    return Padding(
        padding: EdgeInsets.all(30),
        child: Form(
          key: _key,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 80,
              ),
              Text(
                'ACCESO A MI CUENTA',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              Divider(
                color: Colors.white,
                height: 30,
              ),
              SizedBox(
                height: 50,
              ),
              TextFormField(
                style: TextStyle(color: Colors.white),
                validator: (text) {
                  if (text.length == 0) {
                    return "Este campo es requerido";
                  } else if (text.length <= 7) {
                    return "Su contraseña debe ser al menos de 8 caracteres";
                  } else if (!contRegExp.hasMatch(text)) {
                    return "El formato para contraseña no es correcto";
                  }
                  return null;
                },
                keyboardType: TextInputType.text,
                maxLength: 14,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  enabledBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: globals.gold),
                    gapPadding: 5,
                  ),
                  focusedBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: globals.gold),
                    gapPadding: 5,
                  ),
                  labelText: 'ELIGE UNA CONTRASEÑA',
                  labelStyle: TextStyle(color: globals.gold),
                  counterText: '',
                ),
                onSaved: (text) => _contrasena = text,
                obscureText: _obscureText,
              ),
              TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(globals.blue),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  onPressed: _toggle,
                  child: new Text(
                      _obscureText
                          ? "MOSTRAR CONTRASEÑA"
                          : "OCULTAR CONTRASEÑA",
                      style: TextStyle(fontSize: 10, color: Colors.white))),
              SizedBox(
                height: 30,
              ),
              TextFormField(
                style: TextStyle(color: Colors.white),
                validator: (text) {
                  if (text.length == 0) {
                    return "Este campo es requerido";
                  } else if (text.length <= 7) {
                    return "Su contraseña debe ser al menos de 8 caracteres";
                  } else if (!contRegExp.hasMatch(text)) {
                    return "El formato para contraseña no es correcto";
                  } else if (!(_contrasena == _confContrasena)) {
                    return "Las contraseñas no coinciden";
                  }
                  return null;
                },
                keyboardType: TextInputType.text,
                maxLength: 14,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  enabledBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: globals.gold),
                    gapPadding: 5,
                  ),
                  focusedBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: globals.gold),
                    gapPadding: 5,
                  ),
                  labelText: 'REPITE TU CONTRASEÑA',
                  labelStyle: TextStyle(color: globals.gold),
                  counterText: '',
                ),
                onSaved: (text) => _confContrasena = text,
                obscureText: _obscureTextConf,
              ),
              TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(globals.blue),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  onPressed: _toggleConf,
                  child: new Text(
                      _obscureTextConf
                          ? "MOSTRAR CONTRASEÑA"
                          : "OCULTAR CONTRASEÑA",
                      style: TextStyle(fontSize: 10, color: Colors.white))),
              SizedBox(
                height: 15,
              ),
              Text("DE 8 A 14 CARACTÉRES",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
              Text("SÓLO LETRAS Y NÚMEROS",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
              SizedBox(
                height: 15,
              ),
              ButtonTheme(
                minWidth: 200.0,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(globals.blue),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                    ),
                  ),
                  child: Text('ENVIAR',
                      style: TextStyle(fontSize: 20, color: globals.gold)),
                  onPressed: () async {
                    if (_key.currentState.validate()) {
                      _key.currentState.save();
                      setState(() {
                        globals.password = _contrasena;
                      });
                      // bool result = await userRegister();
                      // if (result) {
                      //   Navigator.pushNamed(context, '/user/resume');
                      // } else {
                      //   Fluttertoast.showToast(
                      //     msg:
                      //         "Hubo un error al hacer el registro, intente en otro momento",
                      //     backgroundColor: Colors.grey,
                      //   );
                      // }
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
            ],
          ),
        ));
  }
}
