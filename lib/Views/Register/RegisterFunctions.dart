import 'dart:convert';

import 'package:dgdu_app/Models/DDEntry.dart';
import 'package:dgdu_app/Models/ResponseRequest.dart';
import 'package:dgdu_app/Models/UserRegister.dart';
import 'package:dgdu_app/bd/UserBD.dart';
import 'package:dgdu_app/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

List<DDEntry> getUserType() {
  List<DDEntry> list = [
    DDEntry(key: '3', value: 'Externo'),
    DDEntry(key: '4', value: 'Bachillerato'),
    DDEntry(key: '5', value: 'Licenciatura'),
    DDEntry(key: '6', value: 'Posgrado'),
    DDEntry(key: '7', value: 'Egresado'),
    DDEntry(key: '8', value: 'Intercambio'),
    DDEntry(key: '10', value: 'Académico'),
    DDEntry(key: '11', value: 'Administrativo'),
    DDEntry(key: '12', value: 'Confianza'),
    DDEntry(key: '13', value: 'Familiar Académico'),
    DDEntry(key: '14', value: 'Familiar Administrativo'),
    DDEntry(key: '15', value: 'Familiar Confianza'),
  ];
  return list;
}

/// Send the current information about a user.
/// Receives register user data to display
///
/// {@category effort}
/// {@subCategory Information effort displays}
Future<ResponseRequest> userRegister(_nombre, _app, _apm, selectedDate, _curp,
    _genero, idUserType, _correo) async {
  var birthday = selectedDate;
  String date = DateFormat('dd-MM-yyy').format(birthday).toString();

  Uri uri = globals.uriConstructor('/register');
  var data = await http.post(
    uri,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'name': _nombre,
      'last_name': _app,
      'second_last_name': _apm,
      'birthday': date,
      'CURP': _curp,
      'email': _correo,
      'user_type_id': idUserType,
    }),
  );
  var jsonData = json.decode(data.body);
  // print(jsonData);

  // if (!jsonData['error']) {
  //   var user = jsonData['user'];
  //   UserRegister userregister = new UserRegister(
  //       name: user['name'],
  //       last_name: user['last_name'],
  //       second_last_name: user['second_last_name'],
  //       birthday: user['birthdate'],
  //       curp: user['CURP'],
  //       gender: user['female'],
  //       email: user['email'],
  //       weight: user['weight'],
  //       height: user['height'],
  //       worker_number: user['worker_number'],
  //       master: user['master'],
  //       campus: user['campus'],
  //       career: user['career']);

  //   await DBProvider.db.newUserRegister(userregister);
  // }
  ResponseRequest response =
      new ResponseRequest(jsonData['error'], jsonData['message']);
  return response;
}
